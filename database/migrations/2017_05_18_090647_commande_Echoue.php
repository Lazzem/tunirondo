<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class commandeEchoue extends Migration
{

    public function up()
    {
        Schema::create('commandeEchoue', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id');
            $table->float('montant');
        });
    }


    public function down()
    {
        Schema::drop('commandeEchoue');
    }
}
