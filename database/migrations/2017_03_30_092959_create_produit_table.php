<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Produit', function(Blueprint $table) {
            $table->increments('id');
            $table->string('Title');

            $table->string('Organisateur');
            $table->boolean('Status');
            $table->text('Description');
            $table->text('DescriptionMineur');
            $table->string('PhotosPrincipale');
            $table->string('Photos1');
            $table->string('Photos2');
            $table->integer('Categorie')->unsigned();
            $table->foreign('Categorie')->references('id')->on('souscategorie');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Produit');
    }
}
