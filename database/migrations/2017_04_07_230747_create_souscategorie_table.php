<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSouscategorieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('souscategorie', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');

            $table->string('title');
            $table->text('description');
            $table->timestamps();
            $table->integer('tag_id')->unsigned();;
            $table->foreign('tag_id')->references('id')->on('categorie')

                ->onDelete('restrict')

                ->onUpdate('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('souscategorie', function(Blueprint $table) {
            $table->dropForeign('souscategoriee_tag_id_foreign');
        });

        Schema::drop('souscategorie');
    }
}