<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'lastname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'role' => $faker->role,
        'file' => $faker->text,

        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Useer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'lastname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'role' => $faker->role,
        'file' => $faker->text,

        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Produit::class, function (Faker\Generator $faker) {
    return [
        'organisateur' => $faker->safeEmail,
        'Description' => $faker->text,
        'Status'=> $faker->text,
        'Title' => $faker->text,

        'Categorie'=> $faker->text,
        'DescriptionMineur'=> $faker->text,
        'PhotosPrincipale'=> $faker->text,
        'Photos1'=> $faker->text,
        'Photos2'=> $faker->text,

    ];
});
$factory->define(App\Stock::class, function (Faker\Generator $faker) {
    return [
        'idProduit'=> $faker->numberBetween(),
        'Date'=> $faker->dateTime,
        'Stock'=> $faker->numberBetween(),
        'Prix0'=> $faker->numberBetween(),
        'PrixTVA'=> $faker->numberBetween(),

        'Option1' => $faker->text,
        'Prix1'=> $faker->numberBetween(),
        'Option2' => $faker->text,
        'Prix2'=> $faker->numberBetween(),
        'Option3' => $faker->text,
        'Prix3'=> $faker->numberBetween(),
    ];
});