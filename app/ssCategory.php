<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ssCategory extends Model
{
    protected $table = 'souscategorie';
    protected $fillable = ['id','title','description','file','tag_id'];


    public function Category() {
        return $this->belongsTo('App\Category');
    }
    public function Produit()
    {
        return $this->hasMany('App\Produit');
    }
}
