<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Useer extends Model
{
    protected $table = 'users';
    protected $fillable = ['id','name','lastname','email','password','role','file'];
}
