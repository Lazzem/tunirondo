<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StockRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Date' => 'required|date|after:Aujourdhui' ,
            'Stock' => 'required|numeric' ,
            'Prix0' => array('required', 'regex:/^\d{1,13}(\.\d{1,4})?$/') ,
            'Prix1'=> array('regex:/^\d{1,13}(\.\d{1,4})?$/'),
            'Prix2'=> array( 'regex:/^\d{1,13}(\.\d{1,4})?$/'),
            'Prix3'=> array('regex:/^\d{1,13}(\.\d{1,4})?$/')


        ];
    }
}
