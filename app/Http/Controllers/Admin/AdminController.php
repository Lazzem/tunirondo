<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AdminController extends Controller
{
    public function __construct()
    {
       $this->middleware('admin');
    }
    public function dashboard()
    { if (Auth::guard('admin')->user()->role=='2')

        {return view('backOffice/index');}

    else
        Auth::guard('admin')->logout();
        return redirect  ('/admin');


}


}
