<?php

namespace App\Http\Controllers;

use App\ssCategory;
use App\Category;
use App\Http\Requests\ssCategorieRequest;
use Illuminate\Http\Requests;
use App\Http\Controllers\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;


class ssBackCategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ssCategories = ssCategory::paginate(1);
        $categorie = Category::all();
        return view ('BackOffice/ssBackCategorie',compact('ssCategories','categorie'));
    }
    public function get_ssCategorie_by_recherche()
    {
        $q = Input::get ( 'q' );
        $details = ssCategory::where ( 'id', '=',  $q  )->orWhere ( 'title', 'LIKE', '%' . $q . '%' )
            ->paginate(1);
        if (count ( $details ) > 0)
            return view ( 'BackOffice/RecherchessCategorie',compact('details') )->withQuery ( $q );
        else
            return view ( 'BackOffice/RecherchessCategorie' )->withMessage ( 'Aucunne sous categorie trouvé. Essayez de rechercher à nouveau!' );

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorie = Category::all();
        return view ('BackOffice/createssBackCategorie',compact('categorie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ssCategorieRequest $request)
    {

        $ssCategories =  new ssCategory;
        // upload the image //
        $file = $request->file('userfile');
        $destination_path = 'upload/';
        $filename = str_random(6).'_'.$file->getClientOriginalName();
        $file->move($destination_path, $filename);

        // save image data into database //
        $ssCategories->file = $destination_path . $filename;
        $ssCategories->title = $request->input('title');
        $ssCategories->description = $request->input('description');
        $ssCategories->tag_id = $request->input('tag_id');
        $ssCategories->save();
        return redirect('ssBackCategorie')->with('message','Sous-Categorie ajouter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ssCategories = ssCategory::find($id);
        $Categories=Category::all();
        return view('BackOffice.detailssBackCategorie',compact('ssCategories','Categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ssCategories = ssCategory::findOrFail($id);
        $Categories=Category::all();
        return view('BackOffice.editssBackCategorie',compact('ssCategories','Categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ssCategorieRequest $request, $id)
    {
        $ssCategories = ssCategory::find($id);
        // if user choose a file, replace the old one //
        if( $request->hasFile('userfile') ){
            $file = $request->file('userfile');
            $destination_path = 'upload/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destination_path, $filename);
            $ssCategories->file = $destination_path . $filename;
        }

        // replace old data with new data from the submitted form //
        $ssCategories->title = $request->input('title');
        $ssCategories->description = $request->input('description');
        $ssCategories->tag_id = $request->input('tag_id');
        $ssCategories->save();

        return redirect('ssBackCategorie')->with('message','Sous-Categorie modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ssCategories = ssCategory::findOrFail($id);
        $ssCategories->delete();
        return redirect('ssBackCategorie')->with('message','Sous-Categorie supprimer');
    }
}
