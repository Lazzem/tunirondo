<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produit;
use App\Stock;
use App\Http\Requests\StockRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class frontStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $id;

    public function index()
    {
        $idProduit = DB::table('Produit')
            ->select('id')->max('id');
        return view('createStock', compact('idProduit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StockRequest $request)
    {
        $idProduit= DB::table( 'Produit')
            ->select('id')->max('id');
        Stock::create($request->all());

        return redirect ('ConfirmationFrontProduit')->with('message',$idProduit);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idPRODUIT=DB::table( 'Stock')->where('id', '=', $id)->select('idProduit')->get();
        $stock = Stock::findOrFail($id);
        $stock->delete();
        foreach ($idPRODUIT as $idPRODUIT)
        {
            $idp=$idPRODUIT->idProduit;
        }
        return redirect('AjouterProduit/'.$idp.'/edit')->with('message','date supprimer');
    }
}
