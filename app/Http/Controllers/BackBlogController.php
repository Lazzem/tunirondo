<?php

namespace App\Http\Controllers;
use App\Blogss;
use App\Http\Requests\BlogRequest;
use Illuminate\Http\Requests;
use App\Http\Controllers\Request;
use App\menu ;
use DB;

class BackBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blogss::all();
        return view ('BackOffice/BBlog',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('BackOffice.createBlog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {

        $Blogs = new Blogss;

        // upload the image //
        $file = $request->file('userfile');
        $destination_path = 'upload/';
        $filename = str_random(6).'_'.$file->getClientOriginalName();
        $file->move($destination_path, $filename);

        // save image data into database //
        $Blogs->file = $destination_path . $filename;
        $Blogs->title = $request->input('title');
        $Blogs->description = $request->input('description');
        $Blogs->save();

        return redirect('BBlog')->with('message','Blog ajouter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Blogs = Blogss::findOrFail($id);
        return view('BackOffice.detailBlog',compact('Blogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Blogs = Blogss::findOrFail($id);
        return view('BackOffice.editBlog',compact('Blogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $Blogs = Blogss::find($id);

        // if user choose a file, replace the old one //
        if( $request->hasFile('userfile') ){
            $file = $request->file('userfile');
            $destination_path = 'upload/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destination_path, $filename);
            $Blogs->file = $destination_path . $filename;
        }

        // replace old data with new data from the submitted form //
        $Blogs->title = $request->input('title');
        $Blogs->description = $request->input('description');
        $Blogs->save();

        return redirect('BBlog')->with('message','Blog modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Blogs = Blogss::findOrFail($id);
        $Blogs->delete();
        return redirect('BBlog')->with('message','Blog supprimer');
    }
}
