<?php

namespace App\Http\Controllers;
use App\Category;
use App\Http\Requests\CategorieRequest;
use Illuminate\Http\Requests;
use App\Http\Controllers\Request;
use App\menu ;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class BackCategorieCOntroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorie = Category::paginate(1);
        return view ('BackOffice/BackCategorie',compact('categorie'));
    }
    public function get_Categorie_by_recherche()
    {
        $q = Input::get ( 'q' );
        $details = Category::where ( 'id', '=',  $q  )->orWhere ( 'title', 'LIKE', '%' . $q . '%' )
            ->paginate(1);
        if (count ( $details ) > 0)
            return view ( 'BackOffice/RechercheCategorie',compact('details') )->withQuery ( $q );
        else
            return view ( 'BackOffice/RechercheCategorie' )->withMessage ( 'Aucunne categorie trouvé. Essayez de rechercher à nouveau!' );

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = menu::all();
        return view ('BackOffice.createBackCategorie',compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategorieRequest $request)
    {

        $Categories = new Category;

        // upload the image //
        $file = $request->file('userfile');
        $destination_path = 'upload/';
        $filename = str_random(6).'_'.$file->getClientOriginalName();
        $file->move($destination_path, $filename);

        // save image data into database //
        $Categories->file = $destination_path . $filename;
        $Categories->title = $request->input('title');
        $Categories->idmenu = $request->input('idmenu');

        $Categories->description = $request->input('description');
        $Categories->save();

        return redirect('BackCategorie')->with('message','Categorie ajouter');
    }


    public function show($id)
    {
        $Categories = Category::findOrFail($id);
        return view('BackOffice.detailBackCategorie',compact('Categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Categories = Category::findOrFail($id);
        return view('BackOffice.editBackCategorie',compact('Categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategorieRequest $request, $id)
    {
        $Categories = Category::find($id);

        // if user choose a file, replace the old one //
        if( $request->hasFile('userfile') ){
            $file = $request->file('userfile');
            $destination_path = 'upload/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destination_path, $filename);
            $Categories->file = $destination_path . $filename;
        }

        // replace old data with new data from the submitted form //
        $Categories->title = $request->input('title');
        $Categories->idmenu = $request->input('idmenu');

        $Categories->description = $request->input('description');
        $Categories->save();

        return redirect('BackCategorie')->with('message','Categorie modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Categories = Category::findOrFail($id);
        $Categories->delete();
        return redirect('BackCategorie')->with('message','Categorie supprimer');
    }
}
