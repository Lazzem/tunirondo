<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;
use App\Http\Requests;

class SendEmailController extends Controller
{
    public function index()
    {
        $user = User::findOrFail(1);

        Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->name)->subject('Your Reminder!');
    });

    }

    public function send()
    {
        $title = 'title';
        $content = '123';




        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('mejri.mohamed.habibe@gmail.com', 'Christian Nwamba');

            $message->to('houssem.marnissi@gmail.com');

        });

        return response()->json(['message' => 'Request completed']);
    }
}
