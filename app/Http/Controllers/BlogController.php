<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Blogss;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{ public function getForm(){
    $Blogs= DB::table('Blog')
        ->paginate(2);
    return view('blog',compact('Blogs'));
}
    public function detailBlog($id){
        $Blog = Blogss::find($id);
        return view('blog_detail',compact('Blog'));
    }
}
