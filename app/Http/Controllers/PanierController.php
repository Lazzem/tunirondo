<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Commande;
use App\Produit;
use App\Panier;
use Auth;
use Session;
use App\Http\Requests;



class PanierController extends Controller
{
    function index()
    {
        return view('panier');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        Panier::create($request->all());
        return redirect('panier2');
    }

    public function destroy($id)
    {
        $panier = panier::findOrFail($id);
        $panier->delete();
        return redirect('panier')->with('message','produit a etais supprimé');
    }
    public function postPaiement(Request $request)
    {
        if (!Session::has('panier')) {
            return redirect()->route('shop.panier');
        }
        $vieuxPanier = Session::get('panier');
        $panier = new Panier($vieuxPanier);
        //Stripe a ete installé via composer.json -> configuration de stripe ci dessous
        Stripe::setApiKey('sk_test_yY8KNLKjkhQoH1t96kTbVCzM');
        try {
            $charge = Charge::create(array(
                //*100 car exrpimé en cents
                "amount" => $panier->prixTotal * 100,
                "currency" => "eur",
                "source" => $request->input('stripeToken'), // obtained with Stripe.js
                "description" => "Test Charge"
            ));
            $order = new Commande();
            //serialize est une fn php qui transforme l'objet php en une string, evite de creer des milliers de table sql pour stockr les articles achete
            //Reconstruire l'obj avec unserialize()
            $order->cart = serialize($panier);
            $order->address = $request->input('address');
            $order->name = $request->input('name');
            $order->payment_id = $charge->id;
            //sauvegarder la commande en BDD
            Auth::user()->commandes()->save($order);
        } catch (\Exception $e) {
            return redirect()->route('paiement')->with('error', $e->getMessage());
        }
        Session::forget('panier');
        return redirect()->route('produit.index')->with('success', 'Achat effectue avec succes!');
    }

    public function viderPanier(){
        Session::forget('panier');
        return redirect()->route('produit.index')->with('success', 'Panier vidé!');
    }
}
