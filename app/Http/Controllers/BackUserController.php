<?php

namespace App\Http\Controllers;

use App\Useer;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Requests;
use App\Http\Controllers\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class BackUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Useer::paginate(6);
        return view ('BackOffice/user',compact('users'));
    }
    public function get_user_by_recherche()
    {
        $q = Input::get ( 'q' );
        $details = Useer::where ( 'id', '=',  $q  )->orWhere ( 'name', 'LIKE', '%' . $q . '%' )
            ->orWhere ( 'lastname', 'LIKE', '%' . $q . '%' )
            ->orWhere ( 'email', 'LIKE', '%' . $q . '%' )
            ->paginate(6);
        if (count ( $details ) > 0)
            return view ( 'BackOffice/RechercheUtilisateur',compact('details') )->withQuery ( $q );
        else
            return view ( 'BackOffice/RechercheUtilisateur' )->withMessage ( 'Aucun utilisateur trouvé. Essayez de rechercher à nouveau!' );

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('BackOffice.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        Useer::create($request->all());
        return redirect('user')->with('message','Utilisateur a etais ajouter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $User = Useer::findOrFail($id);
        return view('BackOffice.detailUser',compact('User'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $User = Useer::findOrFail($id);
        return view('BackOffice.editUser',compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $User = Useer::findOrFail($id);

        $input = $request->all();

        $User->fill($input)->save();

        return redirect('user')->with('message','Utilisateur a etais modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = Useer::findOrFail($id);
        $User->delete();
        return redirect('user')->with('message','Utilisateur a etais supprimé');
    }
}
