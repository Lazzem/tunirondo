<?php

namespace App\Http\Controllers;

use App\Stock;
use Illuminate\Http\Request;
use App\Produit ;


use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class FrontProduitController extends Controller
{

    public function getProduit($id)
    {
        $produit  = Produit::find($id);
        $date  = DB::table('Stock')->where('idProduit','=',$id)->get();

        return view('detailProduit',compact('produit'),compact('date'));
    }


}
