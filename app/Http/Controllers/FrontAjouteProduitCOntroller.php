<?php

namespace App\Http\Controllers;
use App\Produit;
use App\Http\Requests\ProduitRequest;
use App\ssCategory;
use App\Stock;
use App\Category;
use App\Http\Requests\StockRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

use Illuminate\Support\Facades\Auth;
use App\User;

class FrontAjouteProduitCOntroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $produit=Produit::where('Organisateur','=',Auth::User()->email)->paginate(1);
        return view('ajouterProduit',compact('produit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ssCategorie = ssCategory::all();
        return view ('createFrontStockProduit',compact('ssCategorie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProduitRequest $request )
    {
        $Produit = new Produit ;

        // upload the image //
        $file0 = $request->file('PhotosPrincipale');


        $destination_path = 'upload/';
        $filename0 = str_random(6).'_'.$file0->getClientOriginalName();
        $file0->move($destination_path, $filename0);




        if( $request->hasFile('Photos1') ){
            $file0 = $request->file('Photos1');
            $destination_path = 'upload/';
            $filename0 = str_random(6).'_'.$file0->getClientOriginalName();
            $file0->move($destination_path, $filename0);
            $Produit->Photos1 = $destination_path . $filename0;
        }


        if ( $request->hasFile('Photos2') ){
            $file0 = $request->file('Photos2');
            $destination_path = 'upload/';
            $filename0 = str_random(6).'_'.$file0->getClientOriginalName();
            $file0->move($destination_path, $filename0);
            $Produit->Photos2 = $destination_path . $filename0;

        }

        // save image data into database //
        $Produit->PhotosPrincipale = $destination_path . $filename0;


        $Produit->Organisateur = $request->input('Organisateur');
        $Produit->Status = $request->input('Status');
        $Produit->Title = $request->input('Title');
        $Produit->Categorie = $request->input('Categorie');
        $Produit->DescriptionMineur = $request->input('DescriptionMineur');
        $Produit->Description = $request->input('Description');
        $Produit->save();




        $idProduit= DB::table( 'Produit')
            ->select('id')->max('id');
        return redirect ('FrontStock')->with('message',$idProduit);    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {$produit=Produit::where('Organisateur','=',$id)->paginate(4);
        return view('ajouterProduit',compact('produit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ssCategorie = ssCategory::all();
        $Stock= DB::table( 'Stock')->where('idProduit', '=', $id)->get();
        $Produit = Produit::findOrFail($id);
        return view('editFrontProduit',compact('Produit','ssCategorie'),compact('Stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProduitRequest $request, $id)
    {
        $Produit = Produit::find($id);
        if( $request->hasFile('PhotosPrincipale') ){
            $file0 = $request->file('PhotosPrincipale');
            $destination_path = 'upload/';
            $filename0 = str_random(6).'_'.$file0->getClientOriginalName();
            $file0->move($destination_path, $filename0);
            $Produit->PhotosPrincipale = $destination_path . $filename0;
        }


        if( $request->hasFile('Photos1') ){
            $file0 = $request->file('Photos1');
            $destination_path = 'upload/';
            $filename0 = str_random(6).'_'.$file0->getClientOriginalName();
            $file0->move($destination_path, $filename0);
            $Produit->Photos1 = $destination_path . $filename0;
        }


        if ( $request->hasFile('Photos2') ){
            $file0 = $request->file('Photos2');
            $destination_path = 'upload/';
            $filename0 = str_random(6).'_'.$file0->getClientOriginalName();
            $file0->move($destination_path, $filename0);
            $Produit->Photos2 = $destination_path . $filename0;

        }


        $Produit->Organisateur = $request->input('Organisateur');
        $Produit->Status = $request->input('Status');
        $Produit->Title = $request->input('Title');
        $Produit->Categorie = $request->input('Categorie');
        $Produit->DescriptionMineur = $request->input('DescriptionMineur');
        $Produit->Description = $request->input('Description');
        $Produit->save();




        return redirect('AjouterProduit/')->with('message','Produit modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Produit = Produit::findOrFail($id);
        $Produit->delete();
        return redirect('AjouterProduit/')->with('message','Produit supprimer');
    }
}
