<?php

namespace App\Http\Controllers\authadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class authadminController extends Controller
{


    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $redirectTo = '/Bindex';
    protected $guard= 'admin';

    public function showLoginForm()
    {
        if(Auth::guard('admin')->check())  {

                return redirect('/Bindex');

        }
        return view('backOffice/login');
    }
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect  ('/admin');
    }
}
