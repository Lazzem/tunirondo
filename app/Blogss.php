<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Blogss extends Model
{
    protected $table = 'blog';
    protected $fillable = ['id','title','file','description'];
}
