<?php
return array(
    /** set your paypal credential **/
    'client_id' =>'ARaXFRtE8tehHsiXyBuvOu_RDKpeLYR2r-4lVOcoOY3-X1dLiR2O4ilhoaGCuaHyszKeBSiu4do36OFC',
    'secret' => 'ECGk6n4TyM_ClVNoHlgvnGkE1XEQr7J8mCGz1KXIsxHtknP9KuA3n1cWafHO605RPd-Jtvo3YSYbu9qW',
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 1000,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);