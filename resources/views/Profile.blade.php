@extends('layouts.main')
<!-- Main -->
@section('content')


    <div class="container target">
        <div class="row">
            <div class="col-sm-10">
                <h1 class=""><spam>Bienvenue sur votre profile, <strong>{{ Auth::user()->lastname }}</strong></spam></h1>


            </div>

                    <div class="col-sm-2"><a  class="pull-right">@if(Auth::user()->file == null)<img title="profile image" class="img-circle img-responsive" src="{{asset('images/profile.png')}}">@else
                                <img title="profile image" class="img-circle img-responsive" src="{{asset(Auth::user()->file)}}">@endif</a>
            </div>

        </div>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <!--left col-->
                <ul class="list-group">
                    <li class="list-group-item text-right" style="background: #af93f6;height: 40px" ><strong style="float: left; color: white">PROFILE</strong></li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Nom</strong></span> {{ Auth::user()->name }}</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Prénom</strong></span> {{ Auth::user()->lastname }}</li>

                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Date d'inscription</strong></span> {{ Auth::user()->created_at->format('d/m/Y') }}</li>

                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Email</strong></span> {{ Auth::user()->email }}</li>

                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Role: </strong></span>
                        <?php if (Auth::user()->role=="1")
                        {
                            echo "Organisateur";
                        }
                        else if (Auth::user()->role=="0")
                        {
                            echo "Client";
                        }else
                            echo "Administrateur";

                        ?></li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #af93f6;height: 40px"><strong style="float: left; color: white">PARAMETRE DU COMPTE</strong>

                    </div>
                    <div class="panel-body"><i style="color:green" class="fa fa-check-square"></i><a href="/ModifierPassword"><b><U>Modifier Mot de passe</U></b></a>

                    </div>
                </div>
            </div>
            <!--/col-3-->
            <div class="col-sm-9 " contenteditable="false" style="">
                <div class="panel panel-default target">
                    <div class="panel-heading" contenteditable="false" style="background: #af93f6;height: 40px"><strong style="float: left; color: white">NOS SERVICES</strong></div>
                    <div class="panel-body" style="height: 296px;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="thumbnail"style="height: 272px">
                                    <img  src="images/editer.png" height="280" width="185">
                                    <div class="caption">
                                       <center>

                                              <a href="{{route('Profile.edit',[Auth::user()->id])}}">  <button type="submit" title="Submit" class="button submit" > <span> changer </span> </button>
                                            </a>

                                        </center>
                                        <p>
                                            <br>
                                            Changez vos Informations.
                                        </p>
                                        <p>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-7">
                                <div class="thumbnail" style="height: 272px">
                                    <img src="images/commande.jpg" height="280" width="185">
                                    <div class="caption">
                                       <center>
                                            <a href="/Commande">  <button type="submit" title="Submit" class="button submit" > <span> commande </span> </button>
                                            </a>


                                        </center>


                                        <p>
                                            <br>
                                            Affichage de tous vos commandes .
                                        </p>
                                        <p>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->role=="1" or Auth::user()->role=="2")
                            <div class="col-md-4 col-sm-7">
                                <div class="thumbnail" style="height: 272px">
                                    <img  src="{{asset('images/evenement.png')}}" height="280" width="185">
                                    <div class="caption">

                                        <center>
                                            <a href="{{url('AjouterProduit')}}">  <button type="submit" title="Submit" class="button submit" > <span> évenement </span> </button>
                                            </a>


                                        </center>

                                        <p>
                                        </br>
                                            Ajouter des evenements.
                                        </p>
                                        <p>

                                        </p>
                                    </div>
                                </div>

                            </div>
                                @endif

                        </div>

                    </div>

                </div>



</div>
        </div>


        <!-- End Quantcast tag -->





    </div>
    @stop