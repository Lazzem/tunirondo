@extends('layouts.main')
<!-- Main -->
@section('content')


    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-9 wow bounceInUp animated">
                    <div class="page-title">
                        <h2> Modifier vos informations</h2>
                    </br>
                    </div>
                    <div class="static-contain">
                        <fieldset class="group-select">
                            {{ csrf_field() }}
                        {!! Form::model(Auth::user(),array('route'=>['Profile.update',Auth::user()->id],'method'=>'PUT', 'files'=>'true')) !!}
                            <ul>
                                <li>
                                    <div class="customer-name">
                                        <div class="input-box name-firstname">
                                            <label > Nom<span class="required">*</span></label>
                                            <br>
                                            <input type="text" id="name" name="name" value="{{Auth::user()->name}}" title="First Name" class="input-text ">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="input-box name-lastname">
                                            <label > Prénon <span class="required">*</span> </label>
                                            <br>
                                            <input type="text" id="lastname" name="lastname" value="{{Auth::user()->lastname}}" title="Last Name" class="input-text">
                                            @if ($errors->has('lastname'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="customer-name">
                                        <div class="input-box name-firstname">
                                            <label > Adresse Email<span class="required">*</span></label>
                                            <br>
                                            <input type="text" id="email" name="email" title="First Name" class="input-text"value="{{Auth::user()->email}}">
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="input-box name-lastname">
                                            <label >Rôle <span class="required">*</span> </label>
                                            <br>
                                            <select  name="role" id="role" class="input-text ">
                                                @if(Auth::user()->role == '0')
                                                <option value="0" selected >Utilisateur</option>
                                                <option value="1">Organisateur</option>
                                                    @elseif(Auth::user()->role == '1')
                                                    <option value="0"  >Utilisateur</option>
                                                    <option value="1" selected>Organisateur</option>
                                                @else
                                                    <option value="0"  >Utilisateur</option>
                                                    <option value="1" >Organisateur</option>
                                                    <option value="2" selected>Administrateur</option>
                                                @endif
                                            </select>

                                        </div>
                                    </div>
                                </li>


                                <li>
                                    <br>
                                    <input type="hidden"  name="password" id="password"  class="input-text required-entry" value="{{Auth::user()->password}}">
                                </li>
                                <li>
                                    <br>
                                    <input type="hidden" name="password_confirmation" id="password_confirmation"  class="input-text required-entry" value="{{Auth::user()->password}}">

                                </li>





                            </ul>
                        </fieldset>

                    </div>
                </section>
                <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
                    <a  class="pull-right">
                        @if(Auth::user()->file == null)
                            <img title="profile image" class="img-circle img-responsive" src="{{asset('images/profile.png')}}" style="height: 256px">
                        @else
                            <img title="profile image" class="img-circle img-responsive" src="{{asset(Auth::user()->file)}}" style="height: 256px">
                        @endif</a>
                    <input type="hidden" name="file" value="{{Auth::user()->file}}">
                    {!! Form::file('userfile',null,['class'=>'form-control']) !!}
                </aside>
                <li>
                    <p class="require"><em class="required">* </em>Champs obligatoire</p>
                    <input type="text" name="hideit" id="hideit" value="">
                    <div class="buttons-set">
                        <button type="submit" title="Submit" class="button create-account"> <span> Changez </span> </button>
                    </div>
                </li>
                {!! Form::close() !!}


            </div>
        </div>
    </div>


    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

@stop