@extends('layouts.main')
<!-- Main -->
@section('content')
    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-12">
                    <div class="page-title">
                        <h2>Modifier Produit</h2>
                    </div>




        <div class="wizard-pane active" id="exampleAccount" role="tabpanel">
            <div class="page-content">
                <div class="projects-wrap">


                    <!-- Panel -->
                    <h4>Modifier les déclinaisons</h4>
                    @if (Session::has('message'))
                        <div class="'alert alert-success">{{Session::get('message')}}</div>
                    @endif
                    <div class="panel">
                        <div class="panel-body">
                            <div>
                            <a href="{{url('AjouterFrontDate/'.$Produit->id)}}">
                                <button  class="button submit" type="button">
                                    <i class="icon wb-plus" aria-hidden="true"></i> AJOUTER
                                </button></a>
                            </br>


                            </div>
                            <div class="toolbar"></div>


                            <table class="editable-table table table-striped" id="editableTable">


                                <thead>
                                <tr style="background: #af93f6;">
                                    <th><strong style="float: left; color: white">Date</strong></th>
                                    <th><strong style="float: left; color: white">Stock</strong></th>
                                    <th><strong style="float: left; color: white">Prix</strong></th>
                                    <th><strong style="float: left; color: white">Prix+TVA</strong></th>
                                    <th><strong style="float: left; color: white">Option 1</strong></th>
                                    <th><strong style="float: left; color: white">Prix</strong></th>
                                    <th><strong style="float: left; color: white">Option 2</strong></th>
                                    <th><strong style="float: left; color: white">Prix</strong></th>
                                    <th><strong style="float: left; color: white">Option 3</strong></th>
                                    <th><strong style="float: left; color: white">Prix</strong></th>
                                    <th><strong style="float: left; color: white">Action</strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Stock as $Stock)
                                    <tr>
                                        <td>{!! $Stock->Date !!}</td>
                                        <td>{!!$Stock->Stock!!}</td>
                                        <td>{!! $Stock->Prix0 !!}</td>
                                        <td>{!!$Stock->Prix0*1.2 !!}</td>
                                        <td>{!! $Stock->Option1!!}</td>
                                        <td>{!! $Stock->Prix1 !!}</td>
                                        <td>{!!$Stock->Option2 !!}</td>
                                        <td>{!!$Stock->Prix2!!}</td>
                                        <td>{!! $Stock->Option3 !!}</td>
                                        <td>{!!$Stock->Prix3 !!}</td>
                                        <td>
                                            {!! Form::open(array('route'=>['FrontStock.destroy',$Stock->id],'method'=>'DELETE')) !!}
                                            <button type="submit" class="btn btn-outline btn-danger">Effacer</button>

                                        {!! Form::close() !!}



                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="modal-body">

                        {!! Form::model($Produit,array('route'=>['AjouterProduit.update',$Produit->id],'method'=>'PUT','files'=>'true')) !!}

                                    <input type="hidden" name="Organisateur" id="Organisateur"  value="{{$Produit->Organisateur}}" class="form-control" placeholder="Email">


                            @if ($errors->has('Organisateur'))
                                <span class="alert-danger">
                                        <strong>{{ $errors->first('Organisateur') }}</strong>
                                    </span>
                            @endif

                        <div class="form-group">
                            <div class="form-group ">
                                <label for="select">Categorie</label>

                                <select class="form-control" name="Categorie" value=Categorie>
                                    @foreach($ssCategorie as $ssCategorie)

                                        <option value="{{$ssCategorie->id}}" >{{$ssCategorie->title}}</option>

                                    @endforeach
                                </select>

                            </div>
                        </div>
                        @if($Produit->Status==0)
                            <div class="radio-custom radio-default radio-inline">
                                <input type="radio" id="Status" name="Status" value="0"  checked />
                                <label for="inputBasicMale">Activer</label>
                            </div>

                            <div class="radio-custom radio-default radio-inline">
                                <input type="radio" id="Status" name="Status" value="1"/>
                                <label for="inputBasicFemale">Désactiver</label>
                            </div>
                        @else
                            <div class="radio-custom radio-default radio-inline">
                                <input type="radio" id="Status" name="Status" value="0"  />
                                <label for="inputBasicMale">Activer</label>
                            </div>

                            <div class="radio-custom radio-default radio-inline">
                                <input type="radio" id="Status" name="Status" value="1" checked>
                                <label for="inputBasicFemale">Désactiver</label>
                            </div>
                        @endif
                        <div class="form-group">
                            {!! Form::label('Titre','Titre') !!}
                            {!! Form::text('Title',null,['class'=>'form-control']) !!}
                            <div>  @if ($errors->has('Title'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('Title') }}</strong>
                                    </span>
                                @endif</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Description Principale','Description Principale') !!}
                            {!! Form::text('DescriptionMineur',null,['class'=>'form-control']) !!}
                            @if ($errors->has('DescriptionMineur'))
                                <span class="alert-danger">
                                        <strong>{{ $errors->first('DescriptionMineur') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('Description','Desciption') !!}
                            {!! Form::textarea('Description',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}
                            @if ($errors->has('Description'))
                                <span class="alert-danger">
                                        <strong>{{ $errors->first('Description') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <h4>Modifier les photos</h4>

                        <!-- Example Wizard Accordion -->
                        <div class="margin-bottom-30">
                            <div class="panel-group" id="exampleWizardAccordion" aria-multiselectable="true"
                                 role="tablist">
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeading1" role="tab">
                                        <a class="panel-title" data-toggle="collapse" href="#exampleCollapse1" data-parent="#exampleWizardAccordion"
                                           aria-expanded="true" aria-controls="exampleCollapse1">
                                            La photos principale
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse in" id="exampleCollapse1" aria-labelledby="exampleHeading1"
                                         role="tabpanel">
                                        <div class="panel-body">
                                            <img src="{{ asset($Produit->PhotosPrincipale) }}" height="150" />
                                            <input type="hidden" name="PhotosPrincipale" value="{{$Produit->PhotosPrincipale}}">
                                            {!! Form::file('PhotosPrincipale',null,['class'=>'form-control'],['value'=>'$Produit->PhotosPrincipale']) !!}                                    </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeading2" role="tab">
                                        <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse2"
                                           data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse2">
                                            La deuxième photos
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse" id="exampleCollapse2" aria-labelledby="exampleHeading2"
                                         role="tabpanel">
                                        <div class="panel-body">
                                            <img src="{{ asset($Produit->Photos1) }}" height="150" />
                                            <input type="hidden" name="Photos1" value="{{$Produit->Photos1}}">

                                            {!! Form::file('Photos1',null,['class'=>'form-control']) !!}

                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeading3" role="tab">
                                        <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse3"
                                           data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse3">
                                            La troisieme photos                                    </a>
                                    </div>
                                    <div class="panel-collapse collapse" id="exampleCollapse3" aria-labelledby="exampleHeading3"
                                         role="tabpanel">
                                        <div class="panel-body">
                                            <img src="{{ asset($Produit->Photos2) }}" height="150" />
                                            <input type="hidden" name="Photos2" value="{{$Produit->Photos2}}">

                                            {!! Form::file('Photos2',null,['class'=>'form-control']) !!}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($errors->has('PhotosPrincipale'))
                            <span class="alert-danger">
                                        <strong>Il faut ajouter au moins une photos principale</strong>
                                    </span>
                        @endif
                    <!-- End Example Wizard Accordion -->


                        <div class="form-group">
                            {!! Form::button('Modifier',['type'=>'submit','class'=>'button submit']) !!} <a href="{{ url('/AjouterProduit/') }}" class="btn btn-warning">Annuler</a>
                        </div>




                        {!! Form::close() !!}
                    </div>            </div>

        </div>

    </div>

                </div>
            </div>
        </div>
    </div>
    @stop