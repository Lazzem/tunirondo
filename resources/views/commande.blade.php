@extends('layouts.main')
<!-- Main -->
@section('content')

    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-9 wow bounceInUp animated">
                    <div class="page-title">
                        <h2>Vos commandes</h2>

                    </div>
                    @if ($message = Session::get('success'))
                        <div class="custom-alerts alert alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            {!! $message !!}
                        </div>
                        <?php Session::forget('success');?>
                    @endif
                    <fieldset>
                        <table id="product-review-table" class="data-table">

                            <thead>
                            <tr class="first last" style="background: #af93f6">
                                <th><span class="nobr"><strong style="float: left; color: white">N° facture</strong></span></th>
                                <th><span class="nobr"><strong style="float: left; color: white">Date</strong></span></th>
                                <th><span class="nobr"><strong style="float: left; color: white">Heure</strong></span></th>

                                <th><span class="nobr"><strong style="float: left; color: white">Montant</strong></span></th>
                                <th><span class="nobr"><strong style="float: left; color: white">Facture</strong></span></th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(!count($Commande))
                                <tr>Aucunne commande effectuer pour le moment</tr>
                            @else
                                @foreach($Commande as $commandes)
                                    @if($commandes->user_id == Auth::User()->id)
                                    <tr class="last odd" >

                                        <td class="value">{{$commandes->id}}</td>
                                        <td class="value">{{$commandes->created_at->format('d/m/Y')}}</td>
                                        <td class="value">{{$commandes->created_at->format('H:i')}}</td>

                                        <td class="value">${{$commandes->montant}}</td>
                                        <td class="value last"><a target ="_blank" href="{{ URL('facture/'.$commandes->id)}}"> <U>Voir la facture </U></a></td>
                                    </tr>
                                    @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>

                    </fieldset>



                </section>
            </div>
        </div>
    </div>
@stop