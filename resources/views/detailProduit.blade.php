@extends('layouts.main')



@section('content')
    <!-- main-container -->

<input type="hidden" class="short-description_hidden" id="PrixTotale_hidden" name="PrixTotale_hidden" value=''>

    <section class="main-container col1-layout">
        <div class="main container">
            <div class="col-main">
                <div class="row">
                    <div class="product-view">
                        <div class="product-essential">

                            <form id="myform"  action="/AjouteProduit" method="POST" name="major" >
                                {{ csrf_field() }}
                                <input type="hidden" name="opt1" value="">
                                <input type="hidden" name="opt2" value="">
                                <input type="hidden" name="opt3" value="">
                                <div class="product-img-box col-lg-6 col-sm-6 col-xs-12">

                                    <ul   class="moreview" id="moreview">

                                        <li class="moreview_thumb thumb_1">
                                            <img class="moreview_thumb_image" src="{{asset($produit->PhotosPrincipale)}}" alt="thumbnail">
                                            <img class="moreview_source_image" src="{{asset($produit->PhotosPrincipale)}}" alt="">
                                            <span class="roll-over">{{$produit->Title}}</span>
                                            <img  class="zoomImg" src="{{asset($produit->PhotosPrincipale)}}" alt="thumbnail">
                                        </li>


                                    @if($produit->Photos1 != null)
                                            <li class="moreview_thumb thumb_2 moreview_thumb_active">
                                                <img class="moreview_thumb_image" src="{{asset($produit->Photos1)}}" alt="thumbnail">
                                                <img class="moreview_source_image" src="{{asset($produit->Photos1)}}" alt="">
                                                <span class="roll-over">Roll over image to zoom in</span>
                                                <img  class="zoomImg" src="{{asset($produit->Photos1)}}" alt="thumbnail">
                                            </li>

                                        @endif
                                            @if($produit->Photos2 != null)
                                            <li class="moreview_thumb thumb_3">
                                                <img class="moreview_thumb_image" src="{{asset($produit->Photos2)}}" alt="thumbnail">
                                                <img class="moreview_source_image" src="{{asset($produit->Photos2)}}" alt="">
                                                <span class="roll-over">Roll over image to zoom in</span>
                                                <img  class="zoomImg" src="{{asset($produit->Photos2)}}" alt="thumbnail">
                                            </li>

                                            @endif
                                    </ul>
                                    <div class="moreview-control">
                                        <a href="javascript:void(0)" class="moreview-prev"></a> <a href="javascript:void(0)" class="moreview-next"></a> </div>
                                </div>
                                <div class="product-shop col-lg-6 col-sm-6 col-xs-12">
                                    <div class="product-name">
                                        <h1>{{$produit->Title}}</h1>
                                    </div>
                                    <div class="short-description">
                                        <h2>VUE RAPIDE</h2>
                                        <p>{{$produit->DescriptionMineur}}</p>
                                    </div>
                                    <div class="social">
                                        {{Form::open(array('url'=>'','files'=>true))}}
                                    <div class="price-block">
                                    <div class="short-description">
                                        <h2>Choisir une Date :
                                        <select name="date" id="date" >
                                            <option aria-checked="true" value=''>choisir date </option>
                                            @foreach($date as $dates)
                                              <option class="{{$dates->Stock}}" value="{{$dates->id}}">{{$dates->Date}}</option>


                                                @endforeach
                                        </select></h2>
                                            <span class="span_champs_obligatoire"> veuillez choisir une date </span>

                                            <div name="Stock"id="Stock">


                                            </div>
                                            <div name="PrixUnitaire"id="PrixUnitaire">


                                            </div>
                                        <div name="PrixTVA"id="PrixTVA">


                                        </div>

                                    <div class="price-block">
                                        <div class="short-description" name="suboption"id="suboption">

                                    </div>
                                        <input type="hidden" name="inputPrixTotale" id="inputPrixTotale" value="">

                                        <div class="short-description" id="PrixTotale" name="PrixTotale">


                                    </div>
                                        </div>
                                           {{Form::close()}}
                                    <div class="add-to-box">
                                        <div class="add-to-cart">
                                            <label for="qty">QUANTITÉ:</label>
                                            <div class="pull-left">
                                                <div class="custom pull-left">
                                                    <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="icon-minus">&nbsp;</i></button>
                                                    <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                                                    <button onClick="increasefunction()" class="increase items-count" type="button"><i class="icon-plus">&nbsp;</i></button>
                                                </div>
                                            </div>
                                           <a href="" >
                                               <button  class="button btn-cart" title="Ajouter au Panier" id="add_to_card" type="submit"><span><i class="icon-basket"></i> Ajouter au Panier</span></button></a>
                                            <div class="email-addto-box">

                                                <p class="email-friend"><a href="#" class=""><span>Email to a Friend</span></a></p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    </div>
                                </div>


                            </form>
                        </div>
                        <div class="product-collateral">
                            <div class="col-sm-12 wow bounceInUp animated">
                                <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                                    <li class="active"> <a href="#product_tabs_description" data-toggle="tab"> Description du produit</a> </li>
                                </ul>
                                <div id="productTabContent" class="tab-content">
                                    <div class="tab-pane fade in active" id="product_tabs_description">
                                        <div class="std">
                                            <p>
                                                {{$produit->Description}}
                                            </p>

                                        </div>
                                    </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>



    </section>
    <script type='text/javascript' src="{{asset('js/ajaxProduit.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/parallax.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.jcarousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/cloudzoom.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/common.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!--End main-container -->
    <link rel="stylesheet" href="{{URL::asset('css/animate.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/jquery.fancybox.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome.css')}}" type="text/css">

<script>
    $('#date').on('change',function(e){
        console.log(e);
        var date_id = e.target.value;

        //ajax
        $.get('/ajax_produit?date_id='+ date_id,function(data){
            $('#suboption').empty();
            $('#prix').empty();
            $('#PrixUnitaire').empty();
            $('#PrixTVA').empty();



            $('#Stock').empty();
            $('#PrixTotale').empty();

            $.each(data,function(index,subcat0bj){
                if ( (subcat0bj.Option1 != '')&&(subcat0bj.Option2 != '')&&(subcat0bj.Option3 != '')){
              $('#suboption').append('<h2>les options :</h2><table  class="data-table"><thead> <tr class="first last"> <th><span class="nobr">Nom de l\'option</span></th><th><span class="nobr">Prix</span></th> <th><span class="nobr">Choix</span></th> </tr> </thead><tbody><tr> ' +
                  '<th>'+ subcat0bj.Option1 +'</th>'+
                  '<td >$'+ subcat0bj.Prix1 +'</td>'+
                  '<td ><input type="checkbox" class="radio" value="'+ subcat0bj.Prix1 +'" id="Price_1" name="Price_1" ></td>'+

                  '</tr>'+
                  '<tr> ' +
                  '<th>'+ subcat0bj.Option2 +'</th>'+
                  '<td >$'+ subcat0bj.Prix2 +'</td>'+
                  '<td ><input type="checkbox" class="radio" value="'+ subcat0bj.Prix2 +'" id="Price_2" name="Price_2"></td>'+

                  '</tr>'+
                  '<tr> ' +
                  '<th>'+ subcat0bj.Option3 +'</th>'+
                  '<td >$'+ subcat0bj.Prix3 +'</td>'+
                  '<td ><input type="checkbox" class="radio" value="'+ subcat0bj.Prix3 +'" id="Price_3" name="Price_3" ></td>'+

                  '</tr></tbody></table>');}
                if ( (subcat0bj.Option1 != '')&&(subcat0bj.Option2 == '')&&(subcat0bj.Option3 == '')){
                    $('#suboption').append('<h2>les options :</h2><table  class="data-table"><thead> <tr class="first last"> <th><span class="nobr">Nom de l\'option</span></th><th><span class="nobr">Prix</span></th> <th><span class="nobr">Choix</span></th> </tr> </thead><tbody><tr> ' +
                        '<th>'+ subcat0bj.Option1 +'</th>'+
                        '<td >$'+ subcat0bj.Prix1 +'</td>'+
                        '<td ><input type="checkbox" class="radio option_value" value="'+ subcat0bj.Prix1 +'" id="Price_1" name="Price_1" ></td>'+

                        '</tr>'+
                        '</tbody></table>');}
               if ( (subcat0bj.Option1 != '')&&(subcat0bj.Option2 != '')&&(subcat0bj.Option3 == '')){
                    $('#suboption').append('<h2>les options :</h2><table  class="data-table"><thead> <tr class="first last"> <th><span class="nobr">Nom de l\'option</span></th><th><span class="nobr">Prix</span></th> <th><span class="nobr">Choix</span></th> </tr> </thead><tbody><tr> ' +
                        '<th>'+ subcat0bj.Option1 +'</th>'+
                        '<td >$'+ subcat0bj.Prix1 +'</td>'+
                        '<td ><input type="checkbox" class="radio option_value" value="'+ subcat0bj.Prix1 +'" id="Price_1" name="Price_1" ></td>'+

                        '</tr>'+
                        '<tr> ' +
                        '<th>'+ subcat0bj.Option2 +'</th>'+
                        '<td >$'+ subcat0bj.Prix2 +'</td>'+
                        '<td ><input type="checkbox" class="radio option_value" value="'+ subcat0bj.Prix2 +'" id="Price_2" name="Price_2"></td>'+

                        '</tr>'+
                        '</tbody></table>');}
                        if (subcat0bj.Stock > 0)
                        {$('.add-to-box').show();
              $('#Stock').append('<p class="availability in-stock">Disponibilité: <span>Disponible</span></p>');
                              }

                if (subcat0bj.Stock <= 0)
                {
                    $('#Stock').append('<p class="availability in-stock">Disponibilité: <span>Non Disponible</span></p>');
                    $('.add-to-box').hide();

                }
              $('#PrixTotale').append('<h2>Prix totale :</h2>'+
                  ' <p class="special-price"> <span class="price"> $'+subcat0bj.Prix0 *1.2+' </span> </p>');

                $('#PrixUnitaire').append('<h2>Prix : $'+subcat0bj.Prix0 +'</h2>');
                $('#PrixTVA').append('<h2>Prix TVA : $'+subcat0bj.Prix0*0.2 +'</h2>');
              $('#PrixTotale_hidden').val(subcat0bj.Prix0);
                $('#inputPrixTotale').val(subcat0bj.Prix0);

          });
        });
    });

    function increasefunction() {
        var quantite= $('select[name="date"] :selected').attr('class');
        var result = document.getElementById('qty');
        var qty = result.value;
        if( !isNaN( qty ))
            if(parseInt(quantite) > qty){
                result.value++;
                return false;
            }

    }




</script>
    <script>
        $(document).ready(function(){
            var total=0;

            $('#qty').change(function(){
                var quantite= $('select[name="date"] :selected').attr('class');
               if (parseInt($(this).val()) > quantite )
               {
                   $(this).val(quantite);
               }
            });


            $('#add_to_card').click(function(){
                var select_option =$('#date').val();
                if (select_option =='')
                {
                    $('#date').addClass('champs_obligatoire');
                    $('.span_champs_obligatoire').css('display','block');
                    return false;
                }
                else
                {
                    $('#date').removeClass('champs_obligatoire');
                    $('.span_champs_obligatoire').css('display','none');

                }               
            });

            $('#date').change(function(){
                if ($(this).val() !='')
                {
                    $(this).removeClass('champs_obligatoire');
                    $('.span_champs_obligatoire').css('display','none');
                }
            });



            $(document).on('change', '.option_value', function() {
            total=0;
             $('.option_value').each(function(){
                  if (this.checked)
                 {
                     var current_option = parseFloat($(this).val());
                     total=total+current_option;

                 }

             });



                var curreent_default_price =  parseFloat($('#PrixTotale_hidden').val());
            //alert(total);
            total=total+curreent_default_price*1.2;
            $('#PrixTotale').html('');
            $('#PrixTotale').html('<h2>Prix totale :</h2>'+
                  ' <p class="special-price"> <span class="price"> $'+ total+'</span> </p>');
                $('#inputPrixTotale').val(total);
            });




        });
    </script>


<style>
.champs_obligatoire
{
    color:red;
}

.span_champs_obligatoire
{
    color: red;
    display: none;
    margin-top: 16px;
}
</style>

@stop