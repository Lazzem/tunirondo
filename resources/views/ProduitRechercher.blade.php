@extends('layouts.main')
<!-- Main -->
@section('content')
    <div class="main-container col2-left-layout" xmlns="http://www.w3.org/1999/html">
        <div class="main container">
    @if(isset($details))
                <header class="blog_entry-header clearfix">
                <div class="blog_entry-header-inner">
                    @if( $query != '' )
       <a> <h4  class="blog_entry-title"> Les résultats de la recherche pour  <b> {{ $query }} </b> sont :</h4></a>
                        @endif
                </div>
        </header>
        <div class="category-products">

            <div class="toolbar">



                <div class="pager">

                    <div class="pages">


                        <ul class="pagination">
                            {{$details->render()}}
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="products-grid">
                @foreach($details as $produits)
                    <li class="item col-lg-3 col-md-5 col-sm-7 col-xs-6">
                        <div class="col-item">

                            <div class="product-image-area"> <a class="product-image" title="Sample Product" href="{{url('detailProduit/'.$produits->id)}}"> <img src="{{asset($produits->PhotosPrincipale)}}"  alt="a" width="300" height="300"/> </a>
                                <div class="hover_fly"> <a class="exclusive ajax_add_to_cart_button" href="{{url('ajouter-au-panier/'.$produits->id)}}" title="Add to cart">
                                        <div><i class="icon-shopping-cart"></i><span>Ajouter au panier</span></div>
                                    </a> <a class="quick-view" href="">
                                        <div><i class="icon-eye-open"></i><span>Vue rapide</span></div>

                                    </a> </div>
                            </div>
                            <div class="info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title=" Sample Product" href="{{url('detailProduit/'.$produits->id)}}">{{$produits->Title}}</a> </div>
                                    <!--item-title-->
                                    <div class="item-content">

                                        <div class="price-box">
                                            <p class="special-price"> <span class="price"> $45.00 </span> </p>

                                        </div>
                                    </div>
                                    <!--item-content-->
                                </div>
                                <!--info-inner-->

                                <div class="clearfix"> </div>
                            </div>
                        </div>

                    </li>
                @endforeach
            </ul>
            <div class="toolbar">



                <div class="pager">

                    <div class="pages">

                        <ul class="pagination">
                            {{$details->render()}}
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    @elseif(isset($message))
        <h2>{{ $message }}</h2>
    @endif
    </div>
    </div>

    @stop