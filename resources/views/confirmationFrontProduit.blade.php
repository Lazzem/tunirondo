@extends('layouts.main')
<!-- Main -->
@section('content')
    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-12">
                    <div class="page-title">
                        <h2>Confirmer ou ajouter une autre date</h2>
                    </div>
    <div class="page animsition">



        <table>
            <tr><div class="text-center margin-vertical-20">
                    <i class="icon wb-check font-size-40" aria-hidden="true"></i>
                    <h2>votre produit a été ajouter avec succès </h2>
                </div></tr>

            <tr><h3>Si vous voulez ajouter une autre date <a href="/FrontStock" ><U>cliquer ici</U></a></h3>
            </tr>
            <tr><td> <div  class="margin-bottom-20"><a href="{{url('AjouterProduit/'.Auth::User()->email)}}"><button class="button submit">Confirmer</button></a></div></td></tr>

        </table>

    </div> <!-- End Steps -->
    </div>
            </div>
        </div>
    </div>

    @stop