@extends('layouts.main')
<!-- Main -->
@section('content')

    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-9">
                    <div class="page-title">
                        <h2>Blog</h2>
                    </div>
                    <div class="blog-wrapper" id="main">
                        <div class="site-content" id="primary">
                            <div role="main" id="content">

                                    <article class="blog_entry clearfix">
                                        <header class="blog_entry-header clearfix">
                                            <div class="blog_entry">
                                              <a rel="bookmark">  <h2 class="blog_entry-title"> {{$Blog->title}} </h2></a>
                                            </div>
                                            <!--blog_entry-header-inner-->
                                        </header>
                                        <!--blog_entry-header clearfix-->
                                        <div class="entry-content">
                                            <div class="featured-thumb"><a href="#"><img alt="blog-img4" src="{{asset($Blog->file)}}" width="900"></a></div>
                                            <div class="entry-content" style="font-size: 16px;">
                                                {{$Blog->description}}
                                            </div>
                                        </div>
                                        <footer class="entry-meta" style="font-size: 16px;"> <strong>Ce Blog a été publiée a
                                                <time datetime="2014-07-10T06:53:43+00:00" class="entry-date">{{$Blog->created_at}}</time></strong>
                                            . </footer>
                                    </article>


                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@stop