@extends('layouts.main')
<!-- Main -->
@section('content')


    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-9 wow bounceInUp animated">
                    <div class="page-title">
                        <h2>contacter Nous</h2>
                    </div>
                    <div class="static-contain">
                        <form method="post" action="/envoyer">
                            {{ csrf_field() }}
                        <fieldset class="group-select">

                            <fieldset>

                                <legend>New Address</legend>
                                <input type="hidden" name="billing[address_id]" value="" id="billing:address_id">
                                <ul>
                                    <li>
                                        <div class="customer-name">
                                            <div class="input-box name-firstname">
                                                <label for="billing:firstname"> Nom<span class="required">*</span></label>
                                                <br>
                                                <input type="text" id="billing:firstname" name="nom"  title="First Name" class="input-text ">
                                            </div>
                                            <div class="input-box name-lastname">
                                                <label for="billing:lastname"> Prénom<span class="required">*</span> </label>
                                                <br>
                                                <input type="text" id="billing:lastname" name="prénom" value="" title="Last Name" class="input-text">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="input-box">
                                            <label for="billing:company">Telephone</label>
                                            <br>
                                            <input type="text" id="billing:company" name="Telephone" title="Company" class="input-text">
                                        </div>
                                        <div class="input-box">
                                            <label for="billing:email">Adresse E-mail <span class="required">*</span></label>
                                            <br>
                                            <input type="text" name="email" id="billing:email" title="Email Address" class="input-text validate-email">
                                        </div>
                                    </li>


                                    <li class="">
                                        <label for="comment">Commentaire<em class="required">*</em></label>
                                        <br>
                                        <div class="">
                                            <textarea name="message" id="comment" title="Comment" class="required-entry input-text" cols="5" rows="3"></textarea>
                                        </div>
                                    </li>
                                </ul>
                            </fieldset>

                            <li>
                                <p class="require"><em class="required">* </em>Required Fields</p>
                                <input type="text" name="hideit" id="hideit" value="">
                                <div class="buttons-set">
                                    <button type="submit" title="Submit" class="button submit"> <span>envoyer</span> </button>
                                </div>
                            </li>
                            </ul>
                        </fieldset></form>
                    </div>
                </section>
                <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
                    <div class="block block-company">
                        <div class="block-title">Company </div>
                        <div class="block-content">
                            <ol id="recently-viewed-items">

                                <li class="item last"><strong>Contactez nous</strong></li>
                            <li class="item odd"><a href="about_us.blade.php">À propos de nous</a></li>

                            <li class="item  odd"><a href="/faq">FaQs</a></li>
                            </ol>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>

@stop