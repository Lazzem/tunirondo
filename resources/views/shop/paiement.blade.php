@extends('layouts.main')



@section('content')
    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-9 wow bounceInUp animated">
                    <div class="page-title">
                        <h1> Paiement avec <img src="{{asset('images/sponsor/16.png')}}" alt="payment" height="60"> </h1>

                    </div>
                    <h4>Total à payer: $ {{ $total }} </h4>
                    <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : ''  }}">
                        {{ Session::get('error') }}
                        {{Session::forget('error')}}
                    </div>
                    <div class="static-contain">
                        <fieldset class="group-select">
                            <form action="{!! URL::route('addmoney.paypal') !!}" method="post" id="checkout-form">
                                <fieldset>
                                    <input id="id" type="hidden"type="number"  class="form-control" name="id" value="{{ Auth::User()->id }}" autofocus>

                                    <input id="amount" type="hidden"type="number"  class="form-control" name="amount" value="{{ $total }}" autofocus>
                                    @if ($errors->has('amount'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                    @endif
                                    <legend>New Address</legend>
                                    <input type="hidden" name="billing[address_id]" value="" id="billing:address_id">
                                    <ul>
                                        <li>
                                            <div class="customer-name">
                                                <div class="input-box name-firstname">
                                                    <label for="billing:firstname"> Nom</label>
                                                    <br>
                                                    <input type="text" required name="name" id="name" disabled="true" value="{{Auth::User()->name}}" title="nom" class="input-text ">
                                                </div>
                                                <div class="input-box name-lastname">
                                                    <label for="billing:lastname"> Prénom </label>
                                                    <br>
                                                    <input type="text" required name="lastname" id="lastname" disabled="true" value="{{Auth::User()->lastname}}" title="prénom" class="input-text">
                                                </div>
                                            </div>
                                        </li>
                                        <li>

                                            <div class="input-box">
                                                <label for="billing:email">Adresse E-mail </label>
                                                <br>
                                                <input  type="text" id="email" name="email" disabled="true" value="{{Auth::User()->email}}"  title=" Address email" class="input-text validate-email"required>
                                            </div>

                                        </li>



                                    </ul>
                                </fieldset>
                                {{ csrf_field() }}

                                <li>
                                </br>
                                    <div class="buttons-set">
                                        <button type="submit" class="button continue"><span>Payer avec Paypal</span></button>

                                    </div>
                                </li>
                                </ul>
                            </form>
                        </fieldset>
                    </div>
                </section>
                <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
                    <div class="block block-company">
                        <div class="block-title">PAIENMENT </div>
                        <div class="block-content">
                            <ol id="recently-viewed-items">
                                <li  class="item odd"><a href="/panier">Valider Panier</a></li>
                                <li class="item last"><strong>Paiement avec <img src="{{asset('images/payment-1.png')}}" alt="payment" height="25"> </strong></li>
                            </ol>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="{{ URL::to('src/js/paiement.js') }}"></script>
@endsection