@extends('layouts.main')
@section('content')


<div class="main-container col2-right-layout">
    <div class="main container">
        <div class="row">
            <section class="col-main col-sm-9 wow bounceInUp animated">
                <div class="page-title">
                    <h2> Paiement</h2>

                </div>
                <h4>Total à payer: {{ $total }} DT</h4>
                <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : ''  }}">
                    {{ Session::get('error') }}
                </div>
                <div class="static-contain">
                    <fieldset class="group-select">
                        <form action="{{ route('paiement') }}" method="post" id="checkout-form">
                        <fieldset>

                            <legend>New Address</legend>
                            <input type="hidden" name="billing[address_id]" value="" id="billing:address_id">
                            <ul>
                                <li>
                                    <div class="customer-name">
                                        <div class="input-box name-firstname">
                                            <label for="billing:firstname"> Nom<span class="required">*</span></label>
                                            <br>
                                            <input type="text" required name="name" value="" title="First Name" class="input-text ">
                                        </div>
                                        <div class="input-box name-lastname">
                                            <label for="billing:lastname"> Prénom<span class="required">*</span> </label>
                                            <br>
                                            <input type="text" required name="prénom" value="" title="Last Name" class="input-text">
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-box">
                                        <label for="billing:company">Telephone</label>
                                        <br>
                                        <input type="text" name="tel" value="" title="Company" class="input-text">
                                    </div>
                                    <div class="input-box">
                                        <label for="billing:email">Adresse E-mail <span class="required">*</span></label>
                                        <br>
                                        <input value="{{user()->email}}" type="email"  name="address"title="Email Address" class="input-text validate-email"required>
                                    </div>
                                </li>



                            </ul>
                        </fieldset>
                            {{ csrf_field() }}

                        <li>
                            <p class="require"><em class="required">* </em>Champs obligatoires</p>
                            <input type="text" name="hideit" id="hideit" value="">
                            <div class="buttons-set">
                                <button type="submit" title="Submit" class="button submit"> <span> Payer </span> </button>
                            </div>
                        </li>
                        </ul>
                        </form>
                    </fieldset>
                </div>
            </section>
            <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
                <div class="block block-company">
                    <div class="block-title">PAIENMENT </div>
                    <div class="block-content">
                        <ol id="recently-viewed-items">
                            <li  class="item odd"><a href="/panier">Valider Panier</a></li>
                            <li class="item last"><strong>Paiement </strong></li>
                        </ol>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>
@endsection