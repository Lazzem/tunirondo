@extends('layouts.main')
<!-- Main -->
@section('content')
    <section class="content-wrapper">
        <div class="container">
            <div class="std">
                <div class="page-not-found wow bounceInRight animated">
                    <h2>404</h2>
                    <h3><img src="{{asset('images/signal.png')}}" alt="signal">Oops! La page que vous avez demandée n'a pas été trouvée!</h3>
                    <div><a href="/" class="btn-home"><span>Retour à l'accueil</span></a></div>
                </div>
            </div>
        </div>
    </section>

    @stop