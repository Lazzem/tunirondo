@extends('layouts.main')
<!-- Main -->
@section('content')
    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-12">
                    <div class="page-title">
                        <h2>vos évenement</h2>
                    </div>
<div style="float: right">
    {{link_to_route('AjouterProduit.create','Ajouter',[Auth::user()->id],['class'=>'buy-btn'])}}
</div>
                </div>


        @if($produit->count())
        <div class="category-products">

            <div class="toolbar">
                <div class="sorter">




                </div>
            </div>
            <ul class="products-grid">
                @foreach($produit as $produits)

                    <li class="item col-lg-3 col-md-5 col-sm-7 col-xs-6">
                        <div class="col-item">
                            <div class="sale-label sale-top-right">
                                {!! Form::open(array('route'=>['AjouterProduit.destroy',$produits->id],'method'=>'DELETE')) !!}
                                <button type="submit" class="btn btn-outline btn-danger"><i class="icon wb-trash" aria-hidden="true"></i></button>

                                {!! Form::close() !!}
                                </div>

                            <div class="product-image-area"> <a class="product-image" title="Afficher Produit" href="{{url('detailProduit/'.$produits->id)}}"> <img src="{{asset($produits->PhotosPrincipale)}}"  alt="a" width="300" height="250"/> </a>
                                <div class="hover_fly"> <a class="exclusive ajax_add_to_cart_button" href="{{route('AjouterProduit.edit',$produits->id)}}" title="Modifier Produit">
                                        <div><span><i class="icon wb-settings" aria-hidden="true"></i>Modifier</span></div>
                                    </a> <a class="quick-view" href="{{url('detailProduit/'.$produits->id)}}">
                                        <div><i class="icon-eye-open"></i><span>Afficher</span></div>

                                    </a> </div>
                            </div>
                            <div class="info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title=" Afficher Produit" href="{{url('detailProduit/'.$produits->id)}}">{{$produits->Title}}</a> </div>
                                    <!--item-title-->
                                    <div class="item-content">

                                        <div class="price-box">
                                            <p class="special-price"> <span class="price"> $45.00 </span> </p>

                                        </div>
                                    </div>
                                    <!--item-content-->
                                </div>
                                <!--info-inner-->

                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
                        <div class="toolbar">




                            <div class="pages">


                                <ul class="pagination" style="float: right;">
                                    {{$produit->render()}}
                                </ul>
                            </div>
                        </div>
                </div>

        </div>
    @else
                <div class="category-products">
                    <li class="item col-lg-6 col-md-5 col-sm-7 col-xs-6">

        <h2>vous n'avez aucun évenement</h2>
                    </li>
                </div>
    @endif
                </div>
            </div>
        </div>
    </div>
    <link rel="apple-touch-icon" href="{{asset('../BackOffice/assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('../BackOffice/assets/images/favicon.ico')}}">
@stop