@extends('layouts.backmain')
@section('content')
    <div class="page animsition">
        <div class="page-content">
            <!-- Panel Basic -->


            <div class="panel">
                <header class="panel-heading">
                    <div class="panel-actions"></div>
                    <h3 class="panel-title">Les commandes</h3>
                </header>

                <div class="panel-body">
                    <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">

                        <thead>
                        <tr>
                            <th>Id Commande</th>
                            <th>Id Utilisateur</th>
                            <th>Date</th>
                            <th>Heure</th>

                            <th>Montant</th>
                            <th>Facture</th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Id Commande</th>
                            <th>Id Utilisateur</th>
                            <th>Date</th>
                            <th>Heure</th>

                            <th>Montant</th>
                            <th>Facture</th>
                        </tr>


                        </tfoot>
                        <tbody>





                        @foreach ($commande as $commandes)

                            <tr>
                                <td>{{$commandes->id}}</td>

                                <td>{{$commandes->user_id}}</td>

                                <td>{{$commandes->created_at->format('d/m/Y')}}</td>
                                <td>{{$commandes->created_at->format('H:i')}}</td>
                                <td>${{$commandes->montant}}</td>
                                <td><a href="{{ URL('facture/'.$commandes->payment_id)}}"><U>Voir Facture</U></a></td>



                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            @if (Session::has('message'))
                <div class="'alert alert-success">{{Session::get('message')}}</div>
            @endif

            <div class="panel">
                <header class="panel-heading">
                    <div class="panel-actions"></div>
                    <h3 class="panel-title">Les commandes échouer</h3>
                </header>

                <div class="panel-body">
                    <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">

                        <thead>
                        <tr>
                            <th>Id Commande</th>
                            <th>Id Utilisateur</th>
                            <th>Date</th>
                            <th>Heure</th>

                            <th>Montant</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Id Commande</th>
                            <th>Id Utilisateur</th>
                            <th>Date</th>
                            <th>Heure</th>

                            <th>Montant</th>
                            <th>Action</th>
                        </tr>


                        </tfoot>
                        <tbody>
                        @foreach ($commandeEchouer as $commandesechou)

                            <tr>
                                <td>{{$commandesechou->id}}</td>

                                <td>{{$commandesechou->user_id}}</td>

                                <td>{{$commandesechou->created_at->format('d/m/Y')}}</td>
                                <td>{{$commandesechou->created_at->format('H:i')}}</td>
                                <td>${{$commandesechou->montant}}</td>
                                <td>{!! Form::open(array('route'=>['BCommande.destroy',$commandesechou->id],'method'=>'DELETE')) !!}
                                    <button type="submit" class="btn btn-outline btn-danger"><i class="icon wb-trash" aria-hidden="true"></i></button>

                                    {!! Form::close() !!}</td>



                            </tr>
                        @endforeach







                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End Panel Basic -->
        </div>
    </div>


    <script>
        (function(document, window, $) {
            'use strict';

            var Site = window.Site;

            $(document).ready(function($) {
                Site.run();
            });

            // Fixed Header Example
            // --------------------
            (function() {
                // initialize datatable
                var table = $('#exampleFixedHeader').DataTable({
                    responsive: true,
                    "bPaginate": false,
                    "sDom": "t" // just show table, no other controls
                });

                // initialize FixedHeader
                var offsetTop = 0;
                if ($('.site-navbar').length > 0) {
                    offsetTop = $('.site-navbar').eq(0).innerHeight();
                }
                var fixedHeader = new FixedHeader(table, {
                    offsetTop: offsetTop
                });

                // redraw fixedHeaders as necessary
                $(window).resize(function() {
                    fixedHeader._fnUpdateClones(true);
                    fixedHeader._fnUpdatePositions();
                });
            })();

            // Individual column searching
            // ---------------------------
            (function() {
                $(document).ready(function() {
                    var defaults = $.components.getDefaults("dataTable");

                    var options = $.extend(true, {}, defaults, {
                        initComplete: function() {
                            this.api().columns().every(function() {
                                var column = this;
                                var select = $(
                                    '<select class="form-control width-full"><option value=""></option></select>'
                                )
                                    .appendTo($(column.footer()).empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '',
                                                true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(
                                    d, j) {
                                    select.append('<option value="' + d +
                                        '">' + d + '</option>')
                                });
                            });
                        }
                    });

                    $('#exampleTableSearch').DataTable(options);
                });
            })();

            // Table Tools
            // -----------
            (function() {
                $(document).ready(function() {
                    var defaults = $.components.getDefaults("dataTable");

                    var options = $.extend(true, {}, defaults, {
                        "aoColumnDefs": [{
                            'bSortable': false,
                            'aTargets': [-1]
                        }],
                        "iDisplayLength": 5,
                        "aLengthMenu": [
                            [5, 10, 25, 50, -1],
                            [5, 10, 25, 50, "All"]
                        ],
                        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
                        "oTableTools": {
                            "sSwfPath": "../../assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
                        }
                    });

                    $('#exampleTableTools').dataTable(options);
                });
            })();

            // Table Add Row
            // -------------

            (function() {
                $(document).ready(function() {
                    var defaults = $.components.getDefaults("dataTable");

                    var t = $('#exampleTableAdd').DataTable(defaults);

                    $('#exampleTableAddBtn').on('click', function() {
                        t.row.add([
                            'Adam Doe',
                            'New Row',
                            'New Row',
                            '30',
                            '2015/10/15',
                            '$20000'
                        ]).draw();
                    });
                });
            })();
        })(document, window, jQuery);
    </script>


@stop