@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Le menu du site </h1>
            <div class="page-header-actions">
                <form action="/BackRechercheMenu" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="q" placeholder="Recherche...">
                    </div>
                </form>
            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">
                <ul class="blocks blocks-100 blocks-xlg-5 blocks-md-4 blocks-sm-3 blocks-xs-2">

                    @if (Session::has('message'))
                        <div class="'alert alert-success">{{Session::get('message')}}</div>
                    @endif

                        @if(isset($details))
                            <p> Les résultats de la recherche pour  <b> {{ $query }} </b> sont :</p>

                            @foreach($details as $menus)

                                <li>
                                    <div class="panel">
                                        <figure class="overlay overlay-hover animation-hover">
                                            <img class="caption-figure" src="{{asset('images/menu.png')}}" height="140" width="170">
                                            <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                                                <div class="btn-group">

                                                    {{link_to_route('Bmenu.edit','Edit',[$menus->id],['class'=>'btn btn-icon btn-pure btn-default'])}}

                                                    {!! Form::open(array('route'=>['Bmenu.destroy',$menus->id],'method'=>'DELETE')) !!}
                                                    {!! Form::button('Delete',['type'=>'submit','class'=>'btn btn-icon btn-pure btn-default']) !!}
                                                    {!! Form::close() !!}
                                                </div>

                                            </figcaption>
                                        </figure>
                                        <div class="time pull-right">{{$menus->created_at}}</div>
                                        <div>{{str_limit($menus->title, $limit = 10, $end = '..')}}</div>
                                    </div>
                                </li>


                            @endforeach

                        @elseif(isset($message))
                            <p>{{ $message }}</p>
                        @endif

                </ul>
            </div>




        </div>
    </div>


    <!--add project form -->
    {{link_to_route('Bmenu.create','Add',null,['class'=>'site-action btn-raised btn btn-success btn-floating'])}}


    </div>
    </div>
    </div>

@stop
