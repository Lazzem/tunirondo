@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">{{$Blogs->title}}</h1>
            <div class="page-header-actions">
                <form>
                    <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="" placeholder="Search...">
                    </div>
                </form>
            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">


                <form class="form-horizontal">

                    <img src="{{ asset($Blogs->file) }}" height="150" />
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{ $Blogs->title }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{ $Blogs->description }}</p>
                        </div>
                    </div>

                    <a href="{{ url('/BBlog/'.$Blogs->id.'/edit') }}" class="btn btn-warning">Edit</a>
                    <a href="{{ url('/BBlog') }}" class="btn btn-warning">&lt;Back</a>

                </form>

            </div>
        </div>
    </div>
@stop