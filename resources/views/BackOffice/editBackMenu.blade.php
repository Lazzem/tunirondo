@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Modifier le menu  </h1>

        </div>
        <div class="page-content">
            <div class="projects-wrap">


                <div class="modal-body">

                    {!! Form::model($menu,array('route'=>['Bmenu.update',$menu->id],'method'=>'PUT')) !!}



                    <div class="form-group">
                        <label for="title">Title</label>
                        {!! Form::text('title',null,['class'=>'form-control']) !!}
                    </div>



                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                    <a href="{{ url('/Bmenu') }}" class="btn btn-warning">Annuler</a>


                    {!! Form::close() !!}
                </div>

                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>












@stop