@extends('layouts.backmain')
@section('content')
  <div class="page animsition">
    <div class="page-content">
      <!-- Panel Basic -->
      @if (Session::has('message'))
        <div class="'alert alert-success">{{Session::get('message')}}</div>
      @endif


      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">Les produits</h3>
        </header>

        <div class="panel-body">
          <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">

            <thead>
            <tr>
                <th>Titre</th>
              <th>Sous Categorie</th>
              <th>Organisateur</th>
              <th>Quantité</th>
              <th>Date</th>
              <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>Titre</th>
              <th>Sous Categorie</th>
              <th>Organisateur</th>
              <th>Quantité</th>
              <th>Date</th>
              <th>Action</th>
            </tr>


            </tfoot>
            <tbody>





                @foreach ($resultat as $resultat)
                  <input type="hidden" value="{{$id++}}">
                  <tr>
                      <td>{{$resultat->Title}}</td>

                      <td> @foreach ($sscategory as $sscateg)
                              @if($sscateg->id == $resultat->Categorie)
                                  {{$sscateg->title}}
                               @endif
                               @endforeach
                      </td>
                <td>{{$resultat->Organisateur}}</td>
             <td>{{$resultat->Stock}}</td>
                      <td>{{$resultat->Date}}</td>

                      <td>
                          <table>
                              <tr>
                  <td><button class="btn btn-outline btn-primary" data-target="#exampleTabs{{$id}}" data-toggle="modal"
                          type="button"><i class="icon wb-eye" aria-hidden="true"></i> </button></td>
                <td>
                    <a href="{{route('BProduits.edit',$resultat->idProduit)}}"> <button type="button" class="btn btn-outline btn-success"><i class="icon wb-settings" aria-hidden="true"></i></button>
                    </a>
                </td>

                <td>{!! Form::open(array('route'=>['BProduits.destroy',$resultat->idProduit],'method'=>'DELETE')) !!}
                          <button type="submit" class="btn btn-outline btn-danger"><i class="icon wb-trash" aria-hidden="true"></i></button>

                {!! Form::close() !!}</td></tr></table>

              <!-- Modal -->
              <div class="modal fade" id="exampleTabs{{$id}}" aria-hidden="true" aria-labelledby="exampleModalTabs"
                   role="dialog" tabindex="-1">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header"    style="margin-left:50px;" >
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                        <table>
                            <tr><td><h3>Titre :</h3><h4 class="modal-title" id="exampleModalTabs">{{$resultat->Title}}</h4>

                           <td>        </td> </td><td colspan="3"><img src="{{$resultat->PhotosPrincipale}}" width="200" height="100"></td></tr></table>
                    </div>


                      <div class="">
                        <div class="" style="margin: 57px"id="exampleLine1" role="tabpanel">
                            <h3>Description principale :</h3>
                          {{$resultat->DescriptionMineur}}
                            <h3>Deuxième description</h3>
                            {{$resultat->Description}}


                        </div>

                        <div class=""style="margin: 57px" id="exampleLine2" role="tabpanel">
                          <h4> Date : {{$resultat->Date}}</h4>
                            <h4> Stock : {{$resultat->Stock}}</h4>
                            <h4> Status : @if ($resultat->Status ==0)
                            Activer
                            @else Désactiver @endif</h4>
                            <h4>Prix Intitial:{{$resultat->Prix0}} </h4>



                        </div>

                        <div class="" style="margin: 57px" id="exampleLine3" role="tabpanel">
                            <img src="{{$resultat->Photos1}}" width="50" height="50">
                            <img src="{{$resultat->Photos2}}" width="50" height="50">

                        </div>
                      </div>

                  </div>
                </div>
              </div>
              <!-- End Modal -->
                      </td>
                  </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
  </div>


  <script>
      (function(document, window, $) {
          'use strict';

          var Site = window.Site;

          $(document).ready(function($) {
              Site.run();
          });

          // Fixed Header Example
          // --------------------
          (function() {
              // initialize datatable
              var table = $('#exampleFixedHeader').DataTable({
                  responsive: true,
                  "bPaginate": false,
                  "sDom": "t" // just show table, no other controls
              });

              // initialize FixedHeader
              var offsetTop = 0;
              if ($('.site-navbar').length > 0) {
                  offsetTop = $('.site-navbar').eq(0).innerHeight();
              }
              var fixedHeader = new FixedHeader(table, {
                  offsetTop: offsetTop
              });

              // redraw fixedHeaders as necessary
              $(window).resize(function() {
                  fixedHeader._fnUpdateClones(true);
                  fixedHeader._fnUpdatePositions();
              });
          })();

          // Individual column searching
          // ---------------------------
          (function() {
              $(document).ready(function() {
                  var defaults = $.components.getDefaults("dataTable");

                  var options = $.extend(true, {}, defaults, {
                      initComplete: function() {
                          this.api().columns().every(function() {
                              var column = this;
                              var select = $(
                                  '<select class="form-control width-full"><option value=""></option></select>'
                              )
                                  .appendTo($(column.footer()).empty())
                                  .on('change', function() {
                                      var val = $.fn.dataTable.util.escapeRegex(
                                          $(this).val()
                                      );

                                      column
                                          .search(val ? '^' + val + '$' : '',
                                              true, false)
                                          .draw();
                                  });

                              column.data().unique().sort().each(function(
                                  d, j) {
                                  select.append('<option value="' + d +
                                      '">' + d + '</option>')
                              });
                          });
                      }
                  });

                  $('#exampleTableSearch').DataTable(options);
              });
          })();

          // Table Tools
          // -----------
          (function() {
              $(document).ready(function() {
                  var defaults = $.components.getDefaults("dataTable");

                  var options = $.extend(true, {}, defaults, {
                      "aoColumnDefs": [{
                          'bSortable': false,
                          'aTargets': [-1]
                      }],
                      "iDisplayLength": 5,
                      "aLengthMenu": [
                          [5, 10, 25, 50, -1],
                          [5, 10, 25, 50, "All"]
                      ],
                      "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
                      "oTableTools": {
                          "sSwfPath": "../../assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
                      }
                  });

                  $('#exampleTableTools').dataTable(options);
              });
          })();

          // Table Add Row
          // -------------

          (function() {
              $(document).ready(function() {
                  var defaults = $.components.getDefaults("dataTable");

                  var t = $('#exampleTableAdd').DataTable(defaults);

                  $('#exampleTableAddBtn').on('click', function() {
                      t.row.add([
                          'Adam Doe',
                          'New Row',
                          'New Row',
                          '30',
                          '2015/10/15',
                          '$20000'
                      ]).draw();
                  });
              });
          })();
      })(document, window, jQuery);
  </script>

  {{link_to_route('BProduits.create','Ajout',null,['class'=>'site-action btn-raised btn btn-success btn-floating'])}}

@stop