@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Ajouter categorie </h1>

        </div>
        <div class="page-content">
            <div class="projects-wrap">


                 <div class="modal-body">

                    {!! Form::open(['url'=>'/BackCategorie', 'method'=>'POST', 'files'=>'true']) !!}

                     <div class="form-group">
                         <label for="userfile">Image File</label>
                         <input type="file" class="form-control" name="userfile">
                     </div>

                     <div class="form-group">
                         <label for="title">Title</label>
                         <input type="text" class="form-control" name="title" value="">
                     </div>
                     <div class="form-group">
                         <div class="form-group ">
                             <label for="select">Affichage sur le menu</label>

                             <select class="form-control" name="idmenu" value=idmenu>
                                 <option value="">Aucun </option>
                                 @foreach($menu as $menus)

                                     <option value="{{$menus->id}}" >{{$menus->title}}</option>

                                 @endforeach
                             </select>

                         </div>
                     </div>


                     <div class="form-group">
                         <label for="description">Description</label>
                         <textarea class="form-control" name="description"></textarea>
                     </div>

                     <div class="form-group">
                         <button type="submit" class="btn btn-primary">Upload</button>
                         <a href="{{ url('/BackCategorie') }}" class="btn btn-warning">Cancel</a>
                     </div>


                    {!! Form::close() !!}
                 </div>

                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>












@stop