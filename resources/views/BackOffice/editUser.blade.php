@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Editer Utilisateur </h1>

        </div>
        <div class="page-content">
            <div class="projects-wrap">


                 <div class="modal-body">

                    {!! Form::model($User,array('route'=>['user.update',$User->id],'method'=>'PUT')) !!}

                     <div class="form-group">
                         {!! Form::label('Nom','Nom ') !!}
                         {!! Form::text('name',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         {!! Form::label('Prénom','Prenom ') !!}
                         {!! Form::text('lastname',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         <label for="activated">Role</label>
                         <select class="form-control" name="role" value="role" >
                             @if($User->role==0)
                                     <option value="0" selected="true" >Client</option>
                                     <option value="1" >Organisateur</option>
                                 <option value="2" >Administrateur</option>
                             @endif
                             @if($User->role==1)
                                     <option value="0" >Client</option>
                                     <option value="1" selected="true" >Organisateur</option>
                                     <option value="2"  >Administrateur</option>
                             @endif
                                 @if($User->role==2)
                                     <option value="0" >Client</option>
                                     <option value="1" >Organisateur</option>
                                     <option value="2" selected="true" >Administrateur</option>
                                 @endif
                         </select>
                     </div>

                     <div class="form-group">
                         {!! Form::label('e-mail','E-mail ') !!}
                         {!! Form::text('email',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         <input type="hidden" name="password" value="{{$User->password}}">
                         <input type="hidden" name="password_confirmation" value="{{$User->password}}">

                     </div>
                     @if($errors->has())
                         <div class="alert alert-danger">
                             @foreach($errors->all() as $error)
                                 <p>{{ $error }}</p>
                             @endforeach
                         </div>
                     @endif

                     <div class="form-group">
                         {!! Form::button('Editer',['type'=>'submit','class'=>'btn btn-primary']) !!}
                     </div>


                    {!! Form::close() !!}
                 </div>



            </div>
        </div>
    </div>












@stop