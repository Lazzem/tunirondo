@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Blog </h1>
            <div class="page-header-actions">
                <form>
                    <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="" placeholder="Search...">
                    </div>
                </form>
            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">
                <ul class="blocks blocks-100 blocks-xlg-5 blocks-md-4 blocks-sm-3 blocks-xs-2">

                    @if (Session::has('message'))
                        <div class="'alert alert-success">{{Session::get('message')}}</div>
                    @endif

                    @foreach($blog as $Categories)

                        <li>
                            <div class="panel">
                                <figure class="overlay overlay-hover animation-hover">
                                    <img class="caption-figure" src="{{asset($Categories->file)}}" height="140" width="170">
                                    <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                                        <div class="btn-group">

                                            {{link_to_route('BBlog.edit','Edit',[$Categories->id],['class'=>'btn btn-icon btn-pure btn-default'])}}

                                            {!! Form::open(array('route'=>['BBlog.destroy',$Categories->id],'method'=>'DELETE')) !!}
                                            {!! Form::button('Delete',['type'=>'submit','class'=>'btn btn-icon btn-pure btn-default']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                        {{link_to_route('BBlog.show','Afficher Blog',[$Categories->id],['class'=>'btn btn-outline btn-default project-button'])}}

                                    </figcaption>
                                </figure>
                                <div class="time pull-right">{{$Categories->created_at}}</div>
                                <div>{{str_limit($Categories->title, $limit = 10, $end = '..')}}</div>
                            </div>
                        </li>

                    @endforeach

                </ul>
            </div>


            <nav>
                <ul class="pagination pagination-no-border">
                    <li class="disabled">
                        <a href="javascript:void(0)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
                    </li>
                    <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
                    <li><a href="javascript:void(0)">2</a></li>
                    <li><a href="javascript:void(0)">3</a></li>
                    <li><a href="javascript:void(0)">4</a></li>
                    <li><a href="javascript:void(0)">5</a></li>
                    <li><a href="javascript:void(0)" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                </ul>
            </nav>

        </div>
    </div>


    <!--add project form -->
    {{link_to_route('BBlog.create','Add',null,['class'=>'site-action btn-raised btn btn-success btn-floating'])}}


    </div>
    </div>
    </div>

@stop