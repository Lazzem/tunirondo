@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Ajouter utilisateur </h1>
            <div class="page-header-actions">

            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">


                 <div class="modal-body">

                    {!! Form::open(array('route'=>'user.store')) !!}

                     <div class="form-group">
                         {!! Form::label('Nom','Nom ') !!}
                         {!! Form::text('name',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         {!! Form::label('Prénom','Prenom ') !!}
                         {!! Form::text('lastname',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         <label for="activated">Role</label>
                         <select class="form-control" name="role" value="role" >

                                 <option value="0"  >Utilisateur</option>
                                 <option value="1" >Organisateur</option>
                                 <option value="2" >Administrateur</option>

                         </select>
                     </div>

                     <div class="form-group">
                         {!! Form::label('e-mail','E-mail ') !!}
                         {!! Form::text('email',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         {!! Form::label('mot de passe','mot de passe') !!}
                         <input type="password" name="password" id="password_confirmation"  class="form-control" value="">

                     </div>
                     <div class="form-group">
                         {!! Form::label('Confirmer Mot de passe','Confirmer Mot de passe ') !!}
                         <input type="password" name="password_confirmation" id="password_confirmation"  class="form-control" value="">

                     </div>

                       <div class="form-group">
                           {!! Form::button('Ajouter',['type'=>'submit','class'=>'btn btn-primary']) !!}
                       </div>


                    {!! Form::close() !!}
                 </div>

                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>












@stop