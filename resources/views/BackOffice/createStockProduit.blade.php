@extends('layouts.Backmain')
@section('content')

    <div class="page animsition">

    <div class="steps steps-sm row" data-plugin="matchHeight" data-by-row="true" role="tablist">
        <div class="step col-md-4 done " data-target="#exampleBilling" role="tab">
            <span class="step-number">1</span>
            <div class="step-desc">
                <span class="step-title">Produit</span>
                <p>Ajoute produit</p>
            </div>
        </div>

        <div class="step col-md-4 current" data-target="#exampleAccount" role="tab">
            <span class="step-number">2</span>
            <div class="step-desc">
                <span class="step-title">Les Dates</span>
                <p>Ajoute déclinaison </p>
            </div>
        </div>

        <div class="step col-md-4 " data-target="#exampleGetting" role="tab">
            <span class="step-number">3</span>
            <div class="step-desc">
                <span class="step-title">Confirmation</span>

            </div>
        </div>
    </div>



       <div class="page-content container-fluid">
            <div class="row">
        <div class="col-md-6" style="float: left">
        <div class="modal-body">
            {!! Form::open(array('route'=>'BStock.store','name'=>'Form1')) !!}

           <div class="form-group">
                {!! Form::label('Date ','Date :') !!}
                {!! Form::date('Date',null,['class'=>'form-control']) !!}
               @if ($errors->has('Date'))
                   <span class="alert-danger">
                                        <strong>{{ $errors->first('Date') }}</strong>
                                    </span>
               @endif
            </div>
            <div class="form-group">
    <input type="hidden" name="idProduit" value="{{$idProduit}}">
</div>
            <div class="form-group">
                {!! Form::label('Stock','Stock :') !!}
                {!! Form::number('Stock',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}
                @if ($errors->has('Stock'))
                    <span class="alert-danger">
                                        <strong>{{ $errors->first('Stock') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Prix :</label>
                <div class="col-sm-9">
                    <input type="text" style="width: 392px;" name="Prix0" class="form-control" id="inputCurrency" data-plugin="formatter"
                           data-pattern="DT[[999]],[[999]].[[99]]"  onKeyUp='CalculerMontantTTC()'/>

                    @if ($errors->has('Prix0'))
                        <span class="alert-danger">
                                        <strong>{{ $errors->first('Prix0') }}</strong>
                                    </span>
                    @endif
                    <p class="help-block">$999,999.99</p>
                </div>

            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Prix avec TVA :</label>
                <div class="col-sm-9">
                    <input type="number"style="width: 392px;" name="PrixTVA" disabled="true" class="form-control" step="any" id="inputCurrency" data-plugin="formatter"
                           value="">
                    <p class="help-block">$999,999.99     &&     TVA = 20% </p>
                </div>

            </div>


        </div>

        </div>

            <!-- Example Wizard Accordion -->
            <div class="col-md-6" style="float: right">
            <div class="margin-bottom-30">
                <div class="panel-group" id="exampleWizardAccordion" aria-multiselectable="true"
                     role="tablist">
                    <div class="panel">
                        <div class="panel-heading" id="exampleHeading1" role="tab">
                            <a class="panel-title" data-toggle="collapse" href="#exampleCollapse1" data-parent="#exampleWizardAccordion"
                               aria-expanded="true" aria-controls="exampleCollapse1">
                                Ajouter  un deuxième prix
                            </a>
                        </div>
                        <div class="panel-collapse collapse in" id="exampleCollapse1" aria-labelledby="exampleHeading1"
                             role="tabpanel">
                            <div class="panel-body">
                                {!! Form::label('nom','Nom de l\'option n°1 :') !!}
                                        {!! Form::text('Option1',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}
                                {!! Form::label('Prix','Prix :') !!}

                                <input type="text" name="Prix1" class="form-control" step="any" id="inputCurrency" data-plugin="formatter"
                                       data-pattern="DT[[999]],[[999]].[[99]]" />
                                <p class="help-block">$999,999.99</p>
                                @if ($errors->has('Prix1'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('Prix1') }}</strong>
                                    </span>
                                @endif</div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading" id="exampleHeading2" role="tab">
                            <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse2"
                               data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse2">
                                Ajouter un troisieme prix
                            </a>
                        </div>
                        <div class="panel-collapse collapse" id="exampleCollapse2" aria-labelledby="exampleHeading2"
                             role="tabpanel">
                            <div class="panel-body">
                                {!! Form::label('nom','Nom de l\'option n°2 :') !!}
                                {!! Form::text('Option2',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}

                                {!! Form::label('Prix','Prix :') !!}

                                <input type="text" name="Prix2" class="form-control" step="any" id="inputCurrency" data-plugin="formatter"
                                       data-pattern="DT[[999]],[[999]].[[99]]" />
                                <p class="help-block">$999,999.99</p>
                                @if ($errors->has('Prix2'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('Prix2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading" id="exampleHeading3" role="tab">
                            <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse3"
                               data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse3">
                                Ajouter un quatrième prix                                    </a>
                        </div>
                        <div class="panel-collapse collapse" id="exampleCollapse3" aria-labelledby="exampleHeading3"
                             role="tabpanel">
                            <div class="panel-body">
                                {!! Form::label('nom','Nom de l\'option n°3 :') !!}
                                {!! Form::text('Option3',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}

                                {!! Form::label('Prix','Prix :') !!}

                                <input type="text" name="Prix3" class="form-control" step="any" id="inputCurrency" data-plugin="formatter"
                                       data-pattern="DT[[999]],[[999]].[[99]]" />
                                <p class="help-block">$999,999.99</p>
                                @if ($errors->has('Prix3'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('Prix3') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <!-- End Example Wizard Accordion -->





        </div>
        </div><div class="form-group">
               {!! Form::button('Enregistrer',['type'=>'submit','class'=>'btn btn-success','onclick'=>'verification()']) !!}
           </div>
           {!! Form::close() !!}

            </div>


</div>
    <Script language='javascript'>
        function CalculerMontantTTC()
        {
                Form1.PrixTVA.value = Number(Form1.Prix0.value) + Number((Form1.Prix0.value / 100) * 20) ;
        }
    </Script>



@stop