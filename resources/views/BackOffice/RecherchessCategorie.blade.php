@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Sous Categories </h1>
            <div class="page-header-actions">
                <form action="/BackRecherchessCategorie" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="q" placeholder="Recherche...">
                    </div>
                </form>
            </div>
        </div>
        @if(isset($details))
            <div class="page-content">
                <div class="projects-wrap">
                    <ul class="blocks blocks-100 blocks-xlg-5 blocks-md-4 blocks-sm-3 blocks-xs-2">

                        <p> Les résultats de la recherche pour  <b> {{ $query }} </b> sont :</p>

                        @foreach($details as $Categories)

                            <li>
                                <div class="panel">
                                    <figure class="overlay overlay-hover animation-hover">
                                        <img class="caption-figure" src="{{asset($Categories->file)}}" height="140" width="170">
                                        <figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">
                                            <div class="btn-group">

                                                {{link_to_route('BackCategorie.edit','Edit',[$Categories->id],['class'=>'btn btn-icon btn-pure btn-default'])}}

                                                {!! Form::open(array('route'=>['BackCategorie.destroy',$Categories->id],'method'=>'DELETE')) !!}
                                                {!! Form::button('Delete',['type'=>'submit','class'=>'btn btn-icon btn-pure btn-default']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                            {{link_to_route('BackCategorie.show','Afficher',[$Categories->id],['class'=>'btn btn-outline btn-default project-button'])}}

                                        </figcaption>
                                    </figure>
                                    <div class="time pull-right">{{$Categories->created_at}}</div>
                                    <div>{{str_limit($Categories->title, $limit = 10, $end = '..')}}</div>
                                </div>
                            </li>

                        @endforeach

                    </ul>
                </div>


                <nav>
                    <ul class="pagination pagination-no-border">
                        {{$details->render()}}

                    </ul>
                </nav>

            </div>
        @elseif(isset($message))
            <p>{{ $message }}</p>
        @endif

    </div>


    <!--add project form -->
    {{link_to_route('ssBackCategorie.create','Add',null,['class'=>'site-action btn-raised btn btn-success btn-floating'])}}


    </div>
    </div>
    </div>

@stop