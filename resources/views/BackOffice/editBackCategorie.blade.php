@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Editer Categorie </h1>
            <div class="page-header-actions">
                <form>
                    <div class="input-search input-search-dark">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="" placeholder="Search...">
                    </div>
                </form>
            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">


                 <div class="modal-body">

                    {!! Form::model($Categories,array('route'=>['BackCategorie.update',$Categories->id],'method'=>'PUT')) !!}

                     <img src="{{ asset($Categories->file) }}" height="150" />
                     <div class="form-group">
                         <label for="userfile">Image File</label>
                         <input type="hidden" name="userfile" value="{{$Categories->file}}">

                         {!! Form::file('userfile',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         <label for="title">Title</label>
                         {!! Form::text('title',null,['class'=>'form-control']) !!}
                     </div>

                     <div class="form-group">
                         <label for="description">Description</label>
                         {!! Form::textarea('description',null,['class'=>'form-control']) !!}
                     </div>

                     <button type="submit" class="btn btn-primary">Save</button>
                     <a href="{{ url('/BackCategorie') }}" class="btn btn-warning">Cancel</a>


                    {!! Form::close() !!}
                 </div>

                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>












@stop