@extends('layouts.Backmain')
<!-- Main -->
@section('content')

    <!-- Forum Content -->
    <div class="page-main">

      <!-- Forum Content Header -->
      <div class="page-header">
        <h1 class="page-title">Getting Started</h1>
        <form class="margin-top-20" action="#" role="search">
          <div class="input-search input-search-dark">
            <input type="text" class="form-control width-full" placeholder="Search..." name="">
            <button type="submit" class="input-search-btn">
              <i class="icon wb-search" aria-hidden="true"></i>
            </button>
          </div>
        </form>
      </div>

      <!-- Forum Nav -->
      <div class="page-nav-tabs">
        <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
          <li class="active" role="presentation">
            <a data-toggle="tab" href="#forum-newest" aria-controls="forum-newest" aria-expanded="true"
            role="tab">Newest</a>
          </li>
          <li role="presentation">
            <a data-toggle="tab" href="#forum-activity" aria-controls="forum-activity" aria-expanded="false"
            role="tab">Activity</a>
          </li>
          <li role="presentation">
            <a data-toggle="tab" href="#forum-answer" aria-controls="forum-answer" aria-expanded="false"
            role="tab">Answer</a>
          </li>
        </ul>
      </div>

      <!-- Forum Content -->
      <div class="page-content tab-content page-content-table">
        <div class="tab-pane active" id="forum-newest" role="tabpanel">
          <table class="table is-indent">
            <tbody>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/1.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Vicinum at aperta, torquem mox doloris illi, officiis.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Herman Beck</span>
                      <span class="started">1 day ago</span>
                      <span class="tags">Themes</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">1</span>
                  <span class="unit">Post</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/2.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Moribus ibidem angore, iudiciorumque careret causa verbis aliena.
                      <div class="flags responsive-hide">
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Mary Adams</span>
                      <span class="started">2 days ago</span>
                      <span class="tags">Configuration</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">2</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/3.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Sinat ut miseram voluptatibus compositis quodsi. Quem afflueret.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Caleb Richards</span>
                      <span class="started">3 days ago</span>
                      <span class="tags">Installation</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">3</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/4.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Graeca modice video patre iuste tradidisse molestiae molestia.
                      <div class="flags responsive-hide">
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By June Lane</span>
                      <span class="started">4 days ago</span>
                      <span class="tags">Announcements</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">4</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/5.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Autem omnes is protervi fortitudinis maerores, geometrica statuat.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Edward Fletcher</span>
                      <span class="started">5 days ago</span>
                      <span class="tags">Development</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">5</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/6.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Tuemur geometrica angore haeret rogatiuncula albuci meo etiam.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Crystal Bates</span>
                      <span class="started">6 days ago</span>
                      <span class="tags">Plugins</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">6</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/7.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Caret adoptionem tollitur, agam dixeris respondendum fortunae familias.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Nathan Watts</span>
                      <span class="started">7 days ago</span>
                      <span class="tags">Technical Support</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">7</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/8.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Una veniamus fruentem firmam, explicari laboramus futuris miser.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Heather Harper</span>
                      <span class="started">8 days ago</span>
                      <span class="tags">Code Review</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">8</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/9.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Aristippus dicantur verterem molestiam tali appetendum. Maximis potest.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Willard Wood</span>
                      <span class="started">9 days ago</span>
                      <span class="tags">Responses</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">9</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/10.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Hac ipsa sit, facile liberiusque ipse frustra multo.
                      <div class="flags responsive-hide">
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Ronnie Ellis</span>
                      <span class="started">10 days ago</span>
                      <span class="tags">Package</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">10</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
            </tbody>
          </table>
          <ul class="pagination pagination-gap">
            <li class="disabled"><a href="javascript:void(0)">Previous</a></li>
            <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
            <li><a href="javascript:void(0)">2</a></li>
            <li><a href="javascript:void(0)">3</a></li>
            <li><a href="javascript:void(0)">4</a></li>
            <li><a href="javascript:void(0)">5</a></li>
            <li><a href="javascript:void(0)">Next</a></li>
          </ul>
        </div>
        <div class="tab-pane" id="forum-activity" role="tabpanel">
          <table class="table is-indent">
            <tbody>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/11.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Repellere summo tritani uterque nullo sollicitudines. Frui lectorem.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Gwendolyn Wheeler</span>
                      <span class="started">1 day ago</span>
                      <span class="tags">Technical Support</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">1</span>
                  <span class="unit">Post</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/12.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Malarum beate spe consilia fabulae, intervalla verbum falso.
                      <div class="flags responsive-hide">
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Daniel Russell</span>
                      <span class="started">2 days ago</span>
                      <span class="tags">Plugins</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">2</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/13.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Nomini libris ergo errorem solido sitne oratio, mediocriterne.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Sarah Graves</span>
                      <span class="started">3 days ago</span>
                      <span class="tags">Announcements</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">3</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/14.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Terrore ennius, sumitur tum provincia quae probatum fingi.
                      <div class="flags responsive-hide">
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Andrew Hoffman</span>
                      <span class="started">4 days ago</span>
                      <span class="tags">Installation</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">4</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/15.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Statua iucundius brevis beatam finitas suscipit ipsis incursione.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Camila Lynch</span>
                      <span class="started">5 days ago</span>
                      <span class="tags">Configuration</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">5</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/16.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Laus optime turbulenta carere cotidie deduceret aequo metuamus.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Ramon Dunn</span>
                      <span class="started">6 days ago</span>
                      <span class="tags">Feature Requests</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">6</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/17.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Efficit accusantium voluit quales, legere inmensae. Pariuntur privamur.
                      <div class="flags responsive-hide">
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Scott Sanders</span>
                      <span class="started">7 days ago</span>
                      <span class="tags">Troubleshooting</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">7</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
            </tbody>
          </table>
          <ul class="pagination pagination-gap">
            <li class="disabled"><a href="javascript:void(0)">Previous</a></li>
            <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
            <li><a href="javascript:void(0)">2</a></li>
            <li><a href="javascript:void(0)">3</a></li>
            <li><a href="javascript:void(0)">4</a></li>
            <li><a href="javascript:void(0)">5</a></li>
            <li><a href="javascript:void(0)">Next</a></li>
          </ul>
        </div>
        <div class="tab-pane" id="forum-answer" role="tabpanel">
          <table class="table is-indent">
            <tbody>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/2.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Augeri, sanos simulent atomi habet ullo consuetudine saepti.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Mary Adams</span>
                      <span class="started">1 day ago</span>
                      <span class="tags">Plugins</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">1</span>
                  <span class="unit">Post</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/3.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Odioque denique teneam animis putem torquentur retinere sermone.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Caleb Richards</span>
                      <span class="started">2 days ago</span>
                      <span class="tags">Technical Support</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">2</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/4.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Diligenter accessio meque difficile propemodum posuit momenti impetu.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By June Lane</span>
                      <span class="started">3 days ago</span>
                      <span class="tags">Code Review</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">3</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/5.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Terrore ennius, sumitur tum provincia quae probatum fingi.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Edward Fletcher</span>
                      <span class="started">4 days ago</span>
                      <span class="tags">Troubleshooting</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">4</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/6.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Habere nati sponte dum pericula exorsus sciscat fructuosam.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Crystal Bates</span>
                      <span class="started">5 days ago</span>
                      <span class="tags">Configuration</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">5</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/7.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Nutu fugiendus, accusata utamur iniucundus captet quippe virtutum.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Nathan Watts</span>
                      <span class="started">6 days ago</span>
                      <span class="tags">Announcements</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">6</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
              <tr data-url="panel.tpl" data-toggle="slidePanel">
                <td class="pre-cell"></td>
                <td class="cell-60 responsive-hide">
                  <a class="avatar" href="javascript:void(0)">
                    <img class="img-responsive" src="../../../assets/portraits/8.jpg"
                    alt="...">
                  </a>
                </td>
                <td>
                  <div class="content">
                    <div class="title">
                      Parvos labore efficeret, liber timorem tarentinis accedis praebeat.
                      <div class="flags responsive-hide">
                        <span class="sticky-top label label-round label-danger"><i class="icon wb-dropup" aria-hidden="true"></i>TOP</span>
                        <i class="locked icon wb-lock" aria-hidden="true"></i>
                      </div>
                    </div>
                    <div class="metas">
                      <span class="author">By Heather Harper</span>
                      <span class="started">7 days ago</span>
                      <span class="tags">Themes</span>
                    </div>
                  </div>
                </td>
                <td class="cell-80 forum-posts">
                  <span class="num">7</span>
                  <span class="unit">Posts</span>
                </td>
                <td class="suf-cell"></td>
              </tr>
            </tbody>
          </table>
          <ul class="pagination pagination-gap">
            <li class="disabled"><a href="javascript:void(0)">Previous</a></li>
            <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
            <li><a href="javascript:void(0)">2</a></li>
            <li><a href="javascript:void(0)">3</a></li>
            <li><a href="javascript:void(0)">4</a></li>
            <li><a href="javascript:void(0)">5</a></li>
            <li><a href="javascript:void(0)">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- Add Topic Form -->
  <button class="site-action btn-raised btn btn-success btn-floating" data-target="#addTopicForm"
  data-toggle="modal" type="button">
    <i class="icon wb-pencil" aria-hidden="true"></i>
  </button>

  <div class="modal fade" id="addTopicForm" aria-hidden="true" aria-labelledby="addTopicForm"
  role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Create New Topic</h4>
        </div>
        <div class="modal-body container-fluid">
          <form>
            <div class="form-group">
              <label class="control-label margin-bottom-15" for="topicTitle">Topic Title:</label>
              <input type="text" class="form-control" id="topicTitle" name="title" placeholder="How To..."
              />
            </div>
            <div class="form-group">
              <textarea name="content" data-provide="markdown" data-iconlibrary="fa" rows="10"></textarea>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-6">
                  <label class="control-label margin-bottom-15" for="topicCategory">Topic Category:</label>
                  <select id="topicCategory" data-plugin="selectpicker">
                    <option>PHP</option>
                    <option>Javascript</option>
                    <option>HTML</option>
                    <option>CSS</option>
                    <option>Ruby</option>
                  </select>
                </div>
                <div class="col-xs-6">
                  <label class="control-label margin-bottom-15" for="topic_tags">Topic Tags:</label>
                  <select id="topic_tags" data-plugin="selectpicker">
                    <option>PHP</option>
                    <option>Javascript</option>
                    <option>HTML</option>
                    <option>CSS</option>
                    <option>Ruby</option>
                  </select>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer text-left">
          <button class="btn btn-primary" data-dismiss="modal" type="submit">Create</button>
          <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Footer -->
  <footer class="site-footer">
    <span class="site-footer-legal">© 2015 Remark</span>
    <div class="site-footer-right">
      Crafted with <i class="red-600 wb wb-heart"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
  </footer>

  <!-- Core  -->
  <script src="../../../assets/vendor/jquery/jquery.js"></script>
  <script src="../../../assets/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../../assets/vendor/animsition/jquery.animsition.js"></script>
  <script src="../../../assets/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="../../../assets/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../../assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="../../../assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

  <!-- Plugins -->
  <script src="../../../assets/vendor/switchery/switchery.min.js"></script>
  <script src="../../../assets/vendor/intro-js/intro.js"></script>
  <script src="../../../assets/vendor/screenfull/screenfull.js"></script>
  <script src="../../../assets/vendor/slidepanel/jquery-slidePanel.js"></script>

  <script src="../../../assets/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../../assets/vendor/bootstrap-markdown/bootstrap-markdown.js"></script>
  <script src="../../../assets/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../../assets/vendor/marked/marked.js"></script>
  <script src="../../../assets/vendor/to-markdown/to-markdown.js"></script>

  <!-- Scripts -->
  <script src="../../../assets/js/core.js"></script>
  <script src="../../../assets/js/site.js"></script>

  <script src="../../../assets/js/sections/menu.js"></script>
  <script src="../../../assets/js/sections/menubar.js"></script>
  <script src="../../../assets/js/sections/sidebar.js"></script>

  <script src="../../../assets/js/configs/config-colors.js"></script>
  <script src="../../../assets/js/configs/config-tour.js"></script>

  <script src="../../../assets/js/components/asscrollable.js"></script>
  <script src="../../../assets/js/components/animsition.js"></script>
  <script src="../../../assets/js/components/slidepanel.js"></script>
  <script src="../../../assets/js/components/switchery.js"></script>
  <script src="../../../assets/js/apps/app.js"></script>
  <script src="../../../assets/js/apps/forum.js"></script>
  <script src="../../../assets/js/components/bootstrap-select.js"></script>

  <script>
    (function(document, window, $) {
      'use strict';
      var AppForum = window.AppForum;

      $(document).ready(function() {
        AppForum.run();
      });
    })(document, window, jQuery);
  </script>

</body>

</html>