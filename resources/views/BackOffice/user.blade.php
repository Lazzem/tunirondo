@extends('layouts.Backmain')
<!-- Main -->
@section('content')

  <!-- Page -->
  <div class="page animsition">
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body">
          <form class="page-search-form" action="/BackRechercheUtilisateur" method="POST" role="search">
            {{ csrf_field() }}
            <div class="input-search input-search-dark">
              <i class="input-search-icon wb-search" aria-hidden="true"></i>
              <input type="text" class="form-control" id="inputSearch" name="q" placeholder="Recherche...">
              <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
            </div>
          </form>

               <br>
            <div class="tab-content">
              <div class="tab-pane active" id="all_contacts" role="tabpanel">
                <ul class="list-group">

                  @if (Session::has('message'))
                    <div class="'alert alert-success">{{Session::get('message')}}</div>
                  @endif

                  @foreach($users as $User)

                  <li class="list-group-item">
                    <div class="media">
                      <div class="media-left">
                        <div class="avatar avatar-online">
                          <img src="{{asset('BackOffice//assets/portraits/1.jpg')}}" alt="...">
                          <i class="avatar avatar-busy"></i>
                        </div>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading">
                          {{$User->name }} {{$User->lastname }}
                          <small>{{$User->created_at}}</small>
                        </h4>
                        <p>
                          <i class="icon icon-color wb-map" aria-hidden="true">
                            {{$User->email}}
                          </i>
                        </p>

                      </div>
                      <div class="media-right">
                        <table>
                          <td>
                              {{link_to_route('user.show','Afficher',[$User->id],['class'=>'btn btn-outline btn-success btn-sm'])}}
                          </td>
                          <td>
                              {{link_to_route('user.edit','Editer',[$User->id],['class'=>'btn btn-outline btn-success btn-sm'])}}
                          </td>

                          <td>
                              {!! Form::open(array('route'=>['user.destroy',$User->id],'method'=>'DELETE')) !!}
                              {!! Form::button('Supprimer',['type'=>'submit','class'=>'btn btn-outline btn-success btn-sm']) !!}
                              {!! Form::close() !!}
                          </td>

                        </table>
                      </div>
                    </div>
                  </li>
                    @endforeach



                </ul>
                <nav>
                  <ul class="pagination pagination-no-border">
                    <li>
                      {{$users->render()}}
                    </li>
                  </ul>
                </nav>
              </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Panel -->
  {{link_to_route('user.create','Ajout',null,['class'=>'site-action btn-raised btn btn-success btn-floating'])}}

    </div>
  </div>
  <!-- End Page -->


  <script>
      (function(document, window, $) {
          'use strict';

          var Site = window.Site;
          $(document).ready(function() {
              Site.run();
          });
      })(document, window, jQuery);
  </script>
  @stop