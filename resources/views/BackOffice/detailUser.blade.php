@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Afficher</h1>
            <div class="page-header-actions">

            </div>
        </div>
        <div class="page-content">
            <div class="projects-wrap">


                 <div class="modal-body">

                    {!! Form::model($User,array('route'=>array('user.show',$User->id),'method'=>'GET')) !!}

                     <div class="form-group">
                         {!! Form::label('Nom','Nom ') !!}
                         {!! Form::text('name',null,['class'=>'form-control','disabled'=>'true']) !!}
                     </div>

                     <div class="form-group">
                         {!! Form::label('Prénom','Prenom ') !!}
                         {!! Form::text('lastname',null,['class'=>'form-control','disabled'=>'true']) !!}
                     </div>

                     <div class="form-group">
                         <select class="form-control" name="role" value="role" disabled="true">
                             @if($User->role==0)
                                 <option value="0" selected="true" >Client</option>
                                 <option value="1" >Organisateur</option>
                                 <option value="2" >Administrateur</option>
                             @endif
                             @if($User->role==1)
                                 <option value="0" >Client</option>
                                 <option value="1" selected="true" >Organisateur</option>
                                 <option value="2"  >Administrateur</option>
                             @endif
                             @if($User->role==2)
                                 <option value="0" >Client</option>
                                 <option value="1" >Organisateur</option>
                                 <option value="2" selected="true" >Administrateur</option>
                             @endif
                         </select>
                     </div>

                     <div class="form-group">
                         {!! Form::label('e-mail','E-mail ') !!}
                         {!! Form::text('email',null,array('class'=>'form-control','disabled'=>'true')) !!}
                     </div>



                    {!! Form::close() !!}

                     <div class="form-group">
                         <a href="/user"><button class="btn btn-primary" type="button"><span>Retour</span></button></a>
                     </div>
                 </div>

            </div>
        </div>
    </div>












@stop