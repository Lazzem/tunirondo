@extends('layouts.Backmain')
<!-- Main -->
@section('content')


  <!-- Page -->
  <div class="page animsition">
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body">
          <form class="page-search-form" role="search">
            <div class="input-search input-search-dark">
              <i class="input-search-icon wb-search" aria-hidden="true"></i>
              <input type="text" class="form-control" id="inputSearch" name="search" placeholder="Search Pages">
              <button type="button" class="input-search-close icon wb-close" aria-label="Close"></button>
            </div>
          </form>
          <h1 class="page-search-title">Search Results For "Web Desing"</h1>
          <p class="page-search-count">About
            <span>1,370</span> result (
            <span>0.13</span> seconds)</p>
          <ul class="list-group list-group-full list-group-dividered">
            <li class="list-group-item">
              <h4><a href="https://github.com/amazingSurge?tab=repositories">Eademque Virtutum Laudantium</a></h4>
              <a class="search-result-link" href="https://github.com/amazingSurge?tab=repositories">https://github.com/amazingSurge?tab=repositories</a>
              <p>Praebeat pecunias viveremus probamus opus apeirian haec perveniri,
                memoriter.Praebeat pecunias viveremus probamus opus apeirian haec
                perveniri, memoriter.Praebeat pecunias viveremus probamus opus
                apeirian haec perveniri, memoriter.</p>
            </li>
            <li class="list-group-item">
              <h4><a href="https://github.com/amazingSurge?tab=repositories">Parum Interiret Consequatur</a></h4>
              <a class="search-result-link" href="https://github.com/amazingSurge?tab=repositories">https://github.com/amazingSurge?tab=repositories</a>
              <p>Regula magnosque ait. Rebus intellegimus occulte instituendarum quoniam
                fabulae.Regula magnosque ait. Rebus intellegimus occulte instituendarum
                quoniam fabulae.</p>
            </li>
            <li class="list-group-item">
              <h4><a href="https://github.com/amazingSurge?tab=repositories">Afficitur Nos Veritus</a></h4>
              <a class="search-result-link" href="https://github.com/amazingSurge?tab=repositories">https://github.com/amazingSurge?tab=repositories</a>
              <p>Elaboraret animum primo. Civibus assueverit consequatur affert viros
                scribi.Elaboraret animum primo. Civibus assueverit consequatur
                affert viros scribi.</p>
            </li>
            <li class="list-group-item">
              <h4><a href="https://github.com/amazingSurge?tab=repositories">Tamquam Secumque Nacti</a></h4>
              <a class="search-result-link" href="https://github.com/amazingSurge?tab=repositories">https://github.com/amazingSurge?tab=repositories</a>
              <p>Pronuntiaret liberos probes horribiles acri ita seiunctum aristippus.
                Humili.Pronuntiaret liberos probes horribiles acri ita seiunctum
                aristippus. Humili.Pronuntiaret liberos probes horribiles acri
                ita seiunctum aristippus. Humili.</p>
            </li>
            <li class="list-group-item">
              <h4><a href="https://github.com/amazingSurge?tab=repositories">Aliquo Difficile In</a></h4>
              <a class="search-result-link" href="https://github.com/amazingSurge?tab=repositories">https://github.com/amazingSurge?tab=repositories</a>
              <p>Omnes arbitrer ancillae fictae maximam renovata, fieri pecuniae mundus.Omnes
                arbitrer ancillae fictae maximam renovata, fieri pecuniae mundus.</p>
            </li>
            <li class="list-group-item">
              <h4><a href="https://github.com/amazingSurge?tab=repositories">Quale Momenti Eitam</a></h4>
              <a class="search-result-link" href="https://github.com/amazingSurge?tab=repositories">https://github.com/amazingSurge?tab=repositories</a>
              <p>Perpetiuntur inportuno iudicio errem. Reperire aliud delectus referta
                fruuntur.Perpetiuntur inportuno iudicio errem. Reperire aliud delectus
                referta fruuntur.</p>
            </li>
          </ul>
          <nav>
            <ul class="pagination pagination-no-border">
              <li class="disabled">
                <a href="javascript:void(0)" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li class="active"><a href="javascript:void(0)">1 <span class="sr-only">(current)</span></a></li>
              <li><a href="javascript:void(0)">2</a></li>
              <li><a href="javascript:void(0)">3</a></li>
              <li><a href="javascript:void(0)">4</a></li>
              <li><a href="javascript:void(0)">5</a></li>
              <li>
                <a href="javascript:void(0)" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- End Panel -->
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <span class="site-footer-legal">© 2015 Remark</span>
    <div class="site-footer-right">
      Crafted with <i class="red-600 wb wb-heart"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
  </footer>

  <!-- Core  -->
  <script src="../../assets/vendor/jquery/jquery.js"></script>
  <script src="../../assets/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../assets/vendor/animsition/jquery.animsition.js"></script>
  <script src="../../assets/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="../../assets/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="../../assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

  <!-- Plugins -->
  <script src="../../assets/vendor/switchery/switchery.min.js"></script>
  <script src="../../assets/vendor/intro-js/intro.js"></script>
  <script src="../../assets/vendor/screenfull/screenfull.js"></script>
  <script src="../../assets/vendor/slidepanel/jquery-slidePanel.js"></script>


  <!-- Scripts -->
  <script src="../../assets/js/core.js"></script>
  <script src="../../assets/js/site.js"></script>

  <script src="../../assets/js/sections/menu.js"></script>
  <script src="../../assets/js/sections/menubar.js"></script>
  <script src="../../assets/js/sections/sidebar.js"></script>

  <script src="../../assets/js/configs/config-colors.js"></script>
  <script src="../../assets/js/configs/config-tour.js"></script>

  <script src="../../assets/js/components/asscrollable.js"></script>
  <script src="../../assets/js/components/animsition.js"></script>
  <script src="../../assets/js/components/slidepanel.js"></script>
  <script src="../../assets/js/components/switchery.js"></script>


  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>

@stop