<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Login | TuniRando Admin </title>

  <link rel="apple-touch-icon" href="{{URL::asset('../../backOffice/assets/images/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{URL::asset('../../backOffice/assets/images/favicon.ico')}}">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/css/bootstrap-extend.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/css/site.min.css')}}">

  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/vendor/flag-icon-css/flag-icon.css')}}">


  <!-- Page -->
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/css/pages/login.css')}}">

  <!-- Fonts -->
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/fonts/web-icons/web-icons.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('../../backOffice/assets/fonts/brand-icons/brand-icons.min.cs')}}s">
  <link rel='stylesheet' href='{{URL::asset('http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic')}}'>


  <!--[if lt IE 9]>
    <script src="{{URL::asset('../../backOffice/assets/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="{{URL::asset('../../backOffice/assets/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{URL::asset('../../backOffice/assets/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="{{URL::asset('../../backOffice/assets/vendor/modernizr/modernizr.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/breakpoints/breakpoints.js')}}"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="page-login layout-full">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
      <div class="brand">
        <img class="brand-img" src="{{asset('images/logo.png')}}" alt="...">
        <h2 class="brand-text">Administrateur</h2>
      </div>
      <p>Connectez-vous à votre compte de pages</p>
      <form role="form" method="POST" action="{{ url('Blogin') }}">
        {{ csrf_field() }}
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

            <label class="sr-only" for="email">E-mail</label>
          <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{ old('email') }}">
          @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <label class="sr-only" for="password">Mot de passe</label>
          <input type="password" class="form-control" id="password" name="password"
          placeholder="Mot de passe">
          @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group clearfix">
          <div class="checkbox-custom checkbox-inline pull-left">

          </div>
          <a class="pull-right" href="/Bforgot-password">Mot de passe oublié ?</a>
        </div>
        <button type="submit" class="btn btn-primary btn-block" id="send2" name="send">Se Connecter</button>
      </form>

      <footer class="page-copyright">

        <p>© 2017. TuniRando.</p>

      </footer>
    </div>
  </div>
  <!-- End Page -->


  <!-- Core  -->
  <script src="{{URL::asset('../../backOffice/assets/vendor/jquery/jquery.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/bootstrap/bootstrap.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/animsition/jquery.animsition.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/asscroll/jquery-asScroll.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/asscrollable/jquery.asScrollable.all.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

  <!-- Plugins -->
  <script src="{{URL::asset('../../backOffice/assets/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/intro-js/intro.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/screenfull/screenfull.j')}}s"></script>
  <script src="{{URL::asset('../../backOffice/assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>

  <script src="{{URL::asset('}../../backOffice/assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

  <!-- Scripts -->
  <script src="{{URL::asset('../../backOffice/assets/js/core.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/site.js')}}"></script>

  <script src="{{URL::asset('../../backOffice/assets/js/sections/menu.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/sections/menubar.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/sections/sidebar.js')}}"></script>

  <script src="{{URL::asset('../../backOffice/assets/js/configs/config-colors.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/configs/config-tour.js')}}"></script>

  <script src="{{URL::asset('../../backOffice/assets/js/components/asscrollable.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/components/animsition.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/components/slidepanel.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/components/switchery.js')}}"></script>
  <script src="{{URL::asset('../../backOffice/assets/js/components/jquery-placeholder.js')}}"></script>

  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>

</body>

</html>