@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">

        <div class="page-header">
            <h1 class="page-title">Produit </h1> </div>
            <!-- Steps -->
            <div class="steps steps-sm row" data-plugin="matchHeight" data-by-row="true" role="tablist">
                <div class="step col-md-4 current" data-target="#exampleAccount" role="tab">
                    <span class="step-number">1</span>
                    <div class="step-desc">
                        <span class="step-title">Produit</span>
                        <p>Ajoute produit</p>
                    </div>
                </div>

                <div class="step col-md-4" data-target="#exampleBilling" role="tab">
                    <span class="step-number">2</span>
                    <div class="step-desc">
                        <span class="step-title">Les Dates</span>
                        <p>Ajoute déclinaison </p>
                    </div>
                </div>

                <div class="step col-md-4" data-target="#exampleGetting" role="tab">
                    <span class="step-number">3</span>
                    <div class="step-desc">
                        <span class="step-title">Confirmation</span>

                    </div>
                </div>
            </div>
            <!-- End Steps -->

        <div class="wizard-pane active" id="exampleAccount" role="tabpanel">
        <div class="page-content">
            <div class="projects-wrap">


                <div class="modal-body">

                    {!! Form::open(array('route'=>'BProduits.store','method'=>'POST', 'files'=>'true')) !!}
                    {{ csrf_field() }}
                    <div class="form-group">
                        {!! Form::label('Organisateur','Organisateur') !!}
                        <div class="form-group">
                            <div class="input-group input-group-icon">
                    <span class="input-group-addon">
                      <span class="icon wb-envelope" aria-hidden="true"></span>
                    </span>
                                <input type="email" name="Organisateur" id="Organisateur" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div>
                        @if ($errors->has('Organisateur'))
                        <span class="alert-danger">
                                        <strong>{{ $errors->first('Organisateur') }}</strong>
                                    </span>
                        @endif</div>
                    </div>

                     <div class="form-group">
                        <div class="form-group ">
                            <label for="select">Categorie</label>

                            <select class="form-control" name="Categorie" value=Categorie>
                                <option value="">Aucunne </option>
                                @foreach($ssCategorie as $ssCategorie)

                                    <option value="{{$ssCategorie->id}}" >{{$ssCategorie->title}}</option>

                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Status :</label>

                            <div class="radio-custom radio-default radio-inline">
                                <input type="radio" id="Status" name="Status" value="0" checked />
                                <label for="inputBasicMale">Activer</label>
                            </div>
                            <div class="radio-custom radio-default radio-inline">
                                <input type="radio" id="Status" name="Status" value="1"/>
                                <label for="inputBasicFemale">Désactiver</label>
                            </div>

                    </div>
                    <div class="form-group">
                        {!! Form::label('Titre','Titre') !!}
                        {!! Form::text('Title',null,['class'=>'form-control']) !!}
                        <div>
                        @if ($errors->has('Title'))
                            <span class="alert-danger">
                                        <strong>{{ $errors->first('Title') }}</strong>
                                    </span>
                        @endif</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Description Principale','Description Principale') !!}
                        {!! Form::text('DescriptionMineur',null,['class'=>'form-control']) !!}
                        @if ($errors->has('DescriptionMineur'))
                            <span class="alert-danger">
                                        <strong>{{ $errors->first('DescriptionMineur') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('Description','Desciption') !!}
                        {!! Form::textarea('Description',null,['class'=>'maxlength-textarea form-control mb-sm']) !!}
                        <div>@if ($errors->has('Description'))
                            <span class="alert-danger">
                                        <strong>{{ $errors->first('Description') }}</strong>
                                    </span>
                        @endif</div>
                    </div>

                    <!-- Example Wizard Accordion -->
                    <div class="margin-bottom-30">
                        <div class="panel-group" id="exampleWizardAccordion" aria-multiselectable="true"
                             role="tablist">
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeading1" role="tab">
                                    <a class="panel-title" data-toggle="collapse" href="#exampleCollapse1" data-parent="#exampleWizardAccordion"
                                       aria-expanded="true" aria-controls="exampleCollapse1">
                                        Ajouter  la photos principale
                                    </a>
                                </div>
                                <div class="panel-collapse collapse in" id="exampleCollapse1" aria-labelledby="exampleHeading1"
                                     role="tabpanel">
                                    <div class="panel-body">
                                        {!! Form::file('PhotosPrincipale',null,['class'=>'form-control']) !!}                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeading2" role="tab">
                                    <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse2"
                                       data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse2">
                                        Ajouter une deuxième photos
                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapse2" aria-labelledby="exampleHeading2"
                                     role="tabpanel">
                                    <div class="panel-body">
                                        {!! Form::file('Photos1',null,['class'=>'form-control']) !!}

                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading" id="exampleHeading3" role="tab">
                                    <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapse3"
                                       data-parent="#exampleWizardAccordion" aria-expanded="false" aria-controls="exampleCollapse3">
                                                  Ajouter une troisieme photos                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapse3" aria-labelledby="exampleHeading3"
                                     role="tabpanel">
                                    <div class="panel-body">
                                        {!! Form::file('Photos2',null,['class'=>'form-control']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Example Wizard Accordion -->

                    @if ($errors->has('PhotosPrincipale'))
                        <span class="alert-danger">
                                        <strong>Il faut ajouter au moins une photos principale</strong>
                                    </span>
                    @endif
                    <div class="form-group">
                    {!! Form::button('suivant',['type'=>'submit','class'=>'btn btn-primary']) !!}<a href="{{ url('/BProduits') }}" class="btn btn-warning">Annuler</a>
                </div>




                    {!! Form::close() !!}
        </div>
    </div>
        </div>

        </div>

    </div>


@stop