@extends('layouts.Backmain')
@section('content')
    <div class="page animsition">
        <div class="page-header">
            <h1 class="page-title">Ajouter un menu </h1>

        </div>
        <div class="page-content">
            <div class="projects-wrap">


                <div class="modal-body">

                    {!! Form::open(['url'=>'/Bmenu', 'method'=>'POST', 'files'=>'true']) !!}


                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" value="">
                    </div>




                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                        <a href="{{ url('/BackCategorie') }}" class="btn btn-warning">Annuler</a>
                    </div>


                    {!! Form::close() !!}
                </div>

                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </div>












@stop