@extends('layouts.main')
<!-- Main -->
@section('content')


    <section class="main-container col1-layout">
        <div class="main container">
            <div class="account-login">
                <div class="page-title">
                    <h2>S'inscrir ou Se connecter</h2>
                </div>
                <fieldset class="col2-set">
                    <legend>S'inscrir ou Se connecter</legend>
                    <div class="col-1 new-users"><strong>S'inscrir</strong>
                        <div class="content">
                            <p>En créant un compte avec notre magasin, vous serez en mesure de valider votre paiement plus rapidement, suivre vos commandes dans votre compte .</p>
                            <div class="buttons-set">
                                <a href="/register"><button class="button create-account" type="button"><span>Créer un compte</span></button></a>
                            </div>
                        </div>
                    </div>
                    <fieldset class="col-2 registered-users"style="height: 362px;"><strong>Vous êtes déjà inscrit ?</strong>
                        <form class="form-center" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="content">

                                <ul class="form-list">
                                    <li>
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email">Adresse E-mail <span class="required">*</span></label>

                                            <br>

                                            <input id="email" title="Email Address" type="email" class="input-text required-entry" name="email" value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="pass">Mot de passe <span class="required">*</span></label>
                                            <br>
                                            <input id="password" type="password" class="input-text required-entry validate-password" name="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block" >
                                                    <strong style="color: red">{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </li>
                                </ul>
                                <p class="required">* Champs obligatoires</p>
                                <div class="buttons-set">
                                    <button id="send2" name="send" type="submit" class="button login"><span>Se connecter</span></button>
                                    <a class="btn btn-link" href="{{ url('/password/reset') }}">Mot de passe oublié ?</a>
                                </div>
                            </div>
                        </form>
                    </fieldset>
                </fieldset>
            </div>
        </div>
    </section>

@stop
<!-- End Main -->