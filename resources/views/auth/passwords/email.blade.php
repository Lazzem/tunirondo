@extends('layouts.main')
@section('content')


    <div class="main-container  col2-right-layout col-lg-offset-1">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-5 wow bounceInUp animated">
                    <div class="page-title">
                        <h2> Réinitialiser votre mot de passe</h2>
                    </div>
                    <div class="static-contain">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ url('/password/email') }}">
                            {{ csrf_field() }}
                        <fieldset class="group-select">

                            <fieldset>
                                <legend>Email</legend>
                                <input type="hidden" name="billing[address_id]" value="" id="billing:address_id">
                                <ul>




                                    <li>
                                        <label for="billing:street1">Saisir votre Email </label>
                                        <br>
                                        <input type="text" title="Street Address" name="email" id="email" value="" class="input-text required-entry">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </li>



                                </ul>
                            </fieldset>

                            <li>

                                <input type="text" name="hideit" id="hideit" value="">
                                <div class="buttons-set">
                                    <button type="submit" title="Submit" class="button create-account"> <span>Continuer </span> </button>
                                </div>
                            </li>
                            </ul>
                        </fieldset>
                        </form>
                    </div>
                </section>

            </div>
        </div>
    </div>>




@stop