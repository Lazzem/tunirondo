@extends('layouts.main')
<!-- Main -->
@section('content')


    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <section class="col-main col-sm-9 wow bounceInUp animated">
                    <div class="page-title">
                        <h2> Modifier Mot de Passe</h2>
                    </div>
                    <div class="static-contain">
                        <fieldset class="group-select">
                            {{ csrf_field() }}
                            {!! Form::model(Auth::user(),array('route'=>['ModifierPassword.update',Auth::user()->id],'method'=>'PUT', 'files'=>'true')) !!}
                            <ul>
                                <li>
                                    <div class="customer-name">
                                        <div class="input-box name-firstname">
                                            <label>Ancien mot de passe<span class="required">*</span></label>
                                            <br>
                                            <input type="password"  name="ancienpassword" id="ancienpassword"  class="input-text required-entry" value="">
                                            </br>
                                        </div>
                                    </div>
                                </li>
                                <li>


                                    <div class="customer-name">

                                        <div class="input-box name-firstname">
                                            <label >Nouveau mot de passe<span class="required">*</span></label>
                                            <br>
                                            <input type="password"  name="password" id="password"  class="input-text required-entry" value="">


                                            <input type="hidden" id="name" name="name" value="{{Auth::user()->name}}" title="First Name" class="input-text ">

                                        </div>
                                        <div class="input-box name-lastname">
                                            <br>

                                            <input type="hidden" id="lastname" name="lastname" value="{{Auth::user()->lastname}}" title="Last Name" class="input-text">

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="customer-name">
                                        <div class="input-box name-firstname">
                                            <label > Confirmer nouveau mot de passe <span class="required">*</span> </label>

                                            <br>
                                            <input type="password" name="password_confirmation" id="password_confirmation"  class="input-text required-entry" value="">

                                            <input type="hidden" id="email" name="email" title="First Name" class="input-text"value="{{Auth::user()->email}}">

                                        </div>
                                        <div class="input-box name-lastname">
                                            <br>
<input type="hidden" name="role" value="{{Auth::User()->role}}">
                                        </div>
                                    </div>
                                </li>


                                <li>
                                    <br>
                                </li>
                                <li>
                                    <br>

                                </li>

                                <li>
                                    <p class="require"><em class="required">* </em>Champs obligatoire</p>
                                    <input type="text" name="hideit" id="hideit" value="">
                                    <div class="buttons-set">
                                        <button type="submit" title="Submit" class="button create-account"> <span> Changez </span> </button>
                                    </div>
                                </li>

                            </ul>
                        </fieldset>
                        {!! Form::close() !!}

                    </div>
                </section>
                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

@stop