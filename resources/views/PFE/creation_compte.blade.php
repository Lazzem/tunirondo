@extends('layouts.head')
@section('content')
  <div class="main-container col5-right-layout">
  <section class="featured-pro container wow bounceInUp animated">
<div class="col-lg-10 col-lg-offset-2">

  <fieldset class="group-select">
  <div class="page-title">
  <h2>CREATE AN ACCOUNT</h2></div>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
      {{ csrf_field() }}
        <ul>
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <li>
        Pseudonyme <span class="required">*</span>
     </li>

       <li>
         <div class="col-md-6">
          <input id="name" type="text" class="input-text" name="name" value="{{ old('name') }}">

          @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
          @endif
        </div>
       </li>
          </div>

     <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
       <li> <label for="email" >E-Mail Address</label><span class="required">*</span></li>

        <li><div class="col-md-6">
          <input id="email" type="email"class="input-text" name="email" value="{{ old('email') }}">

          @if ($errors->has('email'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
          @endif
        </div></li>
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
       <li> <label for="password" >Password</label><span class="required">*</span></li>

        <div class="col-md-6">
         <li> <input id="password" type="password" class="input-text" name="password">

              @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
             @endif
          </li>
        </div>
      </div>

      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
       <li> <label for="password-confirm" >Confirm Password</label><span class="required">*</span></li>

        <div class="col-md-6">
         <li> <input id="password-confirm" type="password" class="input-text" name="password_confirmation">

          @if ($errors->has('password_confirmation'))
            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
          @endif
         </li>
        </div>
      </div>
      <li>
        <input type="checkbox" name="">En génerant un compte,vous acceptez nos <a href=""><mark> Condition Générales.</mark></a>
      </li>

      <div class="form-group">
        <div class="col-md-4 col-md-offset-2">
         <li> <button type="submit"class="button create-account" >
            <i class="fa fa-btn fa-user"></i> INSCRIPTION
          </button>
         </li>
        </div>
      </div>
</ul>
</form>
  </fieldset>
  </div>

  </section>
  </div>
  </div>
  <!--endsection -->
  <footer class="footer wow bounceInUp animated">
    <div class="brand-logo ">
      <div class="container">
        <div class="slider-items-products">
          <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
            <div class="slider-items slider-width-col6">

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo1.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo2.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo3.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo4.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo5.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo6.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo1.png" alt="Image"></a> </div>
              <!-- End Item -->

              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo4.png" alt="Image"></a> </div>
              <!-- End Item -->

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-7">
            <div class="block-subscribe">
              <div class="newsletter">
                <form>
                  <h4>newsletter</h4>
                  <input type="text" placeholder="Enter your email address" class="input-text required-entry validate-email" title="Sign up for our newsletter" id="newsletter1" name="email">
                  <button class="subscribe" title="Subscribe" type="submit"><span>Subscribe</span></button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-5">
            <div class="social">
              <ul>
                <li class="fb"><a href="#"></a></li>
                <li class="tw"><a href="#"></a></li>
                <li class="googleplus"><a href="#"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-middle container">
      <div class="row">
        <div class="col-md-3 col-sm-4">
          <div class="footer-logo"><a href="index.blade.php" title="Logo"><img src="images/logo.png" alt="logo"></a></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu. </p>
          <div class="payment-accept">
            <div><img src="images/payment-1.png" alt="payment"> </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-4">
          <h4>Shopping Guide</h4>
          <ul class="links">
            <li class="first"><a href="#" title="How to buy">How to buy</a></li>
            <li><a href="faq.blade.php" title="FAQs">FAQs</a></li>
            <li><a href="#" title="Payment">Payment</a></li>
            <li><a href="#" title="Shipment&lt;/a&gt;">Shipment</a></li>
            <li><a href="#" title="Where is my order?">Where is my order?</a></li>
            <li class="last"><a href="#" title="Return policy">Return policy</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-sm-4">
          <h4>Style Advisor</h4>
          <ul class="links">
            <li class="first"><a title="Your Account" href="login.blade.php">Your Account</a></li>
            <li><a title="Information" href="#">Information</a></li>
            <li><a title="Addresses" href="#">Addresses</a></li>
            <li><a title="Addresses" href="#">Discount</a></li>
            <li><a title="Orders History" href="#">Orders History</a></li>
            <li class="last"><a title=" Additional Information" href="#">Additional Information</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-sm-4">
          <h4>Information</h4>
          <ul class="links">
            <li class="first"><a href="#" title="privacy policy">Privacy policy</a></li>
            <li><a href="#/" title="Search Terms">Search Terms</a></li>
            <li><a href="#" title="Advanced Search">Advanced Search</a></li>
            <li><a href="contact_us.blade.php" title="Contact Us">Contact Us</a></li>
            <li><a href="#" title="Suppliers">Suppliers</a></li>
            <li class=" last"><a href="#" title="Our stores" class="link-rss">Our stores</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-4">
          <h4>Contact us</h4>
          <div class="contacts-info">
            <address>
            <i class="add-icon">&nbsp;</i>123 Main Street, Anytown, <br>
            &nbsp;CA 12345  USA
            </address>
            <div class="phone-footer"><i class="phone-icon">&nbsp;</i> +1 800 123 1234</div>
            <div class="email-footer"><i class="email-icon">&nbsp;</i> <a href="#">support@magikcommerce.com</a> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-xs-12 coppyright"><center> &copy; 2015. All Rights Reserved. </center> </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->
</div>

<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/parallax.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>
</html>