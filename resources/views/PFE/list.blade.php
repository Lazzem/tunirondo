﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Polo, premium HTML5 &amp; CSS3 template</title>

<!-- Favicons Icon -->
<link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS Style -->
<link rel="stylesheet" href="css/animate.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/revslider.css" type="text/css">
<link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="css/owl.theme.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.css" type="text/css">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,300,700,800,400,600' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="page"> 
  <!-- Header -->
  <header class="header-container">
    <div class="header-top">
      <div class="container">
        <div class="row"> 
          <!-- Header Language -->
          <div class="col-xs-6">
            
            
            <!-- End Header Language --> 
            
            <!-- Header Currency -->
           
            
            <!-- End Header Currency -->
            
            <div class="welcome-msg hidden-xs"> Default welcome msg! </div>
          </div>
          <div class="col-xs-6"> 
            
            <!-- Header Top Links -->
            <div class="toplinks">
              <div class="links">
                <div class="myaccount"><a title="My Account" href="login.blade.php"><span class="hidden-xs">My Account</span></a></div>
                <div class="check"><a title="Checkout" href="checkout.blade.php"><span class="hidden-xs">Checkout</span></a></div>
                <div class="login"><a title="Login" href="login.blade.php"><span class="hidden-xs">Log In</span></a></div>
              </div>
            </div>
            <!-- End Header Top Links --> 
          </div>
        </div>
      </div>
    </div>
    <div class="header container">
      <div class="row">
        <div class="col-lg-2 col-sm-3 col-md-2"> 
          <!-- Header Logo --> 
          <a class="logo" title="Magento Commerce" href="index.blade.php"><img alt="Magento Commerce" src="images/logo.png"></a>
          <!-- End Header Logo --> 
        </div>
        <div class="col-lg-8 col-sm-6 col-md-8"> 
          <!-- Search-col -->
          <div class="search-box">
            <form action="cat" method="POST" id="search_mini_form" name="Categories">
              <select name="category_id" class="cate-dropdown hidden-xs">
                <option value="0">All Categories</option>
                <option value="36">Camera</option>
                <option value="37">Electronics</option>
                <option value="42">&nbsp;&nbsp;&nbsp;Cell Phones</option>
                <option value="43">&nbsp;&nbsp;&nbsp;Cameras</option>
                <option value="44">&nbsp;&nbsp;&nbsp;Laptops</option>
                <option value="45">&nbsp;&nbsp;&nbsp;Hard Drives</option>
                <option value="46">&nbsp;&nbsp;&nbsp;Monitors</option>
                <option value="47">&nbsp;&nbsp;&nbsp;Mouse</option>
                <option value="48">&nbsp;&nbsp;&nbsp;Digital Cameras</option>
                <option value="38">Desktops</option>
                <option value="39">Computer Parts</option>
                <option value="40">Televisions</option>
                <option value="41">Featured</option>
              </select>
              <input type="text" placeholder="Search here..." value="" maxlength="70" class="" name="search" id="search">
              <button id="submit-button" class="search-btn-bg"><span>Search</span></button>
            </form>
          </div>
          <!-- End Search-col --> 
        </div>
        <!-- Top Cart -->
        <div class="col-lg-2 col-sm-3 col-md-2">
          <div class="top-cart-contain">
            <div class="mini-cart">
              <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"> <i class="glyphicon glyphicon-shopping-cart"></i>
                <div class="cart-box"><span class="title">cart</span><span id="cart-total">2 item </span></div>
                </a></div>
              <div>
                <div class="top-cart-content arrow_box">
                  <div class="block-subtitle">Recently added item(s)</div>
                  <ul id="cart-sidebar" class="mini-products-list">
                    <li class="item even"> <a class="product-image" href="#" title="Downloadable Product "><img alt="Downloadable Product " src="products-images/product1.jpg" width="80"></a>
                      <div class="detail-item">
                        <div class="product-details"> <a href="#" title="Remove This Item" onClick="" class="glyphicon glyphicon-remove">&nbsp;</a> <a class="glyphicon glyphicon-pencil" title="Edit item" href="#">&nbsp;</a>
                          <p class="product-name"> <a href="product_detail.blade.php" title="Downloadable Product">Sample Product </a> </p>
                        </div>
                        <div class="product-details-bottom"> <span class="price">$100.00</span> <span class="title-desc">Qty:</span> <strong>1</strong> </div>
                      </div>
                    </li>
                    <li class="item last odd"> <a class="product-image" href="#" title="  Sample Product "><img alt="  Sample Product " src="products-images/product1.jpg" width="80"></a>
                      <div class="detail-item">
                        <div class="product-details"> <a href="#" title="Remove This Item" onClick="" class="glyphicon glyphicon-remove">&nbsp;</a> <a class="glyphicon glyphicon-pencil" title="Edit item" href="#">&nbsp;</a>
                          <p class="product-name"> <a href="#" title="  Sample Product "> Sample Product </a> </p>
                        </div>
                        <div class="product-details-bottom"> <span class="price">$320.00</span> <span class="title-desc">Qty:</span> <strong>2</strong> </div>
                      </div>
                    </li>
                  </ul>
                  <div class="top-subtotal">Subtotal: <span class="price">$420.00</span></div>
                  <div class="actions">
                    <button class="btn-checkout" type="button"><span>Checkout</span></button>
                    <button class="view-cart" type="button"><span>View Cart</span></button>
                  </div>
                </div>
              </div>
            </div>
            <div id="ajaxconfig_info"> <a href="#/"></a>
              <input value="" type="hidden">
              <input id="enable_module" value="1" type="hidden">
              <input class="effect_to_cart" value="1" type="hidden">
              <input class="title_shopping_cart" value="Go to shopping cart" type="hidden">
            </div>
          </div>
        </div>
        <!-- End Top Cart --> 
      </div>
    </div>
  </header>
  <!-- end header --> 
   <!-- Navbar -->
  <nav>
    <div class="container">
      <div class="nav-inner">
        <div class="logo-small"> <a class="logo" title="Magento Commerce" href="index.blade.php"><img alt="Magento Commerce" src="images/logo.png"></a> </div>
        <!-- mobile-menu -->
        <div class="hidden-desktop" id="mobile-menu">
          <ul class="navmenu">
            <li>
              <div class="menutop">
                <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
                <h2>Menu</h2>
              </div>
              <ul class="submenu">
                <li>
                  <ul class="topnav">
                    <li class="level0 nav-6 level-top first parent"> <a class="level-top" href="#"> <span>Pages</span> </a>
                      <ul class="level0">
                        <li class="level1 first"><a href="grid.blade.php"><span>Grid</span></a></li>
                        <li class="level1 nav-10-2"> <a href="list.blade.php"> <span>List</span> </a> </li>
                        <li class="level1 nav-10-3"> <a href="product_detail.blade.php"> <span>Product Detail</span> </a> </li>
                        <li class="level1 nav-10-4"> <a href="shopping_cart.blade.php"> <span>Shopping Cart</span> </a> </li>
                        <li class="level1 first"><a href="checkout.blade.php"><span>Checkout</span></a> </li>
                        <li class="level1 nav-10-4"> <a href="wishlist.html"> <span>Wishlist</span> </a> </li>
                        <li class="level1"> <a href="dashboard.blade.php"> <span>Dashboard</span> </a> </li>
                        <li class="level1"> <a href="multiple_addresses.blade.php"> <span>Multiple Addresses</span> </a> </li>
                        <li class="level1"> <a href="about_us.blade.php"> <span>About us</span> </a> </li>
                        <li class="level1"> <a href="compare.blade.php"> <span>Compare</span> </a> </li>
                        <li class="level1"> <a href="faq.blade.php"> <span>FAQ</span> </a> </li>
                        <li class="level1"> <a href="login.blade.php"> <span>Login</span> </a> </li>
                        <li class="level1"> <a href="quick_view.blade.php"> <span>Quick view</span> </a> </li>
                        <li class="level1"><a href="blog.blade.php"><span>Blog</span></a> </li>
                        <li class="level1"><a href="contact_us.blade.php"><span>Contact us</span></a> </li>
                        <li class="level1"><a href="404error.blade.php"><span>404 Error Page</span></a> </li>
                      </ul>
                    </li>
                    <li class="level0 nav-7 level-top parent"> <a class="level-top" href="grid.blade.php"> <span>Women</span> </a>
                      <ul class="level0">
                        <li class="level1 nav-1-1 first parent"> <a href="grid.blade.php"> <span>Clothing</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-1-1-1 first"> <a href="grid.blade.php"> <span>Western Wear</span> </a> </li>
                            <li class="level2 nav-1-1-2"> <a href="grid.blade.php"> <span>Night Wear</span> </a> </li>
                            <li class="level2 nav-1-1-3"> <a href="grid.blade.php"> <span>Ethnic Wear</span> </a> </li>
                            <li class="level2 nav-1-1-4 last"> <a href="grid.blade.php"> <span>Designer Wear</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-1-2 parent"> <a href="grid.blade.php"> <span>Watches</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-1-2-5 first"> <a href="grid.blade.php"> <span>Fashion</span> </a> </li>
                            <li class="level2 nav-1-2-6"> <a href="grid.blade.php"> <span>Dress</span> </a> </li>
                            <li class="level2 nav-1-2-7"> <a href="grid.blade.php"> <span>Sports</span> </a> </li>
                            <li class="level2 nav-1-2-8 last"> <a href="grid.blade.php"> <span>Casual</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-1-3 parent"> <a href="grid.blade.php"> <span>Styliest Bag</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-1-3-9 first"> <a href="grid.blade.php"> <span>Clutch Handbags</span> </a> </li>
                            <li class="level2 nav-1-3-10"> <a href="grid.blade.php"> <span>Diaper Bags</span> </a> </li>
                            <li class="level2 nav-1-3-11"> <a href="grid.blade.php"> <span>Bags</span> </a> </li>
                            <li class="level2 nav-1-3-12 last"> <a href="grid.blade.php"> <span>Hobo Handbags</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-1-4 last parent"> <a href="grid.blade.php"> <span>Material Bag</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-1-4-13 first"> <a href="grid.blade.php"> <span>Beaded Handbags</span> </a> </li>
                            <li class="level2 nav-1-4-14"> <a href="grid.blade.php"> <span>Fabric Handbags</span> </a> </li>
                            <li class="level2 nav-1-4-15"> <a href="grid.blade.php"> <span>Handbags</span> </a> </li>
                            <li class="level2 nav-1-4-16 last"> <a href="grid.blade.php"> <span>Leather Handbags</span> </a> </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li class="level0 nav-8 level-top parent"> <a class="level-top" href="grid.blade.php"> <span>Men</span> </a>
                      <ul class="level0">
                        <li class="level1 nav-2-1 first parent"> <a href="grid.blade.php"> <span>Clothing</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-2-1-1 first"> <a href="grid.blade.php"> <span>Casual Wear</span> </a> </li>
                            <li class="level2 nav-2-1-2"> <a href="grid.blade.php"> <span>Formal Wear</span> </a> </li>
                            <li class="level2 nav-2-1-3"> <a href="grid.blade.php"> <span>Ethnic Wear</span> </a> </li>
                            <li class="level2 nav-2-1-4 last"> <a href="grid.blade.php"> <span>Denims</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-2-2 parent"> <a href="grid.blade.php"> <span>Shoes</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-2-2-5 first"> <a href="grid.blade.php"> <span>Formal Shoes</span> </a> </li>
                            <li class="level2 nav-2-2-6"> <a href="grid.blade.php"> <span>Sport Shoes</span> </a> </li>
                            <li class="level2 nav-2-2-7"> <a href="grid.blade.php"> <span>Canvas Shoes</span> </a> </li>
                            <li class="level2 nav-2-2-8 last"> <a href="grid.blade.php"> <span>Leather Shoes</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-2-3 parent"> <a href="grid.blade.php"> <span>Watches</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-2-3-9 first"> <a href="grid.blade.php"> <span>Digital</span> </a> </li>
                            <li class="level2 nav-2-3-10"> <a href="grid.blade.php"> <span>Chronograph</span> </a> </li>
                            <li class="level2 nav-2-3-11"> <a href="grid.blade.php"> <span>Sports</span> </a> </li>
                            <li class="level2 nav-2-3-12 last"> <a href="grid.blade.php"> <span>Casual</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-2-4 parent"> <a href="grid.blade.php"> <span>Jackets</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-2-4-13 first"> <a href="grid.blade.php"> <span>Coats</span> </a> </li>
                            <li class="level2 nav-2-4-14"> <a href="grid.blade.php"> <span>Formal Jackets</span> </a> </li>
                            <li class="level2 nav-2-4-15"> <a href="grid.blade.php"> <span>Leather Jackets</span> </a> </li>
                            <li class="level2 nav-2-4-16 last"> <a href="grid.blade.php"> <span>Blazers</span> </a> </li>
                          </ul>
                        </li>
                        <li class="level1 nav-2-5 last parent"> <a href="grid.blade.php"> <span>Sunglasses</span> </a>
                          <ul class="level1">
                            <li class="level2 nav-2-5-17 first"> <a href="grid.blade.php"> <span>Ray Ban</span> </a> </li>
                            <li class="level2 nav-2-5-18"> <a href="grid.blade.php"> <span>Fasttrack</span> </a> </li>
                            <li class="level2 nav-2-5-19"> <a href="grid.blade.php"> <span>Police</span> </a> </li>
                            <li class="level2 nav-2-5-20 last"> <a href="grid.blade.php"> <span>Oakley</span> </a> </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li class="level0 nav-9 level-top "> <a class="level-top" href="blog.blade.php"> <span>Blog</span> </a> </li>
                    <li class="level0 nav-10 level-top "> <a class="level-top" href="blog.blade.php"> <span>Custom</span> </a> </li>
                    <li class="level0 nav-11 level-top last parent "> <a class="level-top" href="contact_us.blade.php"> <span>Contact</span> </a> </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
          <!--navmenu--> 
        </div>
        
        <!--End mobile-menu -->
        <ul id="nav" class="hidden-xs">
          <li id="nav-home" class="level0 parent drop-menu"><a href="index.blade.php"><span>Home</span> </a>
            <ul class="level1">
              <li class="level1 first parent"><a href="../../Variation1/blue/index.html"><span>Home Version 1</span></a> </li>
              <li class="level1 first parent"><a href="../../Variation2/blue/index.html"><span>Home Version 2</span></a> </li>
              <li class="level1 parent"><a href="../../Variation1/blue/index.html"><span>Blue</span></a> </li>
              <li class="level1 parent"><a href="../../Variation1/red/index.html"><span>Red</span></a> </li>
              <li class="level1 parent"><a href="../../Variation1/lavender/index.html"><span>Lavender</span></a> </li>
              <li class="level1 parent"><a href="../../Variation1/green/index.html"><span>Green</span></a> </li>
              <li class="level1 parent"><a href="../../Variation1/emerald/index.html"><span>Emerald</span></a> </li>
            </ul>
          </li>
          <li class="level0 parent drop-menu"><a href="#"><span>Pages</span> </a>
            <ul class="level1">
              <li class="level1 first"><a href="grid.blade.php"><span>Grid</span></a></li>
              <li class="level1 nav-10-2"> <a href="list.blade.php"> <span>List</span> </a> </li>
              <li class="level1 nav-10-3"> <a href="product_detail.blade.php"> <span>Product Detail</span> </a> </li>
              <li class="level1 nav-10-4"> <a href="shopping_cart.blade.php"> <span>Shopping Cart</span> </a> </li>
              <li class="level1 first parent"><a href="checkout.blade.php"><span>Checkout</span></a> </li>
              <li class="level1 nav-10-4"> <a href="wishlist.html"> <span>Wishlist</span> </a> </li>
              <li class="level1"> <a href="dashboard.blade.php"> <span>Dashboard</span> </a> </li>
              <li class="level1"> <a href="multiple_addresses.blade.php"> <span>Multiple Addresses</span> </a> </li>
              <li class="level1"> <a href="about_us.blade.php"> <span>About us</span> </a> </li>
              <li class="level1"> <a href="compare.blade.php"> <span>Compare</span> </a> </li>
              <li class="level1"> <a href="faq.blade.php"> <span>FAQ</span> </a> </li>
              <li class="level1"> <a href="login.blade.php"> <span>Login</span> </a> </li>
              <li class="level1"> <a href="quick_view.blade.php"> <span>Quick view </span> </a> </li>
              <li class="level1 first parent"><a href="blog.blade.php"><span>Blog</span></a>
                <ul class="level2">
                  <li class="level2 nav-2-1-1 first"><a href="blog_detail.blade.php"><span>Blog Detail</span></a></li>
                </ul>
              </li>
              <li class="level1"><a href="contact_us.blade.php"><span>Contact us</span></a> </li>
              <li class="level1"><a href="404error.blade.php"><span>404 Error Page</span></a> </li>
            </ul>
          </li>
          <li class="level0 nav-6 level-top parent"> <a href="grid.blade.php" class="level-top active"> <span>Women</span> </a>
            <div class="level0-wrapper dropdown-6col">
              <div class="level0-wrapper2">
                <div class="nav-block nav-block-center grid12-8 itemgrid itemgrid-4col"> 
                  
                  <!--mega menu-->
                  
                  <ul class="level0">
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Clothing</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Western Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Night Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Ethnic Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Designer Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Watches</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Fashion</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Dress</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Sports</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Casual</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Styliest Bag</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Clutch Handbags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Diaper Bags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Bags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Hobo Handbags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Material Bag</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Beaded Handbags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Fabric Handbags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Handbags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Leather Handbags</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                  </ul>
                  <!--level0-->
                  
                  <div class="fur-des">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa.</p>
                  </div>
                </div>
                <div class="nav-block nav-block-right std grid12-4">
                  <div class="static-img-block"><a href="#"><img src="images/nav-img1.jpg" alt="Responsive"></a></div>
                </div>
              </div>
            </div>
          </li>
          <li class="level0 nav-5 level-top first"> <a class="level-top" href="grid.blade.php"> <span>Men</span> </a>
            <div class="level0-wrapper dropdown-6col">
              <div class="level0-wrapper2">
                <div class="nav-block nav-block-center">
                  <ul class="level0">
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Clothing</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Casual Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Formal Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Ethnic Wear</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Denims</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Shoes</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Formal Shoes</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Sport Shoes</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Canvas Shoes</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Leather Shoes</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Watches</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Digital</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Chronograph</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Sports</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Casual</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Jackets</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Coats</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Formal Jackets</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Leather Jackets</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Blazers</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                    <li class="level3 nav-6-1 parent item"> <a href="grid.blade.php"><span>Sunglasses</span></a>
                      <!--sub sub category-->
                      
                      <ul class="level1">
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Ray Ban</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Fasttrack</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Police</span></a> </li>
                        <!--level2 nav-6-1-1-->
                        <li class="level2 nav-6-1-1"> <a href="grid.blade.php"><span>Oakley</span></a> </li>
                        <!--level2 nav-6-1-1-->
                      </ul>
                      <!--level1--> 
                      
                      <!--sub sub category--> 
                      
                    </li>
                    <!--level3 nav-6-1 parent item-->
                    
                  </ul>
                  <!--level0--> 
                </div>
              </div>
              <div class="nav-add">
                <div class="push_item">
                  <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="sunglass"></a></div>
                </div>
                <div class="push_item">
                  <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="watch"></a></div>
                </div>
                <div class="push_item">
                  <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="jeans"></a></div>
                </div>
                <div class="push_item">
                  <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="shoes"></a></div>
                </div>
                <div class="push_item push_item_last">
                  <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="swimwear"></a></div>
                </div>
              </div>
            </div>
          </li>
          <li class="level0 parent drop-menu"><a href="grid.blade.php"><span>Sub menu </span>
            <!--<span class="category-label-hot">Hot</span> --> 
            </a>
            <ul class="level1">
              <li class="level1 first parent"><a href="grid"><span>Submenu</span></a>
                <ul class="level2">
                  <li class="level2 first"><a href="#"><span>Menu1</span></a></li>
                  <li class="level2 nav-1-1-2"><a href="#"><span>Menu1</span></a></li>
                  <li class="level2 nav-1-1-3"><a href="#"><span>Menu2</span></a></li>
                  <li class="level2 nav-1-1-4"><a href="#"><span>Menu3</span></a></li>
                  <li class="level2 nav-1-1-5 last"><a href="#"><span>Menu4</span></a></li>
                </ul>
              </li>
              <li class="level1 first parent"><a href="#"><span>Submenu</span></a>
                <ul class="level2">
                  <li class="level2 first"><a href="#"><span>Menu1</span></a></li>
                  <li class="level2 nav-1-1-2"><a href="#"><span>Menu1</span></a></li>
                  <li class="level2 nav-1-1-3"><a href="#"><span>Menu2</span></a></li>
                  <li class="level2 nav-1-1-4"><a href="#"><span>Menu3</span></a></li>
                  <li class="level2 nav-1-1-5 last"><a href="#"><span>Menu4</span></a></li>
                </ul>
              </li>
              <li class="level1 parent"><a href="#"><span>Submenu</span></a> </li>
            </ul>
          </li>
          <li class="level0 nav-8 level-top"> <a href="blog.blade.php" class="level-top"> <span>Blog</span> </a> </li>
          <li class="level0 nav-8 level-top"> <a href="contact_us.blade.php" class="level-top"> <span>Contact</span> </a> </li>
          <li class="nav-custom-link level0 level-top parent"> <a class="level-top" href="#"><span>Custom</span></a>
            <div class="level0-wrapper custom-menu">
              <div class="header-nav-dropdown-wrapper clearer">
                <div class="grid12-5">
                  <div class="custom_img"><a href="#"><img src="images/custom-img1.jpg" alt="custom img1"></a></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                  <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                </div>
                <div class="grid12-5">
                  <div class="custom_img"><a href="#"><img src="images/custom-img2.jpg" alt="custom img2"></a></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                  <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                </div>
                <div class="grid12-5">
                  <div class="custom_img"><a href="#"><img src="images/custom-img3.jpg" alt="custom img3"></a></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                  <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                </div>
                <div class="grid12-5">
                  <div class="custom_img"><a href="#"><img src="images/custom-img4.jpg" alt="custom img4"></a></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                  <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- end nav -->   
  <!-- breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <ul>
          <li class="home"> <a href="index.blade.php" title="Go to Home Page">Home</a><span>&mdash;›</span></li>
          <li class=""> <a href="grid.blade.php" title="Go to Home Page">Women</a><span>&mdash;›</span></li>
          <li class="category13"><strong>Tops &amp; Tees</strong></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- end breadcrumbs --> 
  <!-- Two columns content -->
  <div class="main-container col2-left-layout">
    <div class="main container">
      <div class="row">
        <section class="col-main col-sm-9 col-sm-push-3 wow bounceInUp animated">
          <div class="category-description std">
            <div class="category-image"><img src="images/women_banner.png" alt="cat imges " title="Sofas "> </div>
          </div>
          <div class="category-title">
            <h1>Tops &amp; Tees</h1>
          </div>
          <div class="category-products">
            <div class="toolbar">
              <div class="sorter">
                <div class="view-mode"> <a href="grid.blade.php" title="Grid" class="button button-grid">Grid</a>&nbsp; <span title="List" class="button button-active button-list">List</span>&nbsp; </div>
              </div>
              <div id="sort-by">
                <label class="left">Sort By: </label>
                <ul>
                  <li><a href="#">Position<span class="right-arrow"></span></a>
                    <ul>
                      <li><a href="#">Name</a></li>
                      <li><a href="#">Price</a></li>
                      <li><a href="#">Position</a></li>
                    </ul>
                  </li>
                </ul>
                <a class="button-asc left" href="#" title="Set Descending Direction"><span class="glyphicon glyphicon-arrow-up"></span></a> </div>
              <div class="pager">
                <div id="limiter">
                  <label>View: </label>
                  <ul>
                    <li><a href="#">15<span class="right-arrow"></span></a>
                      <ul>
                        <li><a href="#">20</a></li>
                        <li><a href="#">30</a></li>
                        <li><a href="#">35</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <div class="pages">
                  <label>Page:</label>
                  <ul class="pagination">
                    <li><a href="#">&laquo;</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <ol id="products-list" class="products-list">
              <li class="item odd">
                <div class="product-image"> <a href="#" title="HTC Rhyme Sense"> <img class="small-image" src="products-images/product1.jpg" alt="HTC Rhyme Sense" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box">
                    <p class="old-price"> <span class="price-label"></span> <span id="old-price-212" class="price"> $442.99 </span> </p>
                    <p class="special-price"> <span class="price-label"></span> <span id="product-price-212" class="price"> $222.99 </span> </p>
                  </div>
                  <div class="ratings">
                    <div class="rating-box">
                      <div class="rating"></div>
                    </div>
                    <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#review-form">Add Your Review</a> </p>
                  </div>
                  <div class="desc std">
                    <p>Sed volutpat ac massa eget 
                      lacinia.  
                      Aenean volutpat lacus at dolor blandit </p>
                    <p>Sed sed interdum diam. Donec sit ametenim tempor, dapibus nunc eu, 
                      tincidunt mi. Vivamus dictum nec... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item even">
                <div class="product-image"> <a href="#microsoft-natural-ergonomic-keyboard-4000.html" title="Microsoft Natural Keyboard"> <img class="small-image" src="products-images/product1.jpg" alt="Microsoft Natural Keyboard" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-159"> <span class="price">$99.99</span> </span> </div>
                  <div class="desc std">
                    <p>Sed volutpat ac massa eget 
                      lacinia. Suspendisse non purus semper, tellus vel, tristique urna. 
                      Aenean volutpat lacus at dolor blandit. </p>
                    <p>Sed sed interdum diam. Donec sit ametenim tempor, dapibus nunc eu, 
                      tincidunt mi. Vivamus dignissimm ... <a class="link-learn" title="" href="#">Learn More</a></p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item odd">
                <div class="product-image"> <a href="#30-flat-panel-tft-lcd-cinema-hd-monitor.html" title="30&quot; Flat-Panel HD Monitor"> <img class="small-image" src="products-images/product1.jpg" alt="30&quot; Flat-Panel HD Monitor" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-157"> <span class="price">$699.99</span> </span> </div>
                  <div class="desc std">Computer games, digital photo 
                    editing and graphic applications will astound you on this huge 30" 
                    flat-panel monitor.
                    <p>Sed sed interdum diam. Donec sit ametenim tempor, dapibus nunc eu, 
                      tincidunt mi. </p>
                    <p>Phasellus consequat id purus in convallis. Nulla quis... <a class="link-learn" title="" href="#">Learn More</a></p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item even">
                <div class="product-image"> <a href="#19-widescreen-flat-panel-lcd-monitor.html" title="19&quot; Widescreen LCD Monitor"> <img class="small-image" src="products-images/product1.jpg" alt="19&quot; Widescreen LCD Monitor" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-156"> <span class="price">$399.99</span> </span> </div>
                  <div class="desc std">2 ms response time; 10,000:1 contrast ratio; 300 cd/m² brightness; 1440 x 900 maximum resolution; DVI-D and 15-pin D-sub inputs
                    <p>Phasellus consequat id purus 
                      pretium enimnec, tristique... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item odd">
                <div class="product-image"> <a href="#250gb-5400rpm.html" title="Seagate 250GB HD "> <img class="small-image" src="products-images/product1.jpg" alt="Seagate 250GB HD " width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-155"> <span class="price">$99.00</span> </span> </div>
                  <div class="desc std">1 TB - 7200RPM, SATA 3.0Gb/s, 32MB Cache
                    <p>Maecenas vehicula volutpat elit, in interdum lacus faucibus sit amet. </p>
                    <p>Sed sed interdum diam. Donec sit ametenim tempor, dapibus nunc eu, 
                      tincidunt mi. Vivamus dignissim nisl. Donec eget feugiat ante. 
                      Integerarcu libero, dictum nec congue sed, faucibus ... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item even">
                <div class="product-image"> <a href="#500gb-5400rpm.html" title="Seagate 500GB HD"> <img class="small-image" src="products-images/product1.jpg" alt="Seagate 500GB HD" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-154"> <span class="price">$299<span class="sub">.00</span></span> </span> </div>
                  <div class="desc std">1 TB - 7200RPM, SATA 3.0Gb/s, 32MB Cache
                    <p>Aenean volutpat lacus at dolor blandit, 
                      vitae lobortisante semper. Ut 
                      bibendum metusfringilla, in interdum lacus faucibus sit amet. </p>
                    <p> Donec eget feugiat ante. 
                      Integerarcu libero... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item odd">
                <div class="product-image"> <a href="#intel-core-2-extreme-qx9775-3-20ghz-retail.html" title="Intel Core 2 Extreme QX9775"> <img class="small-image" src="products-images/product1.jpg" alt="Intel Core 2 Extreme QX9775" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-153"> <span class="price">$2,049.99</span> </span> </div>
                  <div class="desc std">Intel Core 2 Extreme QX9775 
                    Processor BX80574QX9775 - 45nm, 3.20GHz, 12MB Cache, 1600MHz FSB,
                    <p>Vivamus dignissim nisl eu euismod ullamcorper. Donec 
                      pellentesque diam id est tristique vestibulum. Donec eget feugiat ante. 
                      Integerarcu libero, dictum nec congue sed, faucibus sit amet lectus. </p>
                    <p>Phasellus consequat... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item even">
                <div class="product-image"> <a href="#24-widescreen-flat-panel-lcd-monitor.html" title="24&quot; Widescreen LCD Monitor"> <img class="small-image" src="products-images/product1.jpg" alt="24&quot; Widescreen LCD Monitor" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-152"> <span class="price">$699.99</span> </span> </div>
                  <div class="desc std">5 ms response time; 10,000:1 contrast ratio; 400 cd/m² brightness; 1920 x 1200 maximum resolution; DVI-D and 15-pin D-sub inputs
                    <p> tellus vel, tristique urna. </p>
                    <p>Phasellus consequat id purus in convallis. Nulla quis nunc auctor, 
                      pretium enimnec, tristique magna... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item odd">
                <div class="product-image"> <a href="#microsoft-wireless-optical-mouse-5004.html" title="Logitech Optical Trackman"> <img class="small-image" src="products-images/product1.jpg" alt="Logitech Optical Trackman" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-160"> <span class="price">$79.99</span> </span> </div>
                  <div class="desc std">Our most advanced trackball yet. 
                    
                    Save space and eliminate desktop clutter.
                    <p>Donec eget feugiat ante. 
                      Integerarcu libero, dictum nec congue sed, faucibus sit amet lectus. </p>
                    <p>Vivamus vitae arcu faucibus, dictum 
                      magna vel, adipiscing... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
              <li class="item last even">
                <div class="product-image"> <a href="#" title="Logitech diNovo Edge Keyboard"> <img class="small-image" src="products-images/product1.jpg" alt="Logitech diNovo Edge Keyboard" width="230"> </a> </div>
                <div class="product-shop">
                  <h2 class="product-name"><a title=" Sample Product" href="product_detail.blade.php"> Sample Product </a></h2>
                  <div class="price-box"> <span class="regular-price" id="product-price-161"> <span class="price">$239.99</span> </span> </div>
                  <div class="ratings">
                    <div class="rating-box">
                      <div class="rating"></div>
                    </div>
                    <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#review-form">Add Your Review</a> </p>
                  </div>
                  <div class="desc std">Li-Ion powered.
                    <p>Sed volutpat ac massa eget lacinia. Suspendisse non purus semper, 
                      vitae lobortisante semper. </p>
                    <p>Integerarcu libero, dictum nec congue sed, faucibus sit... <a class="link-learn" title="" href="#">Learn More</a> </p>
                  </div>
                  <div class="actions">
                    <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                    <span class="add-to-links"> <a title="Add to Wishlist" class="button link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a> <a title="Add to Compare" class="button link-compare" href="compare.blade.php"><span>Add to Compare</span></a> </span> </div>
                </div>
              </li>
            </ol>
          </div>
        </section>
        
      </div>
    </div>
  </div>
  <!-- End Two columns content --> 
  <!-- Footer -->
  
  <footer class="footer wow bounceInUp animated">
    <div class="brand-logo ">
      <div class="container">
        <div class="slider-items-products">
          <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
            <div class="slider-items slider-width-col6"> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo1.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo2.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo3.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo4.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo5.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo6.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo1.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
              <!-- Item -->
              <div class="item"> <a href="#x"><img src="images/b-logo4.png" alt="Image"></a> </div>
              <!-- End Item --> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-7">
            <div class="block-subscribe">
              <div class="newsletter">
                <form>
                  <h4>newsletter</h4>
                  <input type="text" placeholder="Enter your email address" class="input-text required-entry validate-email" title="Sign up for our newsletter" id="newsletter1" name="email">
                  <button class="subscribe" title="Subscribe" type="submit"><span>Subscribe</span></button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-5">
            <div class="social">
              <ul>
                <li class="fb"><a href="#"></a></li>
                <li class="tw"><a href="#"></a></li>
                <li class="googleplus"><a href="#"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-middle container">
      <div class="row">
        <div class="col-md-3 col-sm-4">
          <div class="footer-logo"><a href="index.blade.php" title="Logo"><img src="images/logo.png" alt="logo"></a></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu. </p>
          <div class="payment-accept">
            <div><img src="images/payment-1.png" alt="payment"></div>
          </div>
        </div>
        <div class="col-md-2 col-sm-4">
          <h4>Shopping Guide</h4>
          <ul class="links">
            <li class="first"><a href="#" title="How to buy">How to buy</a></li>
            <li><a href="faq.blade.php" title="FAQs">FAQs</a></li>
            <li><a href="#" title="Payment">Payment</a></li>
            <li><a href="#" title="Shipment&lt;/a&gt;">Shipment</a></li>
            <li><a href="#" title="Where is my order?">Where is my order?</a></li>
            <li class="last"><a href="#" title="Return policy">Return policy</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-sm-4">
          <h4>Style Advisor</h4>
          <ul class="links">
            <li class="first"><a title="Your Account" href="login.blade.php">Your Account</a></li>
            <li><a title="Information" href="#">Information</a></li>
            <li><a title="Addresses" href="#">Addresses</a></li>
            <li><a title="Addresses" href="#">Discount</a></li>
            <li><a title="Orders History" href="#">Orders History</a></li>
            <li class="last"><a title=" Additional Information" href="#">Additional Information</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-sm-4">
          <h4>Information</h4>
          <ul class="links">
            <li class="first"><a href="#" title="privacy policy">Privacy policy</a></li>
            <li><a href="#/" title="Search Terms">Search Terms</a></li>
            <li><a href="#" title="Advanced Search">Advanced Search</a></li>
            <li><a href="contact_us.blade.php" title="Contact Us">Contact Us</a></li>
            <li><a href="#" title="Suppliers">Suppliers</a></li>
            <li class=" last"><a href="#" title="Our stores" class="link-rss">Our stores</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-4">
          <h4>Contact us</h4>
          <div class="contacts-info">
            <address>
            <i class="add-icon">&nbsp;</i>123 Main Street, Anytown, <br>
            &nbsp;CA 12345  USA
            </address>
            <div class="phone-footer"><i class="phone-icon">&nbsp;</i> +1 800 123 1234</div>
            <div class="email-footer"><i class="email-icon">&nbsp;</i> <a href="#">support@magikcommerce.com</a> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-xs-12 coppyright"> &copy; 2015. All Rights Reserved. Designed by <a href="#">magikcommerce.com</a> </div>
          <div class="col-sm-7 col-xs-12 company-links">
            <ul class="links">
              <li><a href="#" title="Magento Themes">Magento Themes</a></li>
              <li><a href="#" title="Premium Themes">Premium Themes</a></li>
              <li><a href="#" title="Responsive Themes">Responsive Themes</a></li>
              <li class="last"><a href="#" title="Magento Extensions">Magento Extensions</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer --> 
  
</div>

<!-- JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="js/parallax.js"></script> 
<script type="text/javascript" src="js/common.js"></script> 
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>
</html>