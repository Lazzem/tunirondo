<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Polo, premium HTML5 &amp; CSS3 template</title>

    <!-- Favicons Icon -->
    <link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />

    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS Style -->
    <link rel="stylesheet" href="{{URL::asset('css/animate.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome.css')}}" type="text/css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,300,700,800,400,600' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="page">
    <!-- Header -->
    <header class="header-container">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <!-- Header Language -->
                    <div class="col-xs-6">


                        <!-- End Header Language -->

                        <!-- Header Currency -->


                        <!-- End Header Currency -->

                        <div class="welcome-msg hidden-xs"> Default welcome msg! </div>
                    </div>
                    <div class="col-xs-6">

                        <!-- Header Top Links -->
                        <div class="toplinks">
                            <div class="links">
                                <div class="myaccount"><a title="My Account" href="/PFE/login"><span class="hidden-xs">My Account</span></a></div>
                                <div class="check"><a title="Checkout" href="/PFE/checkout"><span class="hidden-xs">Checkout</span></a></div>
                                <div class="login"><a title="Login" href="/PFE/login"><span class="hidden-xs">Log In</span></a></div>
                            </div>
                        </div>
                        <!-- End Header Top Links -->
                    </div>
                </div>
            </div>
        </div>
        <div class="header container">
            <div class="row">
                <div class="col-lg-2 col-sm-3 col-md-2">
                    <!-- Header Logo -->
                    <a class="logo" title="Magento Commerce" href="/PFE/index"><img alt="Magento Commerce" src="{{URL::asset('images/logo.png')}}"></a>
                    <!-- End Header Logo -->
                </div>
                <div class="col-lg-8 col-sm-6 col-md-8">
                    <!-- Search-col -->
                    <div class="search-box">
                        <form action="cat" method="POST" id="search_mini_form" name="Categories">
                            <select name="category_id" class="cate-dropdown hidden-xs">
                                <option value="0">Tous les Categories </option>
                                <option value="36">Sortie</option>
                                <option value="32">&nbsp;&nbsp;&nbsp;Randonneé</option>
                                <option value="33">&nbsp;&nbsp;&nbsp;Voyages</option>
                                <option value="37">Visiter</option>
                                <option value="42">&nbsp;&nbsp;&nbsp;Théâtre</option>
                                <option value="43">&nbsp;&nbsp;&nbsp;Cinema</option>
                                <option value="44">&nbsp;&nbsp;&nbsp;Musées</option>
                                <option value="45">&nbsp;&nbsp;&nbsp;Mounuments</option>


                            </select>
                            <input type="text" placeholder="Search here..." value="" maxlength="70" class="" name="search" id="search">
                            <button id="submit-button" class="search-btn-bg"><span>Search</span></button>
                        </form>
                    </div>
                    <!-- End Search-col -->
                </div>
                <!-- Top Cart -->
                <div class="col-lg-2 col-sm-3 col-md-2">
                    <div class="top-cart-contain">
                        <div class="mini-cart">
                            <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"> <i class="glyphicon glyphicon-shopping-cart"></i>
                                    <div class="cart-box"><span class="title">cart</span><span id="cart-total">2 item </span></div>
                                </a></div>
                            <div>
                                <div class="top-cart-content arrow_box">
                                    <div class="block-subtitle">Recently added item(s)</div>
                                    <ul id="cart-sidebar" class="mini-products-list">
                                        <li class="item even"> <a class="product-image" href="#" title="Downloadable Product "><img alt="Downloadable Product " src="products-images/product1.jpg" width="80"></a>
                                            <div class="detail-item">
                                                <div class="product-details"> <a href="#" title="Remove This Item" onClick="" class="glyphicon glyphicon-remove">&nbsp;</a> <a class="glyphicon glyphicon-pencil" title="Edit item" href="#">&nbsp;</a>
                                                    <p class="product-name"> <a href="product_detail.html" title="Downloadable Product">Sample Product</a> </p>
                                                </div>
                                                <div class="product-details-bottom"> <span class="price">$100.00</span> <span class="title-desc">Qty:</span> <strong>1</strong> </div>
                                            </div>
                                        </li>
                                        <li class="item last odd"> <a class="product-image" href="#" title="  Sample Product "><img alt="  Sample Product " src="products-images/product1.jpg" width="80"></a>
                                            <div class="detail-item">
                                                <div class="product-details"> <a href="#" title="Remove This Item" onClick="" class="glyphicon glyphicon-remove">&nbsp;</a> <a class="glyphicon glyphicon-pencil" title="Edit item" href="#">&nbsp;</a>
                                                    <p class="product-name"> <a href="#" title="  Sample Product "> Sample Product </a> </p>
                                                </div>
                                                <div class="product-details-bottom"> <span class="price">$320.00</span> <span class="title-desc">Qty:</span> <strong>2</strong> </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="top-subtotal">Subtotal: <span class="price">$420.00</span></div>
                                    <div class="actions">
                                        <button class="btn-checkout" type="button"><span>Checkout</span></button>
                                        <button class="view-cart" type="button"><span>View Cart</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="ajaxconfig_info"> <a href="#/"></a>
                            <input value="" type="hidden">
                            <input id="enable_module" value="1" type="hidden">
                            <input class="effect_to_cart" value="1" type="hidden">
                            <input class="title_shopping_cart" value="Go to shopping cart" type="hidden">
                        </div>
                    </div>
                </div>
                <!-- End Top Cart -->
            </div>
        </div>
    </header>
    <!-- end header -->
    <!-- Navbar -->
    <nav>
        <div class="container">
            <div class="nav-inner">
                <div class="logo-small"> <a class="logo" title="Magento Commerce" href="index.html"><img alt="Magento Commerce" src="images/logo.png"></a> </div>
                <!-- mobile-menu -->
                <div class="hidden-desktop" id="mobile-menu">
                    <ul class="navmenu">
                        <li>
                            <div class="menutop">
                                <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
                                <h2>Menu</h2>
                            </div>
                            <ul class="submenu">
                                <li>
                                    <ul class="topnav">
                                        <li class="level0 nav-6 level-top first parent"> <a class="level-top" href="#"> <span>decouvrir</span> </a>

                                        </li>
                                        <li class="level0 nav-7 level-top parent"> <a class="level-top" href="grid.html"> <span>Visiter</span> </a>
                                            <ul class="level0">
                                                <li class="level1 nav-1-1 first parent"> <a href="grid.html"> <span>Musées</span> </a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-1-1-1 first"> <a href="grid.html"> <span>Musée National du Bardo</span> </a> </li>
                                                        <li class="level2 nav-1-1-2"> <a href="grid.html"> <span>Musée National du Carthage</span> </a> </li>
                                                        <li class="level2 nav-1-1-3"> <a href="grid.html"> <span>Musée d'El Jem</span> </a> </li>
                                                        <li class="level2 nav-1-1-4 last"> <a href="grid.html"> <span>Musée Chemtou</span> </a> </li>
                                                    </ul>
                                                </li>
                                                <li class="level1 nav-1-2 parent"> <a href="grid.html"> <span>Théâtre</span> </a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-1-2-5 first"> <a href="grid.html"> <span>Genre Comédie </span> </a> </li>
                                                        <li class="level2 nav-1-2-6"> <a href="grid.html"> <span>Genre Drame</span> </a> </li>
                                                        <li class="level2 nav-1-2-7"> <a href="grid.html"> <span>Genre Musicale</span> </a> </li>
                                                        <li class="level2 nav-1-2-8 last"> <a href="grid.html"> <span>Genre Romance</span> </a> </li>
                                                    </ul>
                                                </li>
                                                <li class="level1 nav-1-3 parent"> <a href="grid.html"> <span>Monuments</span> </a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-1-3-9 first"> <a href="grid.html"> <span>Runes De Carthage</span> </a> </li>
                                                        <li class="level2 nav-1-3-10"> <a href="grid.html"> <span>Médina de Hamamet</span> </a> </li>
                                                        <li class="level2 nav-1-3-11"> <a href="grid.html"> <span>Sidi Bousaid</span> </a> </li>
                                                        <li class="level2 nav-1-3-12 last"> <a href="grid.html"> <span>El Jem Amphithéâtre</span> </a> </li>
                                                    </ul>
                                                </li>
                                                <li class="level1 nav-1-4 last parent"> <a href="grid.html"> <span>Cinema</span> </a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-1-4-13 first"> <a href="grid.html"> <span>Genre Action</span> </a> </li>
                                                        <li class="level2 nav-1-4-14"> <a href="grid.html"> <span>Genre Comédie</span> </a> </li>
                                                        <li class="level2 nav-1-4-15"> <a href="grid.html"> <span>Genre  Drame</span> </a> </li>
                                                        <li class="level2 nav-1-4-16 last"> <a href="grid.html"> <span>Genre Historique</span> </a> </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="level0 nav-8 level-top parent"> <a class="level-top" href="grid.html"> <span>Sortir</span> </a>
                                            <ul class="level0">
                                                <li class="level1 nav-2-1 first parent"> <a href="grid.html"> <span>Randonneé</span> </a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-2-1-1 first"> <a href="grid.html"> <span>Balade</span> </a> </li>
                                                        <li class="level2 nav-2-1-2"> <a href="grid.html"> <span>Grand Randonneé Trekking</span> </a> </li>
                                                        <li class="level2 nav-2-1-3"> <a href="grid.html"> <span>Régim Séche et Désert</span> </a> </li>

                                                    </ul>
                                                </li>
                                                <li class="level1 nav-2-2 parent"> <a href="grid.html"> <span>Voyage</span> </a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-2-2-5 first"> <a href="grid.html"> <span>Circuit Touristique</span> </a> </li>
                                                        <li class="level2 nav-2-2-6"> <a href="grid.html"> <span>Voyage en Vélo</span> </a> </li>
                                                        <li class="level2 nav-2-2-7"> <a href="grid.html"> <span>Croisiére en Bâteau</span> </a> </li>
                                                        <li class="level2 nav-2-2-8 last"> <a href="grid.html"> <span>Tournée</span> </a> </li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>
                                        <li class="level0 nav-9 level-top "> <a class="level-top" href="blog.html"> <span>Blog</span> </a> </li>
                                        <li class="level0 nav-10 level-top "> <a class="level-top" href="blog.html"> <span>Contact</span> </a> </li>
                                        <li class="level0 nav-11 level-top last parent "> <a class="level-top" href="contact_us.html"> <span>Custom</span> </a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!--navmenu-->
                </div>

                <!--End mobile-menu -->
                <ul id="nav" class="hidden-xs">
                    <li id="nav-home" class="level0 parent drop-menu"><a href="index.html"><span>Decouvrir</span> </a>

                    </li>

                    <li class="level0 nav-6 level-top parent"> <a href="grid.html" class="level-top"> <span>sortie</span> </a>
                        <div class="level0-wrapper dropdown-6col">
                            <div class="level0-wrapper2">
                                <div class="nav-block nav-block-center">
                                    <ul class="level0">
                                        <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Randonneé</span></a>

                                            <!--sub sub category-->

                                            <ul class="level1">
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Balade</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Grande Randonneé et Trekking</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Regim séche et désert</span></a> </li>
                                                <!--level2 nav-6-1-1-->

                                            </ul>
                                            <!--level1-->

                                            <!--sub sub category-->

                                        </li>
                                        <!--level3 nav-6-1 parent item-->

                                        <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Voyages</span></a>
                                            <!--sub sub category-->

                                            <ul class="level1">
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Circuit Touristique</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Croisiére en vélo</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Croisiére en bateau</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Tourneé</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                            </ul>
                                            <!--level1-->


                                        </li>




                                    </ul>
                                    <!--level0-->
                                </div>
                            </div>
                            <div class="nav-add">
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="sunglass"></a></div>
                                </div>
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="watch"></a></div>
                                </div>
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="jeans"></a></div>
                                </div>
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="shoes"></a></div>
                                </div>
                                <div class="push_item push_item_last">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="swimwear"></a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="level0 nav-5 level-top first"> <a class="level-top" href="grid.html"> <span>Visiter</span> </a>
                        <div class="level0-wrapper dropdown-6col">
                            <div class="level0-wrapper2">
                                <div class="nav-block nav-block-center">
                                    <ul class="level0">
                                        <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Musées</span></a>

                                            <!--sub sub category-->

                                            <ul class="level1">
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Musée National du Bardo</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Musée National du Carthage</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Musée d'El Jem</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Musée de Chemtou</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                            </ul>
                                            <!--level1-->

                                            <!--sub sub category-->

                                        </li>
                                        <!--level3 nav-6-1 parent item-->

                                        <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>théâtre</span></a>
                                            <!--sub sub category-->

                                            <ul class="level1">
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Musicale</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Comédie</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Romance</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Drame</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                            </ul>
                                            <!--level1-->

                                            <!--sub sub category-->

                                        </li>
                                        <!--level3 nav-6-1 parent item-->

                                        <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Monuments</span></a>
                                            <!--sub sub category-->

                                            <ul class="level1">
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Runes de Carthage</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Médina de Hamamet</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Sidi Bousaid</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>EL Jem Amphithéâtre</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                            </ul>
                                            <!--level1-->

                                            <!--sub sub category-->

                                        </li>
                                        <!--level3 nav-6-1 parent item-->

                                        <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Cinema</span></a>
                                            <!--sub sub category-->

                                            <ul class="level1">
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Comedie</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Action</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Drame</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                                <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Genre Historique</span></a> </li>
                                                <!--level2 nav-6-1-1-->
                                            </ul>
                                            <!--level1-->

                                            <!--sub sub category-->

                                        </li>
                                        <!--level3 nav-6-1 parent item-->


                                        <!--level0-->
                                </div>
                            </div>
                            <div class="nav-add">
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="sunglass"></a></div>
                                </div>
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="watch"></a></div>
                                </div>
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="jeans"></a></div>
                                </div>
                                <div class="push_item">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="shoes"></a></div>
                                </div>
                                <div class="push_item push_item_last">
                                    <div class="push_img"><a href="#"><img src="images/menu-sunglass.png" alt="swimwear"></a></div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="level0 nav-8 level-top"> <a href="blog.html" class="level-top"> <span>Blog</span> </a> </li>
                    <li class="level0 nav-8 level-top"> <a href="contact_us.html" class="level-top"> <span>Contact</span> </a> </li>
                    <li class="nav-custom-link level0 level-top parent"> <a class="level-top" href="#"><span>Custom</span></a>
                        <div class="level0-wrapper custom-menu">
                            <div class="header-nav-dropdown-wrapper clearer">
                                <div class="grid12-5">
                                    <div class="custom_img"><a href="#"><img src="images/custom-img1.jpg" alt="custom img1"></a></div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                                    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                                </div>
                                <div class="grid12-5">
                                    <div class="custom_img"><a href="#"><img src="images/custom-img2.jpg" alt="custom img2"></a></div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                                    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                                </div>
                                <div class="grid12-5">
                                    <div class="custom_img"><a href="#"><img src="images/custom-img3.jpg" alt="custom img3"></a></div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                                    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                                </div>
                                <div class="grid12-5">
                                    <div class="custom_img"><a href="#"><img src="images/custom-img4.jpg" alt="custom img4"></a></div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
                                    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

</div>
</body>
</html>