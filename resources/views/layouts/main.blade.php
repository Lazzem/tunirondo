<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TuniRando</title>

    <!-- Favicons Icon -->
    <link rel="icon" href="{{URL::asset('http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico')}}" type="image/x-icon" />
    <link rel="shortcut icon" href="{{URL::asset('http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico')}}" type="image/x-icon" />

    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <!-- CSS Style -->

    <link rel="stylesheet" href="{{URL::asset('css/animate.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/revslider.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.css')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome.css')}}" type="text/css">
    <style>
        .help-block{
            color: red;
        }
    </style>
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,300,700,800,400,600' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="page">
    <!-- Header -->
    <header class="header-container">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <!-- Header Language -->
                    <div class="col-xs-6">
                        @if (!Auth::guest())
                    <div class="welcome-msg hidden-xs"> Bienvenue  <samp style="text-transform: uppercase; font-size: 13px"> {{ Auth::user()->name }} {{ Auth::user()->lastname }}</samp> </div>
                            @endif
                </div>
                <div class="col-xs-6">

                    <!-- Header Top Links -->
                    <div class="toplinks">
                        <div class="links">
                            @if (Auth::guest())
                                <div class="login"><a title="Login" href="/login"><span class="hidden-xs">Identifiez ou inscrivez</span></a></div>

                            @else

                                <div class="myaccount"><a title="Mon Compte" href="{{ url('/Profile') }}"><span class="hidden-xs">Mon Compte</span></a></div>

                                <div ><a title="Déconnexion" href="{{ url('/logout') }}"><span class="popOutList__itemIcon  icon-off" style="font-size: 14px;"> <span class="hidden-xs">Déconnexion</span></span></a></div>

                            @endif
                        </div>
                    </div>
                    <!-- End Header Top Links -->
                </div>
            </div>
        </div>




               <div class="header container">
                   <div class="row">
                      <div class="col-lg-2 col-sm-3 col-md-1">
            <!-- Header Logo -->
            <a class="logo" title="Magento Commerce" href="/"><img alt="Tun Rando" src="{{asset('images/logo.png')}}"width="170" height="50"></a>
            <!-- End Header Logo -->
        </div>
        <div class="col-lg-8 col-sm-6 col-md-8">
            <!-- Search-col -->
            <div class="search-box">
                <form action="/Recherche" method="POST" id="search_mini_form" name="Categories" role="search">
                    {{ csrf_field() }}

                    <select name="category_id" class="cate-dropdown hidden-xs">
                        <option value="0">Tous les Categories </option>
@foreach($sscategories as $ss)
                            <option value="{{$ss->id}}">{{$ss->title}}</option>


                        @endforeach
                    </select>

                    <input type="text" placeholder="Recherche Produit..."  maxlength="70" class="" name="q" id="search">
                    <button id="submit-button" class="search-btn-bg"><span>Recherche</span></button>
                </form>
            </div>
            <!-- End Search-col -->
        </div>
        <!-- Top Cart -->
        <div class="col-lg-2 col-sm-3 col-md-2">

            @if(Session::has('panier'))
            <div class="top-cart-contain">
                <div class="mini-cart">

                    <div  class="basket dropdown-toggle"> <a href="/panier"> <i class="glyphicon glyphicon-shopping-cart"></i>
                            <div class="cart-box"><span class="title">Panier</span><span id="cart-total">{{$quantiteTotal}}</span></div>

                        </a>
                    </div>



                </div>

            </div>
            @else
                <div class="top-cart-contain">
                    <div class="mini-cart">
                        <div  class="basket dropdown-toggle"> <a href="/panier"> <i class="glyphicon glyphicon-shopping-cart"></i>
                                <div class="cart-box"><span class="title">Panier</span><span id="cart-total"></span></div>

                            </a>
                        </div>

                        </div>


                </div>
            @endif
            </a>
        </div>
        <!-- End Top Cart -->
    </div>
</div>

<!-- end header -->
</header>
<!-- Navbar -->
<nav>
    <div class="container">
        <div class="nav-inner">
            <div class="logo-small"> <a class="logo" title="Magento Commerce" href="/"><img alt="Magento Commerce" src="{{asset('images/logo.png')}}" ></a> </div>
            <!-- mobile-menu -->
            <div class="hidden-desktop" id="mobile-menu">
                <ul class="navmenu">
                    <li>
                        <div class="menutop">
                            <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
                            <h2>Menu</h2>
                        </div>
                        <ul class="submenu">
                            <li>
                                <ul class="topnav">
                                    <li class="level0 nav-6 level-top first parent"> <a class="level-top" href="#"> <span>decouvrir</span> </a>

                                    </li>
                                    <li class="level0 nav-7 level-top parent"> <a class="level-top" href="/grid"> <span>Visiter</span> </a>
                                        <ul class="level0">
                                            <li class="level1 nav-1-1 first parent"> <a href="/grid"> <span>Musées</span> </a>
                                                <ul class="level1">
                                                    <li class="level2 nav-1-1-1 first"> <a href="/grid"> <span>Musée National du Bardo</span> </a> </li>
                                                    <li class="level2 nav-1-1-2"> <a href="/grid"> <span>Musée National du Carthage</span> </a> </li>
                                                    <li class="level2 nav-1-1-3"> <a href="/grid"> <span>Musée d'El Jem</span> </a> </li>
                                                    <li class="level2 nav-1-1-4 last"> <a href="/grid"> <span>Musée Chemtou</span> </a> </li>
                                                </ul>
                                            </li>
                                            <li class="level1 nav-1-2 parent"> <a href="/grid"> <span>Théâtre</span> </a>
                                                <ul class="level1">
                                                    <li class="level2 nav-1-2-5 first"> <a href="/grid"> <span>Genre Comédie </span> </a> </li>
                                                    <li class="level2 nav-1-2-6"> <a href="/grid"> <span>Genre Drame</span> </a> </li>
                                                    <li class="level2 nav-1-2-7"> <a href="/grid"> <span>Genre Musicale</span> </a> </li>
                                                    <li class="level2 nav-1-2-8 last"> <a href="/grid"> <span>Genre Romance</span> </a> </li>
                                                </ul>
                                            </li>
                                            <li class="level1 nav-1-3 parent"> <a href="/grid"> <span>Monuments</span> </a>
                                                <ul class="level1">
                                                    <li class="level2 nav-1-3-9 first"> <a href="/grid"> <span>Runes De Carthage</span> </a> </li>
                                                    <li class="level2 nav-1-3-10"> <a href="/grid"> <span>Médina de Hamamet</span> </a> </li>
                                                    <li class="level2 nav-1-3-11"> <a href="/grid"> <span>Sidi Bousaid</span> </a> </li>
                                                    <li class="level2 nav-1-3-12 last"> <a href="/grid"> <span>El Jem Amphithéâtre</span> </a> </li>
                                                </ul>
                                            </li>
                                            <li class="level1 nav-1-4 last parent"> <a href="/grid"> <span>Cinema</span> </a>
                                                <ul class="level1">
                                                    <li class="level2 nav-1-4-13 first"> <a href="/grid"> <span>Genre Action</span> </a> </li>
                                                    <li class="level2 nav-1-4-14"> <a href="/grid"> <span>Genre Comédie</span> </a> </li>
                                                    <li class="level2 nav-1-4-15"> <a href="/grid"> <span>Genre  Drame</span> </a> </li>
                                                    <li class="level2 nav-1-4-16 last"> <a href="/grid"> <span>Genre Historique</span> </a> </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="level0 nav-8 level-top parent"> <a class="level-top" href="/grid"> <span>Sortir</span> </a>
                                        <ul class="level0">
                                            @foreach($categorie as $categories)

                                            <li class="level1 nav-2-1 first parent"> <a href="{{url('/DetailCategorie/'.$categories->id)}}"> <span>{{$categories->title}}</span> </a>
                                                <ul class="level1">
                                                    <li class="level2 nav-2-1-1 first"> <a href="/grid"> <span>Balade</span> </a> </li>
                                                    <li class="level2 nav-2-1-2"> <a href="/grid"> <span>Grand Randonneé Trekking</span> </a> </li>
                                                    <li class="level2 nav-2-1-3"> <a href="/grid"> <span>Régim Séche et Désert</span> </a> </li>

                                                </ul>
                                            </li>
                                            @endforeach
                                            <li class="level1 nav-2-2 parent"> <a href="/grid"> <span>Voyage</span> </a>
                                                <ul class="level1">
                                                    <li class="level2 nav-2-2-5 first"> <a href="/grid"> <span>Circuit Touristique</span> </a> </li>
                                                    <li class="level2 nav-2-2-6"> <a href="/grid"> <span>Voyage en Vélo</span> </a> </li>
                                                    <li class="level2 nav-2-2-7"> <a href="/grid"> <span>Croisiére en Bâteau</span> </a> </li>
                                                    <li class="level2 nav-2-2-8 last"> <a href="/grid"> <span>Tournée</span> </a> </li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </li>
                                    <li class="level0 nav-9 level-top "> <a class="level-top" href="/blog"> <span>Blog</span> </a> </li>
                                    <li class="level0 nav-10 level-top "> <a class="level-top" href="/blog"> <span>Contact</span> </a> </li>
                                    <li class="level0 nav-11 level-top last parent "> <a class="level-top" href="/contact"> <span>Custom</span> </a> </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--navmenu-->
            </div>

            <!--End mobile-menu -->
            <ul id="nav" class="hidden-xs">
                <li id="nav-home" class="level0 parent drop-menu"><a href="/"><span>Decouvrir</span> </a>

                </li>
@foreach( $menu as $menus)
                <li class="level0 nav-6 level-top parent"> <a  class="level-top"> <span>{{$menus->title}}</span> </a>
                    <div class="level0-wrapper dropdown-6col">
                        <div class="level0-wrapper2">
                            <div class="nav-block nav-block-center">
                                <ul class="level0">
                                    @foreach($categorie as $categories)
                        @if($categories->idmenu == $menus->id)
                                    <li class="level3 nav-6-1 parent item"> <a ><span>{{$categories->title}}</span></a>

                                        <!--sub sub category-->

                                        <ul class="level1">
                                            @foreach($souscategorie as $souscategories)
                                                @if($souscategories->tag_id == $categories->id )

                                            <li class="level2 nav-6-1-1"> <a href="{{route('detailCategorie.show',$souscategories->id)}}"><span>{{$souscategories->title}}</span></a> </li>
                                               @endif

                                            @endforeach

                                        </ul>

                                        <!--sub sub category-->

                                    </li>

                                    @endif
                                    @endforeach
                                    <!--level3 nav-6-1 parent item-->





                                </ul>
                                <!--level0-->
                            </div>
                        </div>
                        <div class="nav-add">


</div>


</div>
</li>
@endforeach

<li class="level0 nav-8 level-top"> <a href="/blog" class="level-top"> <span>Blog</span> </a> </li>
<li class="level0 nav-8 level-top"> <a href="/contact_us" class="level-top"> <span>Contact</span> </a> </li>
<li class="nav-custom-link level0 level-top parent"> <a class="level-top" href="#"><span>Custom</span></a>
<div class="level0-wrapper custom-menu">
<div class="header-nav-dropdown-wrapper clearer">
<div class="grid12-5">
    <div class="custom_img"><a href="#"><img src="{{asset('images/custom-img1.jpg')}}" alt="custom img1"></a></div>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
</div>
<div class="grid12-5">
    <div class="custom_img"><a href="#"><img src="{{asset('images/custom-img2.jpg')}}images/custom-img2.jpg" alt="custom img2"></a></div>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
</div>
<div class="grid12-5">
    <div class="custom_img"><a href="#"><img src="{{asset('images/custom-img3.jpg')}}" alt="custom img3"></a></div>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
</div>
<div class="grid12-5">
    <div class="custom_img"><a href="#"><img src="{{asset('images/custom-img4.jpg')}}" alt="custom img4"></a></div>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
    <button type="button" title="Add to Cart" class="learn_more_btn"><span>Learn More</span></button>
</div>
</div>
</div>
</li>
</ul>
</div>
</div>
</nav>


<!-- Main -->
<div class="main-container">
@yield('content')


</div>

<!-- end Main -->



<footer class="footer wow bounceInUp animated">
<div class="brand-logo ">
<div class="container">
<div class="slider-items-products">
<div id="brand-logo-slider" class="product-flexslider hidden-buttons">
<div class="slider-items slider-width-col6" style="height: 50px;">

    <div class="item"> <img src="{{asset('images/sponsor/16.png')}}" alt="Image"width="150" height="60"> </div>

<!-- Item -->
<div class="item"><img src="{{asset('images/sponsor/1.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<div class="item"><img src="{{asset('images/sponsor/2.png')}}" alt="Image"width="150" height="60"></div>
<!-- End Item -->

<!-- Item -->
<div class="item"> <img src="{{asset('images/sponsor/4.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<div class="item"> <img src="{{asset('images/sponsor/6.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<div class="item"> <img src="{{asset('images/sponsor/8.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<div class="item"> <img src="{{asset('images/sponsor/10.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<div class="item"><img src="{{asset('images/sponsor/12.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<!-- End Item -->


<!-- Item -->
<!-- End Item -->

<!-- Item -->
<div class="item"> <img src="{{asset('images/sponsor/15.png')}}" alt="Image"width="150" height="60"> </div>
<!-- End Item -->

<!-- Item -->
<!-- End Item -->

<!-- Item -->
<!-- End Item -->

</div>
</div>
</div>
</div>
</div>
<div class="footer-top">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-7">
<div class="block-subscribe">
<div class="newsletter">

</div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-5">
<div class="social">
<ul>
    <li class="fb"><a href="#"></a></li>
    <li class="tw"><a href="#"></a></li>
    <li class="googleplus"><a href="#"></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="footer-middle container">
<div class="row">
<div class="col-md-3 col-sm-4">
<div class="footer-logo"><a href="/" title="Logo"><img src="{{asset('images/logo.png')}}"width="170" height="50" alt="logo"></a></div>
<p>Type de paiement </p>
<div class="payment-accept">
<div><img src="{{asset('images/payment-1.png')}}" alt="payment"> </div>
</div>
</div>
<div class="col-md-2 col-sm-4">
<h4>Shopping Guide</h4>
<ul class="links">
<li class="first"><a href="#" title="How to buy">How to buy</a></li>
<li><a href="/faq" title="FAQs">FAQs</a></li>
<li><a href="#" title="Payment">Payment</a></li>
<li><a href="#" title="Shipment&lt;/a&gt;">Shipment</a></li>
<li><a href="#" title="Where is my order?">Where is my order?</a></li>
<li class="last"><a href="#" title="Return policy">Return policy</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-4">
<h4>Style Advisor</h4>
<ul class="links">
<li class="first"><a title="Your Account" href="/">Your Account</a></li>
<li><a title="Information" href="#">Information</a></li>
<li><a title="Addresses" href="#">Addresses</a></li>
<li><a title="Addresses" href="#">Discount</a></li>
<li><a title="Orders History" href="#">Orders History</a></li>
<li class="last"><a title=" Additional Information" href="#">Additional Information</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-4">
<h4>Information</h4>
<ul class="links">
<li class="first"><a href="#" title="privacy policy">Privacy policy</a></li>
<li><a href="#/" title="Search Terms">Search Terms</a></li>
<li><a href="#" title="Advanced Search">Advanced Search</a></li>
<li><a href="/contact_us" title="Contact Us">Contact Us</a></li>
<li><a href="#" title="Suppliers">Suppliers</a></li>
<li class=" last"><a href="#" title="Our stores" class="link-rss">Our stores</a></li>
</ul>
</div>
<div class="col-md-3 col-sm-4">
<h4>Contact us</h4>
<div class="contacts-info">
<address>
<i class="add-icon">&nbsp;</i>123 Main Street, Anytown, <br>
&nbsp;CA 12345  USA
</address>
<div class="phone-footer"><i class="phone-icon">&nbsp;</i> +1 800 123 1234</div>
<div class="email-footer"><i class="email-icon">&nbsp;</i> <a href="#">support@magikcommerce.com</a> </div>
</div>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="container">
<div class="row">
<div class="col-sm-12 col-xs-12 coppyright"><center> &copy; 2017. TuniRando. </center> </div>
</div>
</div>
</div>
</footer>
<!-- End Footer -->

</div>
<!-- JavaScript -->
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/parallax.js')}}"></script>
<script type="text/javascript" src="{{asset('js/common.js')}}"></script>
<script type="text/javascript" src="{{asset('js/revslider.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.jcarousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cloudzoom.js')}}"></script>

<script type='text/javascript'>
jQuery(document).ready(function(){
jQuery('#rev_slider_4').show().revolution({
dottedOverlay: 'none',
delay: 5000,
startwidth: 770,
startheight: 460,

hideThumbs: 200,
thumbWidth: 200,
thumbHeight: 50,
thumbAmount: 2,

navigationType: 'thumb',
navigationArrows: 'solo',
navigationStyle: 'round',

touchenabled: 'on',
onHoverStop: 'on',

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,

spinner: 'spinner0',
keyboardNavigation: 'off',

navigationHAlign: 'center',
navigationVAlign: 'bottom',
navigationHOffset: 0,
navigationVOffset: 20,

soloArrowLeftHalign: 'left',
soloArrowLeftValign: 'center',
soloArrowLeftHOffset: 20,
soloArrowLeftVOffset: 0,

soloArrowRightHalign: 'right',
soloArrowRightValign: 'center',
soloArrowRightHOffset: 20,
soloArrowRightVOffset: 0,

shadow: 0,
fullWidth: 'on',
fullScreen: 'off',

stopLoop: 'off',
stopAfterLoops: -1,
stopAtSlide: -1,

shuffle: 'off',

autoHeight: 'off',
forceFullWidth: 'on',
fullScreenAlignForce: 'off',
minFullScreenHeight: 0,
hideNavDelayOnMobile: 1500,

hideThumbsOnMobile: 'off',
hideBulletsOnMobile: 'off',
hideArrowsOnMobile: 'off',
hideThumbsUnderResolution: 0,

hideSliderAtLimit: 0,
hideCaptionAtLimit: 0,
hideAllCaptionAtLilmit: 0,
startWithSlide: 0,
fullScreenOffsetContainer: ''
});
});
</script>
</body>
</html>