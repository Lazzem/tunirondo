<!DOCTYPE html>
<html class="no-js before-run" lang="fr">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">

    <title>TuniRando | Interface Admin </title>




    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/formvalidation/formValidation.css')}}">
    <!-- Plugin -->

    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/datatables-bootstrap/dataTables.bootstrap.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/datatables-fixedheader/dataTables.fixedHeader.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/datatables-responsive/dataTables.responsive.css')}}">




    <link rel="apple-touch-icon" href="{{asset('../BackOffice/assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('../BackOffice/assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/css/site.min.css')}}">

    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/flag-icon-css/flag-icon.css')}}">

    <!-- Plugin -->
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/chartist-js/chartist.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/vendor/jvectormap/jquery-jvectormap.css')}}">

    <!-- Page -->
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/css/../fonts/weather-icons/weather-icons.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/css/dashboard/v1.css')}}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/fonts/web-icons/web-icons.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('BackOffice/assets/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


    <link rel="stylesheet" href="{{URL::asset('../../assets/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{URL::asset('../../assets/vendor/blueimp-file-upload/jquery.fileupload.css')}}">


    <!--[if lt IE 9]>
    <script src="{{URL::asset('BackOffice/assets/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{URL::asset('BackOffice/assets/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{URL::asset('BackOffice/assets/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{URL::asset('BackOffice/assets/vendor/modernizr/modernizr.js')}}"></script>
    <script src="{{URL::asset('BackOffice/assets/vendor/breakpoints/breakpoints.js')}}"></script>
    <style>
        @media (max-width: 480px) {
            .panel-actions .dataTables_length {
                display: none;
            }
        }

        @media (max-width: 320px) {
            .panel-actions .dataTables_filter {
                display: none;
            }
        }

        @media (max-width: 767px) {
            .dataTables_length {
                float: left;
            }
        }

        #exampleTableAddToolbar {
            padding-left: 30px;
        }
    </style>
    <style>
        .site-action{
            width: 66px;
            height: 63px;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="{{URL::asset('BackOffice/assets/vendor/html5shiv/html5shiv.min.js')}}"></script>

    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{URL::asset('BackOffice/assets/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{URL::asset('BackOffice/assets/vendor/respond/respond.min.js')}}"></script>


    <!-- Scripts -->
    <script src="{{URL::asset('BackOffice/assets/vendor/modernizr/modernizr.js')}}"></script>
    <script src="{{URL::asset('BackOffice/assets/vendor/breakpoints/breakpoints.js')}}"></script>

    <script>
        Breakpoints();
    </script>
</head>
<body class="dashboard">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided" data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon wb-more-horizontal" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="{{URL::asset('images/logo.png')}}" title="Remark" style="width: 212px">
        </div>
    </div>

    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">

                <li class="hidden-xs" id="toggleFullscreen">
                    <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>


            </ul>
            <!-- End Navbar Toolbar -->

            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

                <li class="dropdown">
                    <a class="navbar-avatar dropdown-toggle" href="{{ url('/logoutadmin') }}" aria-expanded="false"
                       data-animation="slide-bottom" role="button">


                       <i class="icon wb-power" aria-hidden="true"></i> Déconnexion


                    </a>

                </li>

                <li class="dropdown">
                    <a data-toggle="dropdown" href="javascript:void(0)" title="Messages" aria-expanded="false"
                       data-animation="slide-bottom" role="button">
                        <i class="icon wb-envelope" aria-hidden="true"></i>
                        <span class="badge badge-info up">3</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                        <li class="dropdown-menu-header" role="presentation">
                            <h5>MESSAGES</h5>
                            <span class="label label-round label-info">New 3</span>
                        </li>

                        <li class="list-group" role="presentation">
                            <div data-role="container">
                                <div data-role="content">
                                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-online">
                            <img src="{{asset('assets/portraits/2.jpg')}}" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Mary Adams</h6>
                                                <div class="media-meta">
                                                    <time datetime="2015-06-17T20:22:05+08:00">30 minutes ago</time>
                                                </div>
                                                <div class="media-detail">Anyways, i would like just do it</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-off">
                            <img src="{{asset('assets/portraits/3.jpg')}}" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Caleb Richards</h6>
                                                <div class="media-meta">
                                                    <time datetime="2015-06-17T12:30:30+08:00">12 hours ago</time>
                                                </div>
                                                <div class="media-detail">I checheck the document. But there seems</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-busy">
                            <img src="{{asset('assets/portraits/4.jpg')}}" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">June Lane</h6>
                                                <div class="media-meta">
                                                    <time datetime="2015-06-16T18:38:40+08:00">2 days ago</time>
                                                </div>
                                                <div class="media-detail">Lorem ipsum Id consectetur et minim</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                        <div class="media">
                                            <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-away">
                            <img src="{{asset('assets/portraits/5.jpg')}}" alt="..." />
                            <i></i>
                          </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Edward Fletcher</h6>
                                                <div class="media-meta">
                                                    <time datetime="2015-06-15T20:34:48+08:00">3 days ago</time>
                                                </div>
                                                <div class="media-detail">Dolor et irure cupidatat commodo nostrud nostrud.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-footer" role="presentation">
                            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                                <i class="icon wb-settings" aria-hidden="true"></i>
                            </a>
                            <a href="javascript:void(0)" role="menuitem">
                                See all messages
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>

    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu">
                    <li class="site-menu-category">Administrateur</li>
                    <li class="site-menu-item">
                        <a class="animsition-link" href="/Bindex" data-slug="page-profile">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Acceuil</span>
                        </a>
                    </li>

                    <li class="site-menu-item">
                        <a class="animsition-link" href="/Bprofile" data-slug="page-profile">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Profile</span>
                        </a>
                    </li>

                    <li class="site-menu-item">
                        <a class="animsition-link" href="/Bmenu" data-slug="page-profile">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Menu du site</span>
                        </a>
                    </li>

                    <li class="site-menu-item">
                        <a class="animsition-link" href="/BackCategorie" data-slug="page-faq">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Categorie</span>
                        </a>
                    </li>
                    <li class="site-menu-item">
                        <a class="animsition-link" href="/ssBackCategorie" data-slug="page-faq">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Sous categorie</span>
                        </a>
                    </li>

                    <li class="site-menu-item">
                        <a class="animsition-link" href="/BProduits" data-slug="page-faq">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Produits</span>
                        </a>
                    </li>
                    <li class="site-menu-item">
                        <a class="animsition-link" href="/BCommande" data-slug="page-faq">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Commande</span>
                        </a>
                    </li>

                    <li class="site-menu-item">
                        <a class="animsition-link" href="/BBlog" data-slug="page-faq">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title">Blog</span>
                        </a>
                    </li>

                    <li class="site-menu-item">
                        <a class="animsition-link" href="/user" data-slug="page-user">
                            <i class="site-menu-icon " aria-hidden="true"></i>
                            <span class="site-menu-title"><i class="icon fa-users" aria-hidden="true" style="font-size: 27px;"></i>Utilisateur</span>
                        </a>
                    </li>
                </ul>

                </ul>
            </div>
        </div>
    </div>


</div>
<div class="site-gridmenu">
    <ul>
        <li>
            <a href="/Bindex">
                <i class="icon wb-dashboard"></i>
                <span>Acceuil</span>
            </a>
        </li>
        <li>
            <a href="/">
                <i class="icon wb-image"></i>
                <span>Acceuil de site</span>
            </a>
        </li>


    </ul>
</div>



<div class="main-container">
    @yield('content')
</div>


<script src="{{URL::asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>



<!-- Core  -->
<script src="{{URL::asset('BackOffice/assets/vendor/jquery/jquery.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/animsition/jquery.animsition.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/asscroll/jquery-asScroll.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/asscrollable/jquery.asScrollable.all.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

<!-- Plugins -->
<script src="{{URL::asset('BackOffice/assets/vendor/switchery/switchery.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/intro-js/intro.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>

<script src="{{URL::asset('BackOffice/assets/vendor/skycons/skycons.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/chartist-js/chartist.min.jstest')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/aspieprogress/jquery-asPieProgress.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/jvectormap/maps/jquery-jvectormap-ca-lcc-en.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>


<script src="{{URL::asset('BackOffice/assets/vendor/moment/moment.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/fullcalendar/fullcalendar.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/jquery-selective/jquery-selective.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

<script src="{{URL::asset('BackOffice/assets/vendor/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-tmpl/tmpl.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-canvas-to-blob/canvas-to-blob.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-load-image/load-image.all.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload-process.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload-image.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload-audio.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload-video.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload-validate.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/blueimp-file-upload/jquery.fileupload-ui.js')}}"></script>


<!-- Scripts -->
<script src="{{URL::asset('BackOffice/assets/js/core.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/site.js')}}"></script>

<script src="{{URL::asset('BackOffice/assets/js/sections/menu.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/sections/menubar.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/sections/sidebar.js')}}"></script>

<script src="{{URL::asset('BackOffice/assets/js/configs/config-colors.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/configs/config-tour.js')}}"></script>

<script src="{{URL::asset('BackOffice/assets/js/components/asscrollable.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/animsition.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/slidepanel.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/datatables.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/switchery.js')}}"></script>





<script src="{{URL::asset('BackOffice/assets/js/components/switchery.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/matchheight.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/jvectormap.js')}}"></script>


<script src="{{URL::asset('BackOffice/assets/js/components/bootstrap-touchspin.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/components/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/apps/app.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/js/apps/calendar.js')}}"></script>


<script src="{{URL::asset('BackOffice/assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/datatables-fixedheader/dataTables.fixedHeader.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/datatables-bootstrap/dataTables.bootstrap.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/datatables-responsive/dataTables.responsive.js')}}"></script>
<script src="{{URL::asset('BackOffice/assets/vendor/datatables-tabletools/dataTables.tableTools.js')}}"></script>





<!-- Scripts -->







<script>
    (function(document, window, $) {
        'use strict';
        var AppCalendar = window.AppCalendar;

        $(document).ready(function() {
            AppCalendar.run();
        });
    })(document, window, jQuery);
</script>

<script>
    (function(document, window, $) {
        $(document).ready(function($) {
            Site.run();
        });

        // Example File Upload
        // -------------------
        $('#exampleUploadForm').fileupload({
            url: '../../server/fileupload/',
            dropzone: $('#exampleUploadForm'),
            filesContainer: $('.file-list'),
            uploadTemplateId: false,
            downloadTemplateId: false,
            uploadTemplate: tmpl(
                '{% for (var i=0, file; file=o.files[i]; i++) { %}' +
                '<div class="file template-upload fade col-lg-2 col-md-4 col-sm-6 {%=file.type.search("image") !== -1? "image" : "other-file"%}">' +
                '<div class="file-item">' +
                '<div class="preview vertical-align">' +
                '<div class="file-action-wrap">' +
                '<div class="file-action">' +
                '{% if (!i && !o.options.autoUpload) { %}' +
                '<i class="icon wb-upload start" data-toggle="tooltip" data-original-title="Upload file" aria-hidden="true"></i>' +
                '{% } %}' +
                '{% if (!i) { %}' +
                '<i class="icon wb-close cancel" data-toggle="tooltip" data-original-title="Stop upload file" aria-hidden="true"></i>' +
                '{% } %}' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="info-wrap">' +
                '<div class="title">{%=file.name%}</div>' +
                '</div>' +
                '<div class="progress progress-striped active" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" role="progressbar">' +
                '<div class="progress-bar progress-bar-success" style="width:0%;"></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '{% } %}'
            ),
            downloadTemplate: tmpl(
                '{% for (var i=0, file; file=o.files[i]; i++) { %}' +
                '<div class="file template-download fade col-lg-2 col-md-4 col-sm-6 {%=file.type.search("image") !== -1? "image" : "other-file"%}">' +
                '<div class="file-item">' +
                '<div class="preview vertical-align">' +
                '<div class="file-action-wrap">' +
                '<div class="file-action">' +
                '<i class="icon wb-trash delete" data-toggle="tooltip" data-original-title="Delete files" aria-hidden="true"></i>' +
                '</div>' +
                '</div>' +
                '<img src="{%=file.url%}"/>' +
                '</div>' +
                '<div class="info-wrap">' +
                '<div class="title">{%=file.name%}</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '{% } %}'
            ),
            forceResize: true,
            previewCanvas: false,
            previewMaxWidth: false,
            previewMaxHeight: false,
            previewThumbnail: false
        }).on('fileuploadprocessalways', function(e, data) {
            var length = data.files.length;

            for (var i = 0; i < length; i++) {
                if (!data.files[i].type.match(
                        /^image\/(gif|jpeg|png|svg\+xml)$/)) {
                    data.files[i].filetype = 'other-file';
                } else {
                    data.files[i].filetype = 'image';
                }
            }
        }).on('fileuploadadded', function(e) {
            var $this = $(e.target);

            if ($this.find('.file').length > 0) {
                $this.addClass('has-file');
            } else {
                $this.removeClass('has-file');
            }
        }).on('fileuploadfinished', function(e) {
            var $this = $(e.target);

            if ($this.find('.file').length > 0) {
                $this.addClass('has-file');
            } else {
                $this.removeClass('has-file');
            }
        }).on('fileuploaddestroyed', function(e) {
            var $this = $(e.target);

            if ($this.find('.file').length > 0) {
                $this.addClass('has-file');
            } else {
                $this.removeClass('has-file');
            }
        }).on('click', function(e) {
            if ($(e.target).parents('.file').length === 0) $('#inputUpload')
                .trigger('click');
        });

        $(document).bind('dragover', function(e) {
            var dropZone = $('#exampleUploadForm'),
                timeout = window.dropZoneTimeout;
            if (!timeout) {
                dropZone.addClass('in');
            } else {
                clearTimeout(timeout);
            }
            var found = false,
                node = e.target;
            do {
                if (node === dropZone[0]) {
                    found = true;
                    break;
                }
                node = node.parentNode;
            } while (node !== null);
            if (found) {
                dropZone.addClass('hover');
            } else {
                dropZone.removeClass('hover');
            }
            window.dropZoneTimeout = setTimeout(function() {
                window.dropZoneTimeout = null;
                dropZone.removeClass('in hover');
            }, 100);
        });

        $('#inputUpload').on('click', function(e) {
            e.stopPropagation();
        });

        $('#uploadlink').on('click', function(e) {
            e.stopPropagation();
        });
    })(document, window, jQuery);
</script>

</body>
<script>
    $(document).ready(function($) {
        Site.run();

        (function() {
            var snow = new Skycons({
                "color": $.colors("blue-grey", 500)
            });
            snow.set(document.getElementById("widgetSnow"), "snow");
            snow.play();

            var sunny = new Skycons({
                "color": $.colors("blue-grey", 700)
            });
            sunny.set(document.getElementById("widgetSunny"), "clear-day");
            sunny.play();
        })();

        (function() {
            var lineareaColor = new Chartist.Line(
                '#widgetLineareaColor .ct-chart', {
                    labels: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
                    series: [
                        [4, 4.5, 4.3, 4, 5, 6, 5.5],
                        [3, 2.5, 3, 3.5, 4.2, 4, 5],
                        [1, 2, 2.5, 2, 3, 2.8, 4]
                    ]
                }, {
                    low: 0,
                    showArea: true,
                    showPoint: false,
                    showLine: false,
                    fullWidth: true,
                    chartPadding: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    },
                    axisX: {
                        showLabel: false,
                        showGrid: false,
                        offset: 0
                    },
                    axisY: {
                        showLabel: false,
                        showGrid: false,
                        offset: 0
                    }
                });
        })();

        (function() {
            var stacked_bar = new Chartist.Bar(
                '#widgetStackedBar .ct-chart', {
                    labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                        'V', 'W', 'X', 'Y', 'Z'
                    ],
                    series: [
                        [50, 90, 100, 90, 110, 100, 120, 130, 115, 95, 80, 85,
                            100, 140, 130, 120, 135, 110, 120, 105, 100, 105,
                            90, 110, 100, 60
                        ],
                        [150, 190, 200, 190, 210, 200, 220, 230, 215, 195,
                            180, 185, 200, 240, 230, 220, 235, 210, 220, 205,
                            200, 205, 190, 210, 200, 160
                        ]
                    ]
                }, {
                    stackBars: true,
                    fullWidth: true,
                    seriesBarDistance: 0,
                    chartPadding: {
                        top: 0,
                        right: 30,
                        bottom: 30,
                        left: 20
                    },
                    axisX: {
                        showLabel: false,
                        showGrid: false,
                        offset: 0
                    },
                    axisY: {
                        showLabel: false,
                        showGrid: false,
                        offset: 0
                    }
                });
        })();

        // timeline
        // --------
        (function() {
            var timeline_labels = [];
            var timeline_data1 = [];
            var timeline_data2 = [];
            var totalPoints = 20;
            var updateInterval = 1000;
            var now = new Date().getTime();

            function GetData() {
                timeline_labels.shift();
                timeline_data1.shift();
                timeline_data2.shift();

                while (timeline_data1.length < totalPoints) {
                    var x = Math.random() * 100 + 800;
                    var y = Math.random() * 100 + 400;
                    timeline_labels.push(now += updateInterval);
                    timeline_data1.push(x);
                    timeline_data2.push(y);
                }
            }

            var timlelineData = {
                labels: timeline_labels,
                series: [
                    timeline_data1,
                    timeline_data2
                ]
            };

            var timelineOptions = {
                low: 0,
                showArea: true,
                showPoint: false,
                showLine: false,
                fullWidth: true,
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            };
            new Chartist.Line("#widgetTimeline .ct-chart", timlelineData,
                timelineOptions);

            function update() {
                GetData();

                new Chartist.Line("#widgetTimeline .ct-chart", timlelineData,
                    timelineOptions);
                setTimeout(update, updateInterval);
            }

            update();

        })();

        (function() {
            new Chartist.Line("#widgetLinepoint .ct-chart", {
                labels: ['1', '2', '3', '4', '5', '6'],
                series: [
                    [1, 1.5, 0.5, 2, 2.5, 1.5]
                ]
            }, {
                low: 0,
                showArea: false,
                showPoint: true,
                showLine: true,
                fullWidth: true,
                lineSmooth: false,
                chartPadding: {
                    top: -10,
                    right: -4,
                    bottom: 10,
                    left: -4
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            });
        })();

        (function() {
            new Chartist.Bar("#widgetSaleBar .ct-chart", {
                labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K',
                    'L', 'M', 'N', 'O', 'P', 'Q'
                ],
                series: [
                    [50, 90, 100, 90, 110, 100, 120, 130, 115, 95, 80, 85,
                        100, 140, 130, 120
                    ]
                ]
            }, {
                low: 0,
                fullWidth: true,
                chartPadding: {
                    top: -10,
                    right: 20,
                    bottom: 30,
                    left: 20
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            });
        })();

        (function() {
            new Chartist.Bar("#widgetWatchList .small-bar-one", {
                labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
                series: [
                    [50, 90, 100, 90, 110, 100, 120, 130]
                ]
            }, {
                low: 0,
                fullWidth: true,
                chartPadding: {
                    top: -10,
                    right: 0,
                    bottom: 0,
                    left: 20
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            });

            new Chartist.Bar("#widgetWatchList .small-bar-two", {
                labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
                series: [
                    [50, 90, 100, 90, 110, 100, 120, 120]
                ]
            }, {
                low: 0,
                fullWidth: true,
                chartPadding: {
                    top: -10,
                    right: 0,
                    bottom: 0,
                    left: 20
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            });

            new Chartist.Line("#widgetWatchList .line-chart", {
                labels: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
                series: [
                    [20, 50, 70, 110, 100, 200, 230],
                    [50, 80, 140, 130, 150, 110, 160]
                ]
            }, {
                low: 0,
                showArea: false,
                showPoint: false,
                showLine: true,
                lineSmooth: false,
                fullWidth: true,
                chartPadding: {
                    top: 0,
                    right: 10,
                    bottom: 0,
                    left: 10
                },
                axisX: {
                    showLabel: true,
                    showGrid: false,
                    offset: 30
                },
                axisY: {
                    showLabel: true,
                    showGrid: true,
                    offset: 30
                }
            });
        })();

        (function() {
            new Chartist.Line("#widgetLinepointDate .ct-chart", {
                labels: ['1', '2', '3', '4', '5', '6'],
                series: [
                    [1, 1.5, 0.5, 2, 2.5, 1.5]
                ]
            }, {
                low: 0,
                showArea: false,
                showPoint: true,
                showLine: true,
                fullWidth: true,
                lineSmooth: false,
                chartPadding: {
                    top: 0,
                    right: -4,
                    bottom: 10,
                    left: -4
                },
                axisX: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                },
                axisY: {
                    showLabel: false,
                    showGrid: false,
                    offset: 0
                }
            });

        })();

    });
</script>

</body>

</html>